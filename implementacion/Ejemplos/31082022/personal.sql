# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `ajcm8.personal`;
CREATE DATABASE IF NOT EXISTS `ajcm8.personal`;
-- Seleccionamos la base de datos
USE `ajcm8.personal`;

CREATE TABLE if NOT EXISTS `ajcm8.personal`.`poblacion`(
	idPoblacion INTEGER AUTO_INCREMENT,
	ciudad VARCHAR(100) NOT NULL,
	PRIMARY KEY (idPoblacion)  
);

CREATE TABLE if NOT EXISTS `ajcm8.personal`.`persona`(
   idPersona INTEGER AUTO_INCREMENT,
   nombre VARCHAR(100),
   apellidos VARCHAR(100),
   poblacion INTEGER NOT NULL,
   PRIMARY KEY (idPersona),
   CONSTRAINT fkPersonaPoblacion
	FOREIGN KEY (poblacion) REFERENCES `ajcm8.personal`.`poblacion`(idPoblacion)  ON DELETE RESTRICT ON UPDATE RESTRICT
);