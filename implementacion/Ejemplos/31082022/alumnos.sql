/*Ejemplo de clase sobre la base de datos alumnos del alumno ajcm8
Extructura de las consultas y minimos que no piden para realizarlas
en cuanto a extructura*/

# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `ajcm8.alumnos`;
CREATE DATABASE IF NOT EXISTS `ajcm8.alumnos`;
-- Seleccionamos la base de datos
USE `ajcm8.alumnos`;

CREATE TABLE IF NOT EXISTS `ajcm8.alumnos`.`cursos`(
	#campos
	idCurso INTEGER AUTO_INCREMENT,
	nombre VARCHAR(100) NOT NULL,#null es el valor de defecto, al no ponerlo
	fechaComienzo DATE, #DEFAULT CURRENT_TIMESTAMP, valor hora actual del sistema 
	precio FLOAT DEFAULT 0,
	#claves e indices
	PRIMARY KEY(idCurso),
	UNIQUE KEY alias (nombre),
	KEY nombre1 (fechaComienzo), #nombre1 para referenciar la clave o indice (INDEX) para cuando queramos borrar el indice o clave`ajcm8.alumnos`
	-- Y claves foraneas  relacionadas tambien como clave que no hemos puesto porque no tenemos otra tabla para relacionar
	KEY k1 (fechaComienzo,precio) -- filtrado de la tabla en los campos dados ordenados primero por fechaComienzo y dentro de este por precio
	-- Duplica la tabla con los indices fechaComienzo y precio para realizar las busquedas mas rapidamente
);

CREATE TABLE IF NOT EXISTS `ajcm8.alumnos`.`alumnos`(
   -- campos
	idAlumno INTEGER AUTO_INCREMENT,
	nombre VARCHAR(100),
	apellidos VARCHAR(200),
	telefono VARCHAR(20) NOT NULL,
	fechaNacimiento DATE,
	email VARCHAR(100) NOT NULL,
	poblacion VARCHAR(100) DEFAULT 'Santander',
	-- claves e indices o restricciones
	PRIMARY KEY (idAlumno),
	UNIQUE KEY uk1 (nombre,apellidos),
	UNIQUE KEY email (email),
	KEY telefono (telefono),
	KEY poblacion (poblacion)
);