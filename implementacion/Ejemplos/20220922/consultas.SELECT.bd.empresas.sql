USE `ajcm8.empresas`;
-- -------------------------------------------------------------------------------------
SELECT * FROM personas AS p;
-- -------------------------------------------------------------------------------------
SELECT p.id, p.nombre, p.apellidos FROM personas AS p;
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre FROM personas AS p;-- Elimina los registros repetidos de la consulta
-- -------------------------------------------------------------------------------------
SELECT DISTINCT `ajcm8.empresas`.personas.nombre FROM `ajcm8.empresas`.personas;
-- -------------------------------------------------------------------------------------
SELECT DISTINCT `ajcm8.empresas`.p.nombre FROM `ajcm8.empresas`.personas AS p;
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre AS n, p.poblacion AS po FROM personas AS p;-- También se pueden colocar alias a los campos
-- -------------------------------------------------------------------------------------
SELECT DISTINCT e.nombre FROM empresas AS e;-- Ejercicio 1
-- -------------------------------------------------------------------------------------
SELECT DISTINCT e.poblacion FROM empresas AS e;-- Ejercicio 2
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre FROM personas AS p;-- Ejercicio 3
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre, p.fecha FROM personas AS p;-- Ejercicio 4
-- -------------------------------------------------------------------------------------
SELECT DISTINCT e.codigo, e.nombre FROM empresas AS e WHERE e.trabajadores IS NOT NULL;-- Ejercicio 5

SELECT DISTINCT p.empresas FROM personas AS p;
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre FROM personas AS p WHERE p.empresa=2;
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.id,p.nombre FROM personas AS p WHERE p.empresa=3;
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre, p.apellidos, (p.dato1+p.dato2+p.dato3) AS Suma FROM personas AS p WHERE (p.dato1+p.dato2+p.dato3)>10;
-- No podemos utilizar el alias suma en el where por que el campo no existe en la tabla
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre, p.apellidos FROM personas AS p WHERE (p.dato1+p.dato2+p.dato3)>20;
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre, p.apellidos, (p.dato1+p.dato2+p.dato3) AS Suma FROM personas AS p WHERE (p.dato1+p.dato2+p.dato3)>20;
-- -------------------------------------------------------------------------------------
SELECT POW(2,2);
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre,p.apellidos,p.dato1,p.dato1>5  FROM personas AS p WHERE p.dato1>5;
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.*, MONTH(p.fecha) AS MesFecha FROM personas AS p;
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.*, MONTH(p.fecha) AS MesFecha FROM personas AS p WHERE MONTH(fecha)=9;
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.*, MONTH(p.fecha) AS MesFecha FROM personas AS p WHERE p.fecha LIKE '%-09-%';
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre, p.apellidos,p.poblacion FROM personas AS p WHERE p.poblacion LIKE 'L%';
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre,p.apellidos,p.poblacion FROM personas AS p WHERE SUBSTRING(p.poblacion,1,1)='l';
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre,p.apellidos,p.poblacion FROM personas AS p WHERE LEFT(p.poblacion,1)='L';
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre,p.apellidos,p.poblacion FROM personas AS p WHERE SUBSTRING(p.poblacion,2,1)='o';
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre, p.apellidos,p.poblacion FROM personas AS p WHERE p.poblacion LIKE '_o%';
-- -------------------------------------------------------------------------------------
SELECT DISTINCT p.nombre,p.apellidos,p.poblacion FROM personas AS p WHERE RIGHT(LEFT(p.poblacion,2),1)='o';
-- -------------------------------------------------------------------------------------

-- -------------------------------------------------------------------------------------

-- -------------------------------------------------------------------------------------

-- -------------------------------------------------------------------------------------

-- -------------------------------------------------------------------------------------

-- -------------------------------------------------------------------------------------

-- -------------------------------------------------------------------------------------

-- -------------------------------------------------------------------------------------

-- -------------------------------------------------------------------------------------