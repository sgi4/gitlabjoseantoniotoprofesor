﻿USE empresas;

/**
  EJEMPLO 1
  LISTAR EL CODIGO DE LAS EMPRESAS CUYO 
  NUMERO DE TRABAJADORES ESTA ENTRE 3 Y 5
**/

  -- opcion 1
  SELECT e.codigo
    FROM empresas e 
    WHERE e.trabajadores BETWEEN 3 AND 5;

  -- opcion 2
    SELECT e.codigo
      FROM empresas e
      WHERE e.trabajadores>=3 AND e.trabajadores<=5;

 -- Estas consultas podemos realizarlas utilizando el operador
 -- de conjuntos interseccion

  -- opcion 3
   SELECT * FROM 
   (SELECT e.codigo
      FROM empresas e
      WHERE e.trabajadores>=3) C1
   NATURAL JOIN   
   (SELECT e.codigo
      FROM empresas e
      WHERE e.trabajadores<=5) C2;

  -- opcion 4
    SELECT e.codigo
      FROM empresas e
      WHERE e.trabajadores>=3 
        AND e.codigo IN 
                    (SELECT e.codigo
                      FROM empresas e
                      WHERE e.trabajadores<=5
                     );




