USE `ajcm8.pract9.ciclistas`;
-- ----------------------------------------------------------------------------------------------------------
SELECT c.dorsal,c.nombre

FROM ciclista AS C

WHERE c.nomequipo='banesto'

ORDER BY c.nombre ASC;
-- ----------------------------------------------------------------------------------------------------------
SELECT c.dorsal, c.nombre, c.edad

FROM ciclista AS C

WHERE c.nomequipo='banesto'

ORDER BY c.edad DESC, c.nombre DESC;
-- ----------------------------------------------------------------------------------------------------------
SELECT c.dorsal, c.nombre, c.edad

FROM ciclista AS C

WHERE c.nomequipo='banesto'

ORDER BY 3 DESC, 2 DESC; -- Numero indica la posicion del campo en el select en mysql mariadb o laragon
-- ----------------------------------------------------------------------------------------------------------
SELECT c.dorsal,c.nombre, UCASE(c.nombre), LCASE(c.nombre), (Pow(c.edad,2)/(c.edad*5)) AS IMC

FROM ciclista AS c;
-- ----------------------------------------------------------------------------------------------------------
SELECT c.dorsal,c.nombre, UPPER(c.nombre), LOWER(c.nombre), (Power(c.edad,2)/(c.edad*5)) AS IMC

FROM ciclista AS c;
-- ----------------------------------------------------------------------------------------------------------
USE `ajcm8.empresas`;
SELECT p.nombre, GREATEST(p.dato1,p.dato2,p.dato3) AS MAX
FROM personas AS p;
-- ----------------------------------------------------------------------------------------------------------
USE `ajcm8.empresas`;
SELECT MAX( DISTINCT p.dato1) AS MaxDeColumna  -- no se puede mostrar mas campos de la tabla en el select por que no funciona con
FROM personas AS p;                            -- con las funciones de totales habria que añadir la clausula Group BY
-- si pusieramos el campo nombre no daria el nombre de registro que tiene el máximo.

SELECT p.nombre,p.dato1 AS MaxDeColumna
FROM personas AS p
WHERE p.dato1=(SELECT MAX(p.dato1) FROM personas AS p);

-- ----------------------------------------------------------------------------------------------------------
USE `ajcm8.pract9.ciclistas`;
SELECT COUNT(*) 
FROM ciclista AS c; -- cuenta todos los registros de la tabla ciclista
-- ----------------------------------------------------------------------------------------------------------
USE `ajcm8.pract9.ciclistas`;
SELECT COUNT(nomequipo) -- pero si contamos por un campo los registros totales si este tiene algun dato a null no lo contaria
FROM ciclista AS c;
-- ----------------------------------------------------------------------------------------------------------
USE `ajcm8.pract9.ciclistas`;
SELECT COUNT(DISTINCT nomequipo) 
FROM ciclista AS c;

SELECT COUNT(*)
FROM (SELECT DISTINCT c.nomequipo FROM ciclista AS c) AS c1; -- siempre hay que poner un alias cualdo las subconsulta va en el From
-- ----------------------------------------------------------------------------------------------------------
USE `ajcm8.empresas`;
SELECT COUNT(DISTINCT poblacion) 
FROM empresas AS e;

SELECT COUNT(*)
FROM (SELECT DISTINCT e.poblacion FROM empresas AS e) AS c1; -- siempre hay que poner un alias cualdo las subconsulta va en el From
-- ----------------------------------------------------------------------------------------------------------
USE `ajcm8.ciclistas`;
SELECT COUNT(*) AS Total
FROM ciclista AS c
WHERE c.edad >28;
-- ----------------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------------