SET NAMES 'utf8';
USE `ajcm8.empleados`;

-- producto cartesiano 

SELECT *
FROM emple,depart;

-- join desde un producto cartesiano hecho de forma antigua

SELECT *
FROM emple,depart
WHERE emple.dept_no=depart.dept_no;

-- inner join actual se supone que esta optimizado en cuanto a tiempo, por lo tanto es mejor utilizarlo

SELECT *
FROM emple
JOIN depart ON emple.dept_no=depart.dept_no;

-- esto no debemos realizarlo nunca, es preferible utilizar el where para el filtrado de las tablas
SELECT *
FROM emple
JOIN depart WHERE emple.dept_no=depart.dept_no;

-- utilizando using los campos de combinacion tienen que llamarse igual
SELECT *
FROM emple
JOIN depart USING(dept_no);