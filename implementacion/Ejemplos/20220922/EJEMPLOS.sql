﻿/*
  CONSULTAS DE SELECCION
  FECHA : 16/03/2022
 */

  USE empresas;

/*
  Ejemplo 1
  LISTAR TODOS LOS REGISTROS DE LA TABLA PERSONAS
*/

  SELECT * FROM personas; -- LA TABLA SIN ALIAS

  SELECT * FROM personas AS p; -- LA TABLA CON ALIAS Y LA CLAUSULA AS (OPCIONAL)
  
  SELECT * FROM personas p; -- LA TABLA CON ALIAS Y SIN CLAUSULA AS



SELECT p.id,
       p.nombre,
       p.apellidos,
       p.poblacion,
       p.fecha,
       p.dato1,
       p.dato2,
       p.dato3,
       p.fechaTrabaja,
       p.empresa FROM personas p; -- LA TABLA COLOCANDO LOS CAMPOS (NO UTILIZO *) Y CON ALIAS

SELECT personas.id,
       personas.nombre,
       personas.apellidos,
       personas.poblacion,
       personas.fecha,
       personas.dato1,
       personas.dato2,
       personas.dato3,
       personas.fechaTrabaja,
       personas.empresa FROM personas; -- LA TABLA CON LOS CAMPOS Y SIN ALIAS


-- EXPLICACION DE LA SINTAXIS BASICA
  SELECT 
  *  -- nombres de los campos
  FROM 
  personas -- tabla 
  p;       -- alias de la tabla

  
/*
  EJEMPLO 2
  LISTAR EL NOMBRE Y LOS APELLIDOS DE LAS PERSONAS
*/

  SELECT DISTINCT -- CON DISTINCT NO QUITA LOS REPETIDOS (NOMBRE Y APELLIDOS IGUALES)
     p.nombre,p.apellidos  -- los campos a mostrar
  FROM 
    personas p; -- tabla

  /* 
    EJEMPLO 3
    MOSTRAR EL NOMBRE Y LAS POBLACIONES DE LAS EMPRESAS
  */

    SELECT DISTINCT 
      e.nombre, e.poblacion -- LOS CAMPOS A MOSTRAR
    FROM 
      empresas.empresas e; -- LAS TABLAS QUE UTILIZAMOS


/*
  EJEMPLO 4
  MOSTRAR EL NOMBRE DE LAS EMPRESAS
*/

   SELECT DISTINCT e.nombre FROM empresas e;

/*
  EJEMPLO 5
  MOSTRRAR LA POBLACION DE LAS EMPRESAS
*/

  SELECT DISTINCT e.poblacion FROM empresas e;

/* 
  EJEMPLO 6
  MOSTRAR EL NOMBRE DE LAS PERSONAS
*/

  SELECT DISTINCT p.nombre FROM personas p;

/*
  EJEMPLO 7
  MOSTRAR LA FECHA Y EL NOMBRE DE LAS PERSONAS
*/
  SELECT DISTINCT p.fecha, p.nombre FROM personas p;

/*
  EJEMPLO 8
  MOSTRAR EL CODIGO DE LAS EMPRESAS QUE TIENEN TRABAJADORES
*/
  SELECT DISTINCT p.empresa FROM personas p;


/*
  EJEMPLO 9
  LISTAS EL NOMBRE DE LOS TRABAJADORES CUYA EMPRESA ES LA 6
*/

  SELECT DISTINCT -- SIN REPETIDOS
      p.nombre -- LOS CAMPOS A MOSTRAR
    FROM 
      personas p -- LA TABLA
    WHERE
      p.empresa=6; -- pregunta que realizo a los registros

/*
  EJEMPLO 10
  LISTAS EL NOMBRE Y CODIGO DE LOS TRABAJADORES CUYA EMPRESA ES LA 3
*/

  SELECT DISTINCT -- SIN REPETIDOS
      p.nombre,p.id -- LOS CAMPOS A MOSTRAR
    FROM 
      personas p -- LA TABLA
    WHERE
      p.empresa=3; -- pregunta que realizo a los registros

  /**
    EJEMPLO 11
    
    MOSTRAR LAS PERSONAS QUE TRABAJAN EN LA MISMA 
    EMPRESA QUE LA PERSONA CON CODIGO 1
  **/

    -- EMPRESA PARA LA QUE TRABAJA LA PERSONA CON CODIGO 1
    SELECT p.empresa FROM personas p WHERE p.id=1;

    -- MOSTRAR LAS PERSONAS QUE TRABAJAN EN LA EMPRESA 953
    SELECT * 
      FROM 
      personas p 
      WHERE 
      p.empresa=(SELECT p.empresa FROM personas p WHERE p.id=1);


    /**
      EJEMPLO 12
      NOMBRE DEL TRABAJADOR CON ID 2 Y NOMBRE DE LA EMPRESA CON ID 2
     **/

      -- NOMBRE DEL TRABAJADOR CON ID 2
      SELECT p.nombre FROM personas p WHERE p.id=2;

      -- NOMBRE DE LA EMPRESA CON CODIGO 2
      SELECT e.nombre FROM empresas e WHERE e.codigo=2;

      -- CONSULTA FINAL
      SELECT 
        (SELECT p.nombre FROM personas p WHERE p.id=2) nombrePersona,
        (SELECT e.nombre FROM empresas e WHERE e.codigo=2) nombreEmpresa;
    

  



