DROP DATABASE IF EXISTS `ajcm8.ejemplofk5`;
CREATE DATABASE IF NOT EXISTS `ajcm8.ejemplofk5`;
-- Seleccionamos la base de datos
USE `ajcm8.ejemplofk5`;

DROP TABLE if EXISTS `ajcm8.ejemplofk5`.`departamentos`;
CREATE TABLE `ajcm8.ejemplofk5`.`departamentos`(
	-- campos
	`cod-dep` INTEGER AUTO_INCREMENT,
	-- indices
	PRIMARY KEY(`cod-dep`)
);

INSERT INTO departamentos(`cod-dep`)
	VALUES (1),
			 (2),
			 (3);

DROP TABLE if EXISTS `ajcm8.ejemplofk5`.`empleados`;
CREATE TABLE `ajcm8.ejemplofk5`.`empleados`(
	-- campos
	dni VARCHAR(10),
	`cod-dpto-pertenece` INTEGER,
	-- indices
	PRIMARY KEY (dni),
	CONSTRAINT fkEmpleadosDepartamentos
		FOREIGN KEY (`cod-dpto-pertenece`) REFERENCES `ajcm8.ejemplofk5`.`departamentos`(`cod-dep`) ON DELETE RESTRICT ON UPDATE RESTRICT 
);

INSERT INTO empleados(dni,`cod-dpto-pertenece`)
	VALUES ('13765456-F',1),
			 ('20456987-D',1),
			 ('13765983-Z',1);
			 
CREATE TABLE `ajcm8.ejemplofk5`.`proyectos`(
	-- campos
	`cod-proy` INTEGER AUTO_INCREMENT,
	-- indices
	PRIMARY KEY(`cod-proy`)
);	

INSERT INTO proyectos(`cod-proy`)
	VALUES (1),
			 (2),
			 (3);		 
			 
CREATE TABLE `ajcm8.ejemplofk5`.`trabaja`(
	-- campos
	`dni-empleado` VARCHAR(10),
	`cod-proy` INTEGER,
	fecha DATE,
	-- indices
	PRIMARY KEY(`dni-empleado`,`cod-proy`),
	CONSTRAINT fkTrabajaEmpleados
		FOREIGN KEY (`dni-empleado`) REFERENCES `ajcm8.ejemplofk5`.`empleados`(dni) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkTrabajaProyectos
		FOREIGN KEY (`cod-proy`) REFERENCES `ajcm8.ejemplofk5`.`proyectos`(`cod-proy`) ON DELETE RESTRICT ON UPDATE RESTRICT 	 
);

INSERT INTO trabaja(`dni-empleado`,`cod-proy`,fecha)
	VALUES ('13765456-F',3,'1971/08/08'),
			 ('13765983-Z',1,'1900/07/01'),
			 ('20456987-D',2,'2000/01/01');
