DROP DATABASE IF EXISTS `ajcm8.ejemplofk6`;
CREATE DATABASE IF NOT EXISTS `ajcm8.ejemplofk6`;
-- Seleccionamos la base de datos
USE `ajcm8.ejemplofk6`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.ejemplofk6`.`equipo`;
CREATE TABLE `ajcm8.ejemplofk6`.`equipo`(
	-- campos
	codigo INTEGER AUTO_INCREMENT,
	descripcion VARCHAR(100),
	precio FLOAT,
	stock INTEGER,
	-- indices
	PRIMARY KEY(codigo)
);

INSERT INTO `ajcm8.ejemplofk6`.`equipo`(codigo,descripcion,precio,stock)
	Values
	(1,'PC 1',400.45,10),
	(2,'PC 2',600.99,2),
	(3,'Impresora',60.99,3);

# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.ejemplofk6`.`seccion`;
CREATE TABLE `ajcm8.ejemplofk6`.`seccion`(
	-- campos
	id INTEGER AUTO_INCREMENT,
	nombre VARCHAR(100),
	-- indices
	PRIMARY KEY(id)
);

INSERT INTO `ajcm8.ejemplofk6`.`seccion`(id,nombre)
	Values
	(1,'Standard'),
	(2,'AltaGama'),
	(3,'Perifericos');
	
DROP TABLE if EXISTS `ajcm8.ejemplofk6`.`empleados`;
CREATE TABLE `ajcm8.ejemplofk6`.`empleados`(
	-- campos
	id INTEGER AUTO_INCREMENT,
	nif VARCHAR(10),
	nombre VARCHAR(100),
	apellido1 VARCHAR(100),
	apellido2 VARCHAR(100),
	seccion INTEGER,
	-- indices
	PRIMARY KEY(id),
	constraint uk1 UNIQUE KEY(nif),
	CONSTRAINT fkEmpleadosSeccion
		FOREIGN KEY (seccion) REFERENCES `ajcm8.ejemplofk6`.`seccion`(id) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.ejemplofk6`.`empleados`(id,nif,nombre,apellido1,apellido2,seccion)
	Values
	(1,'13765343-L','Rosa','Gutierrez','Suarez',1),
	(2,'20999543-G','Pedro','Gomez','Perez',1),
	(3,'20765345-Z','David','Mendo','Juarez',2);
	
DROP TABLE if EXISTS `ajcm8.ejemplofk6`.`clientes`;
CREATE TABLE `ajcm8.ejemplofk6`.`clientes`(
	-- campos
	id INTEGER AUTO_INCREMENT,
	nif VARCHAR(10),
	nombre VARCHAR(100),
	apellido1 VARCHAR(100),
	apellido2 VARCHAR(100),
	provincia VARCHAR(100),
	domicilio VARCHAR(100),
	ciudad VARCHAR(100),
	-- indices
	PRIMARY KEY(id)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.ejemplofk6`.`clientes`(id,nif,nombre,apellido1,apellido2,provincia,domicilio,ciudad)
	Values
	(1,'20456876-D','Emilio','Santos','Gomez','Cantabria','Gran Via, 6','Santander'),
	(2,'13987634-H','Raul','Gomez','Ruiz','Madrid','Calle Alta 4','Madrid'),
	(3,'13765456-V','Rosa','de los MIlagros','Juarez','Barcelona','Las Ramblas,3','Barcelona');
	
DROP TABLE if EXISTS `ajcm8.ejemplofk6`.`tfnoCliente`;
CREATE TABLE `ajcm8.ejemplofk6`.`tfnoCliente`(
	-- campos
	cliente INTEGER,
	telefono VARCHAR(12),
	-- indices
	PRIMARY KEY(cliente,telefono),
	CONSTRAINT fkTfnoClienteClientes
		FOREIGN KEY (cliente) REFERENCES `ajcm8.ejemplofk6`.`clientes`(id) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.ejemplofk6`.`tfnoCliente`(cliente,telefono)
	Values
	(1,'942-458-532'),
	(2,'91-4758-847'),
	(3,'93-837-444');

DROP TABLE if EXISTS `ajcm8.ejemplofk6`.`componentes`;
CREATE TABLE `ajcm8.ejemplofk6`.`componentes`(
	-- campos
	codigo INTEGER AUTO_INCREMENT,
	descripcion VARCHAR(100),
	precio float,
	stock INTEGER,
	-- indices
	PRIMARY KEY(codigo) 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.ejemplofk6`.`componentes`(codigo,descripcion,precio,stock)
	Values
	(1,'micro PENTIUM',30.55,20),
	(2,'MEMORIA DDR3 4GB',45.55,2),
	(3,'Tarjeta expansion PCI Grafica VGA',60.43,3);

DROP TABLE if EXISTS `ajcm8.ejemplofk6`.`consta`;
CREATE TABLE `ajcm8.ejemplofk6`.`consta`(
	-- campos
	equipo INTEGER,
	componente INTEGER,
	cantidad INTEGER,
	-- indices
	PRIMARY KEY(equipo,componente),
	CONSTRAINT fkConstaEquipo
		FOREIGN KEY (equipo) REFERENCES `ajcm8.ejemplofk6`.`equipo`(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT, 
	CONSTRAINT fkConstaComponentes
		FOREIGN KEY (componente) REFERENCES `ajcm8.ejemplofk6`.`componentes`(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.ejemplofk6`.`consta`(equipo,componente,cantidad)
	Values
	(1,1,1),
	(1,2,5),
	(3,2,2);
	
DROP TABLE if EXISTS `ajcm8.ejemplofk6`.`compraequipos`;
CREATE TABLE `ajcm8.ejemplofk6`.`compraequipos`(
	-- campos
	idCompra INTEGER AUTO_INCREMENT,
	cliente INTEGER,
	equipo INTEGER,
	empleado INTEGER,
	-- indices
	PRIMARY KEY(idCompra),
	CONSTRAINT uk2 UNIQUE KEY(cliente,equipo,empleado),
	CONSTRAINT fkCompraEquiposClientes
		FOREIGN KEY (cliente) REFERENCES `ajcm8.ejemplofk6`.`clientes`(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkCompraEquiposEquipos
		FOREIGN KEY (equipo) REFERENCES `ajcm8.ejemplofk6`.`equipo`(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkCompraequiposEmpleados
		FOREIGN KEY (empleado) REFERENCES `ajcm8.ejemplofk6`.`empleados`(id) ON DELETE RESTRICT ON UPDATE RESTRICT 	 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.ejemplofk6`.`compraequipos`(idCompra,Cliente,equipo,empleado)
	Values
	(1,1,2,2),
	(2,3,1,3),
	(3,2,1,3);

DROP TABLE if EXISTS `ajcm8.ejemplofk6`.`compracomponentes`;
CREATE TABLE `ajcm8.ejemplofk6`.`compracomponentes`(
	-- campos
	idCompra INTEGER AUTO_INCREMENT,
	cliente INTEGER,
	equipo INTEGER,
	empleado INTEGER,
	-- indices
	PRIMARY KEY alias(idCompra),
	CONSTRAINT uk3 UNIQUE KEY (cliente,equipo,empleado),
	CONSTRAINT fkCompraComponentesClientes
		FOREIGN KEY (cliente) REFERENCES `ajcm8.ejemplofk6`.`clientes`(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkCompraComponentesEquipos
		FOREIGN KEY (equipo) REFERENCES `ajcm8.ejemplofk6`.`equipo`(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkCompraComponentesEmpleados
		FOREIGN KEY (empleado) REFERENCES `ajcm8.ejemplofk6`.`empleados`(id) ON DELETE RESTRICT ON UPDATE RESTRICT 	  
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.ejemplofk6`.`compracomponentes`(idCompra,Cliente,equipo,empleado)
	Values
	(1,1,2,2),
	(2,3,1,3),
	(3,2,1,3);

