﻿-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.22-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para ajcm8.depart
DROP DATABASE IF EXISTS `ajcm8.depart`;
CREATE DATABASE IF NOT EXISTS `ajcm8.depart` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `ajcm8.depart`;

-- Volcando estructura para tabla ajcm8.depart.depart
DROP TABLE IF EXISTS `ajcm8.depart`.`depart`;
CREATE TABLE IF NOT EXISTS `ajcm8.depart`.`depart` (
  `dept_no` int(11) NOT NULL DEFAULT 0,
  `dnombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `loc` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`dept_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=4096;

-- Volcando datos para la tabla ajcm8.depart.depart: ~6 rows (aproximadamente)
INSERT INTO `ajcm8.depart`.`depart` (`dept_no`, `dnombre`, `loc`) VALUES
	(10, 'CONTABILIDAD', 'SEVILLA'),
	(20, 'INVESTIGACIÓN', 'MADRID'),
	(30, 'VENTAS', 'BARCELONA'),
	(40, 'PRODUCCIÓN', 'BILBAO'),
	(50, 'DESARROLLO', 'SANTANDER'),
	(60, 'INFORMATICA', 'OVIEDO');

-- Volcando estructura para tabla ajcm8.depart.emple
DROP TABLE IF EXISTS `ajcm8.depart`.`emple`;
CREATE TABLE IF NOT EXISTS `ajcm8.depart`.`emple` (
  `emp_no` int(11) NOT NULL,
  `apellido` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `oficio` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dir` int(11) DEFAULT NULL,
  `fecha_alt` date DEFAULT NULL,
  `salario` int(11) DEFAULT NULL,
  `comision` int(11) DEFAULT NULL,
  `dept_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`emp_no`),
  KEY `FK_emple_depart` (`dept_no`),
  CONSTRAINT `FK_emple_depart` FOREIGN KEY (`dept_no`) REFERENCES `depart` (`dept_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AVG_ROW_LENGTH=1170;

-- Volcando datos para la tabla ajcm8.depart.emple: ~15 rows (aproximadamente)
INSERT INTO `ajcm8.depart`.`emple` (`emp_no`, `apellido`, `oficio`, `dir`, `fecha_alt`, `salario`, `comision`, `dept_no`) VALUES
	(7369, 'SÁNCHEZ', 'EMPLEADO', 7902, '2020-12-17', 1040, NULL, 20),
	(7499, 'ARROYO', 'VENDEDOR', 7698, '2020-02-20', 1500, 390, 30),
	(7521, 'SALA', 'VENDEDOR', 7698, '2010-02-22', 1625, 650, 30),
	(7566, 'JIMÉNEZ', 'DIRECTOR', 7839, '2010-04-02', 2900, NULL, 20),
	(7654, 'MARTÍN', 'VENDEDOR', 7698, '2010-09-29', 1600, 1020, 30),
	(7698, 'NEGRO', 'DIRECTOR', 7839, '2010-05-01', 3005, NULL, 30),
	(7782, 'CEREZO', 'DIRECTOR', 7839, '2010-06-09', 2885, NULL, 10),
	(7788, 'GIL', 'ANALISTA', 7566, '2010-11-09', 3000, NULL, 20),
	(7839, 'REY', 'PRESIDENTE', NULL, '2010-11-17', 4100, NULL, 10),
	(7844, 'TOVAR', 'VENDEDOR', 7698, '2010-09-08', 1350, 0, 30),
	(7876, 'ALONSO', 'EMPLEADO', 7788, '2010-09-23', 1430, NULL, 20),
	(7900, 'JIMENO', 'EMPLEADO', 7698, '2010-12-03', 1335, NULL, 30),
	(7902, 'FERNÁNDEZ', 'ANALISTA', 7566, '2010-12-03', 3000, NULL, 20),
	(7934, 'MUÑOZ', 'EMPLEADO', 7782, '2015-01-23', 1690, NULL, 10),
	(8000, 'GOMEZ', 'EMPLEADO', 7698, '2019-05-20', 2500, 1000, 50);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
