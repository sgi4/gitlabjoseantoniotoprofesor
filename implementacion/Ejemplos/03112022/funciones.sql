SET NAMES 'utf8';
USE `ajcm8.compranp`;

-- funcion parecido al procedimiento diferenciandose unicamente en que devuelve un valor

DROP FUNCTION if EXISTS f1;
DELIMITER //
CREATE FUNCTION f1()
RETURNS INT 
BEGIN
  DECLARE c1 INT DEFAULT 6;
  SET c1= 8;
RETURN c1;
END//
DELIMITER ;

SET @c2=f1();
SELECT @c2;

DROP FUNCTION if EXISTS f2;
DELIMITER //
CREATE FUNCTION f2(n1 INT,n2 INT)
RETURNS INT 
BEGIN
  DECLARE suma INT DEFAULT 0;
  SET suma= n1+n2;
RETURN suma;
END//
DELIMITER ;

SET @c2=f2(1,3);
SELECT @c2;
SELECT f2(1,3);

DROP FUNCTION if EXISTS f2;
DELIMITER //
CREATE FUNCTION f2(n1 INT,n2 INT)
RETURNS INT DETERMINISTIC -- para indicar cuando la funcion solo devuelve valores escalares
BEGIN
  DECLARE suma INT DEFAULT 0;
  SET suma= n1+n2;
RETURN suma;
END//
DELIMITER ;

SET @c2=f2(1,3);
SELECT @c2;
SELECT f2(1,3);

SELECT f2(productos.precio,10) FROM productos;

DROP FUNCTION if EXISTS f3;
DELIMITER //
CREATE FUNCTION f3(n1 INT,n2 INT, n3 FLOAT)
RETURNS FLOAT
BEGIN
  DECLARE suma FLOAT DEFAULT 0;
  SET suma= n1*n2*n3;
RETURN suma;
END//
DELIMITER ;

SET @c2=f3(1,3,3.3);
SELECT @c2;
SELECT f3(1,3,3.3);

DROP FUNCTION if EXISTS f4;
DELIMITER //
CREATE FUNCTION f4(n1 VARCHAR(20),n2 VARCHAR(20))
RETURNS VARCHAR(41)
BEGIN
  DECLARE suma VARCHAR(41) DEFAULT '';
  SET suma= CONCAT(n1,',',n2);
RETURN suma;
END//
DELIMITER ;

SET @c2=f4('Hola','Mundo');
SELECT @c2;
SELECT f4('Hola','Mundo');

DROP FUNCTION if EXISTS f2;
DELIMITER //
CREATE FUNCTION f2(n1 INT,n2 INT)
RETURNS INT
BEGIN
  DECLARE suma INT DEFAULT 0;
  SET suma= n1+n2;
  SELECT suma;-- tenemos un error porque una funcion no puede proyectar o mostrar un resultado intermedio desde dentro de la funcion;
RETURN suma;
END//
DELIMITER ;

SET @c2=f2(1,3);
SELECT @c2;
SELECT f2(1,3);