DROP DATABASE IF EXISTS `ajcm8.disparadores2`;
CREATE DATABASE `ajcm8.disparadores2`;
USE `ajcm8.disparadores2`;

CREATE TABLE empresas(
  empresa varchar(100),
  trabajadores int DEFAULT 0,
  fechaUltimo datetime,
  fechaEntrada date,
  diaSemana varchar(100),
  diaMes int,
  mes varchar(100),
  mesNumero int,
  anno int,
  PRIMARY KEY(empresa)
);

CREATE TABLE clientes(
  codigo int AUTO_INCREMENT,
  referencia varchar(20),
  nombre varchar(100),
  empresa varchar(100),
  email varchar(100),
  fechaNacimiento date,
  fecha datetime DEFAULT (CURRENT_TIMESTAMP),
  PRIMARY KEY(codigo),
  CONSTRAINT clientesEmpresas FOREIGN KEY (empresa) REFERENCES empresas(empresa) ON DELETE RESTRICT on UPDATE RESTRICT
);

-- Crear un disparador para que cuando inserte un empresa calcule 
-- diasemana
-- mes
-- mesNumero
-- anno
-- de la fecha de entrada

DROP TRIGGER if EXISTS biempresa;
delimiter //
CREATE OR REPLACE TRIGGER biempresa
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
BEFORE INSERT 
	ON empresas
	FOR EACH ROW
	-- trigger order  
BEGIN
  SET @@lc_time_names='es_ES';
  SET NEW.diaSemana=DAYNAME(NEW.fechaEntrada);
  SET NEW.mes=MONTHNAME(NEW.fechaEntrada);
  SET NEW.diaMes=DAY(NEW.fechaEntrada);
  SET NEW.mesNumero=MONTH(NEW.fechaEntrada);
  SET NEW.anno=YEAR(NEW.fechaEntrada);
END//
delimiter ;

-- Crear un disparador para que cuando actualice una empresa me coloque correctamente
-- diasemana
-- mes
-- mesNumero
-- anno
-- de la fecha de entrada

DROP TRIGGER if EXISTS buempresa;
delimiter //
CREATE OR REPLACE TRIGGER buempresa
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
BEFORE UPDATE
	ON empresas
	FOR EACH ROW
	-- trigger order  
BEGIN
  SET @@lc_time_names='es_ES';
  SET NEW.diaSemana=DAYNAME(NEW.fechaEntrada);
  SET NEW.mes=MONTHNAME(NEW.fechaEntrada);
  SET NEW.diaMes=DAY(NEW.fechaEntrada);
  SET NEW.mesNumero=MONTH(NEW.fechaEntrada);
  SET NEW.anno=YEAR(NEW.fechaEntrada);
END//
delimiter ;

-- modificar el disparador anterior para que en caso de que al actualizar la empresa no se modidique la fecha de entrada
-- no realice ningun calculo

DROP TRIGGER if EXISTS buempresa;
delimiter //
CREATE OR REPLACE TRIGGER buempresa
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
BEFORE UPDATE
	ON empresas
	FOR EACH ROW
	-- trigger order  
BEGIN
  IF(NEW.fechaEntrada<>OLD.fechaEntrada)then
  		SET @@lc_time_names='es_ES';
  		SET NEW.diaSemana=DAYNAME(NEW.fechaEntrada);
  		SET NEW.mes=MONTHNAME(NEW.fechaEntrada);
  		SET NEW.diaMes=DAY(NEW.fechaEntrada);
  		SET NEW.mesNumero=MONTH(NEW.fechaEntrada);
  		SET NEW.anno=YEAR(NEW.fechaEntrada);
  END if;
END//
delimiter ;

INSERT INTO empresas (empresa, fechaEntrada)
  VALUES ('alpe', CURDATE()),('carrefour','2022/12/2'),('seur','2021/11/2');

UPDATE empresas
  SET empresas.fechaEntrada= CURDATE() WHERE empresas.empresa='alpe';
UPDATE empresas
	SET empresas.fechaEntrada='1998/12/2' WHERE empresas.empresa='carrefour';	
UPDATE empresas
   SET empresas.fechaEntrada='1998/11/2' WHERE empresas.empresa='seur';

DROP TRIGGER if EXISTS bicliente;
delimiter //
CREATE OR REPLACE TRIGGER bicliente
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
BEFORE INSERT 
	ON clientes
	FOR EACH ROW
	-- trigger order  
BEGIN
  /*DECLARE contador INTEGER DEFAULT 0;
  SET @copia=@@information_schema_stats_expiry;-- mysql nola tiene
  SET @@information_schema_stats_expiry=0;
  SELECT Autoincrement INTO contador
  FROM information_shema TABLES 
  WHERE TABLE_NAME='clientes' AND table_schema ='ajcm8.disparadore2';*/
  DECLARE contador INTEGER DEFAULT 1;
  SELECT MAX(clientes.codigo) INTO contador FROM clientes LIMIT 1;
  if((contador IS NULL)AND(contador<1)) then SET contador=contador+1; END if;
  if(contador<>LAST_INSERT_ID()) then SET contador=contador+1; END if;
  SET new.referencia=CONCAT(NEW.nombre,contador+1);
--  SET @@information_schema_stats_expiry=@copia;
END//
delimiter ;
  
DROP TRIGGER if EXISTS bucliente;
delimiter //
CREATE OR REPLACE TRIGGER bucliente
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
BEFORE UPDATE
	ON clientes
	FOR EACH ROW
	-- trigger order  
BEGIN
   SET new.referencia=CONCAT(new.nombre,new.codigo);
END//
delimiter ;

INSERT into clientes (nombre, empresa, fechaNacimiento)
  VALUES ('ramon', 'alpe', '1980/1/2');
INSERT into clientes (nombre, empresa, fechaNacimiento)
  VALUES ('ana','seur','1978/1/2');
INSERT into clientes (nombre, empresa, fechaNacimiento)
  VALUES ('jose', 'alpe', '1980/1/2');

UPDATE clientes
	SET clientes.fechaNacimiento='1998/1/2' WHERE clientes.nombre='ramon';
	
UPDATE clientes
	SET clientes.fechaNacimiento='1998/1/2' WHERE clientes.nombre='ana';

UPDATE clientes
	SET clientes.fechaNacimiento='1998/1/2' WHERE clientes.nombre='jose';

DELETE from clientes;

DELETE from clientes
WHERE clientes.codigo=18;
