SET NAMES 'utf8';
USE `ajcm8.compranp`;

--- poner todos los totales de compranp a 0
-- -----------------------------
-- consulta de actualizacion
-- -----------------------------
SELECT * FROM compranp; -- miramos la tabla y lo campos de compranp

UPDATE compranp
	SET compranp.total=0;
	
-- colocar los totales de compranp a null
-- consulta de actualizacion
SELECT * FROM compranp;

UPDATE compranp
	SET compranp.total=NULL
WHERE compranp.total=0;

-- EJEMPLO2

ALTER TABLE compran ADD COLUMN numeroClientes INT DEFAULT 0;
SHOW FIELDS FROM compran;

-- herramientas similares a comandos de despuracion simplemente para comprobar
SHOW FIELDS FROM productos;

EXPLAIN productos;

DESCRIBE productos;

-- realizamos la consulta de seleccion que nos muestre lo que nos pide la de actualizacion
SELECT compran.codPro,COUNT(*) AS Numero
FROM compran
GROUP BY compran.codPro;


/*SELECT compran.codPro,COUNT(*) AS Numero
FROM compran
JOIN compranp
ON compran.idCompran=compranp.idCompran
GROUP BY compran.codPro;*/

SHOW FIELDS FROM compran;
SHOW FIELDS FROM compranp;


SELECT * FROM compran
JOIN (
		SELECT compran.codPro,COUNT(*) AS Numero
		FROM compran
		GROUP BY compran.codPro
		)AS c1
ON c1.codPro=compran.codPro;

-- ahora lo convertions en una consulta de actualizacion

UPDATE compran
JOIN (
		SELECT compran.codPro,COUNT(*) AS Numero
		FROM compran
		GROUP BY compran.codPro
		)AS c1
ON c1.codPro=compran.codPro
SET compran.numeroClientes=c1.Numero;

-- pero vemos que en la tabla compran cundosale el mismo producto tambien sale cuantos clientes lo han comprado
-- o sea se repite, lo mejor es meter el campo numeroClientes en la tabla productos
ALTER TABLE productos DROP COLUMN numeroClientes;

ALTER TABLE productos ADD COLUMN numeroClientes INT DEFAULT 0;

UPDATE productos
JOIN (
		SELECT compran.codPro,COUNT(*) AS Numero
		FROM compran
		GROUP BY compran.codPro
		)AS c1
ON c1.codPro=productos.codPro
SET productos.numeroClientes=c1.Numero;

-- 08/11/2022
ALTER TABLE productos ADD COLUMN numeroProductos INT DEFAULT 0;

SELECT * FROM compranp
JOIN (
		SELECT compranp.idCompran,Sum(compranp.cantidad) AS Numero
		FROM compranp
		GROUP BY compranp.idCompran
		)AS c1
ON c1.idCompran=compranp.idCompran;--mal

UPDATE productos
JOIN (
		SELECT compranp.idCompran,Sum(compranp.cantidad) AS Numero
		FROM compranp
		GROUP BY compranp.idCompran
		)AS c1
ON c1.idCompran=productos.codPro
SET productos.numeroProductos=c1.Numero;--mal

SELECT *
FROM compran
INNER JOIN compranp 
ON compran.idCompran=compranp.idCompran;


SELECT compran.codPro,SUM(compranp.cantidad) AS Numero
FROM compran
INNER JOIN compranp 
ON compran.idCompran=compranp.idCompran
GROUP BY compran.codPro;

UPDATE productos
INNER JOIN (
				SELECT compran.codPro,SUM(compranp.cantidad) AS Numero
				FROM compran
				INNER JOIN compranp 
				ON compran.idCompran=compranp.idCompran
				GROUP BY compran.codPro
				)AS c1
ON productos.codPro=c1.codPro
SET productos.numeroProductos=c1.Numero;
-- _____________________________________________________________________________________
SELECT compran.codPro,compranp.cantidad
FROM compranp
INNER JOIN compran
ON compranp.idCompran=compran.idCompran;

SELECT productos.codPro,sum(productos.precio*c1.cantidad) AS total
FROM productos
INNER JOIN (
				SELECT compran.codPro,compranp.cantidad
				FROM compranp
				INNER JOIN compran
				ON compranp.idCompran=compran.idCompran
				) AS c1
ON productos.codPro=c1.codPro
GROUP BY productos.codPro;

SELECT productos.codPro,(productos.precio*c1.cantidad) AS total
FROM productos
INNER JOIN (
				SELECT compran.codPro,compranp.cantidad
				FROM compranp
				INNER JOIN compran
				ON compranp.idCompran=compran.idCompran
				) AS c1
ON productos.codPro=c1.codPro;

SELECT *
from compranp
JOIN (
		SELECT c1.idCompran,productos.codPro,(productos.precio*c1.cantidad) AS total
		FROM productos
		INNER JOIN (
						SELECT compran.idCompran,compran.codPro,compranp.cantidad
						FROM compranp
						INNER JOIN compran
						ON compranp.idCompran=compran.idCompran
						) AS c1
		ON productos.codPro=c1.codPro
		)AS c2
ON compranp.idCompran=c2.idCompran;

UPDATE compranp
JOIN (
		SELECT c1.idCompran,productos.codPro,(productos.precio*c1.cantidad) AS total
		FROM productos
		INNER JOIN (
						SELECT compran.idCompran,compran.codPro,compranp.cantidad
						FROM compranp
						INNER JOIN compran
						ON compranp.idCompran=compran.idCompran
						) AS c1
		ON productos.codPro=c1.codPro
		)AS c2
ON compranp.idCompran=c2.idCompran
SET compranp.total=c2.total;

SELECT * FROM compranp;

SELECT productos.codPro,productos.nombre,if(productos.precio<=10,"BARATO","CARO") AS Coste
FROM productos;

SELECT *
from compranp
JOIN (
		SELECT c1.idCompran,productos.codPro,(productos.precio*c1.cantidad*if(c1.cantidad<=10,1,if((c1.cantidad>10)and(c1.cantidad<=30),0.9,if(c1.cantidad>30,0.7,1)))) AS total
		FROM productos
		INNER JOIN (
						SELECT compran.idCompran,compran.codPro,compranp.cantidad
						FROM compranp
						INNER JOIN compran
						ON compranp.idCompran=compran.idCompran
						) AS c1
		ON productos.codPro=c1.codPro
		)AS c2
ON compranp.idCompran=c2.idCompran;

UPDATE compranp
JOIN (
		SELECT c1.idCompran,productos.codPro,(productos.precio*c1.cantidad*if(c1.cantidad<=10,1,if((c1.cantidad>10)and(c1.cantidad<=30),0.9,if(c1.cantidad>30,0.7,1)))) AS total
		FROM productos
		INNER JOIN (
						SELECT compran.idCompran,compran.codPro,compranp.cantidad
						FROM compranp
						INNER JOIN compran
						ON compranp.idCompran=compran.idCompran
						) AS c1
		ON productos.codPro=c1.codPro
		)AS c2
ON compranp.idCompran=c2.idCompran
SET compranp.total=c2.total;

UPDATE compranp
JOIN (
		SELECT c1.idCompran,c1.cantidad,productos.precio
		FROM productos
		INNER JOIN (
						SELECT compran.idCompran,compran.codPro,compranp.cantidad
						FROM compranp
						INNER JOIN compran
						ON compranp.idCompran=compran.idCompran
						) AS c1
		ON productos.codPro=c1.codPro
		)AS c2
ON compranp.idCompran=c2.idCompran
SET compranp.total=c2.cantidad*c2.precio*(if(c2.cantidad<=10,1,if((c2.cantidad>10)AND(c2.cantidad<=30),0.9,if(c2.cantidad>30,0.7,1))));

SELECT * FROM compranp;
-- ------------------------------------------
-- consultas de borrado o eliminacion
-- ------------------------------------------
SELECT *
FROM productos
WHERE productos.precio>=20;

DELETE
FROM productos
WHERE productos.precio>=20;

DELETE productos.*
FROM productos,compranp
WHERE productos.precio>=20;

DELETE productos
FROM productos,compranp
WHERE productos.precio>=20;

DELETE p.*
FROM productos AS p
WHERE p.precio>=20;
-- eliminar las compras cuyos productos el precio es mayor o igual 20

SELECT *
FROM compran
INNER JOIN productos
ON productos.codPro=compran.codPro
WHERE productos.precio>=20;

DELETE compran.*
FROM compran
INNER JOIN productos
ON productos.codPro=compran.codPro
WHERE productos.precio>=20;

-- eliminar las compras de los clientes de santander
SELECT *
FROM compran
INNER JOIN clientes
ON compran.codCli=clientes.codCli
WHERE clientes.poblacion IN (SELECT poblacion.idPob
										FROM poblacion
										WHERE poblacion.nombre="Santander");

DELETE compran
FROM compran
INNER JOIN clientes
ON compran.codCli=clientes.codCli
WHERE clientes.poblacion IN (SELECT poblacion.idPob
										FROM poblacion
										WHERE poblacion.nombre="Santander");
-- ----------------------------------------
-- consultas de datos anexados
-- ----------------------------------------

-- creamos una nueva tabla
DROP TABLE if EXISTS productosvendidos;
CREATE TABLE productosVendidos(
id INT AUTO_INCREMENT,
nombre VARCHAR(50),
PRIMARY KEY (id)
);
-- introducimos los registros con los datos en la tabla

INSERT INTO productosvendidos (nombre)
			SELECT DISTINCT productos.nombre
			FROM productos
			INNER JOIN compran
			ON productos.codPro=compran.codPro;
			
DELETE FROM productosvendidos;
TRUNCATE productosvendidos; -- reseteamos los indices los autoincremento comienza en o desde uno

-- --------------------------------------------------------------------------
-- consulta de creacion de tabla para consultas de datos anexados
-- --------------------------------------------------------------------------

CREATE TABLE productosvendidos as
			SELECT DISTINCT productos.nombre
			FROM productos
			INNER JOIN compran
			ON productos.codPro=compran.codPro;

CREATE TABLE productosVendidos(
			id INT AUTO_INCREMENT,
			nombre VARCHAR(50),
			PRIMARY KEY (id)
			)	AS 
			SELECT DISTINCT productos.nombre
			FROM productos
			INNER JOIN compran
			ON productos.codPro=compran.codPro;

DELETE FROM productosvendidos;
TRUNCATE productosvendidos; -- reseteamos los indices los autoincremento comienza en o desde uno
	
-- crear una tabla con los productos que no se han vendido
DROP TABLE if EXISTS productosnovendidos;
INSERT INTO productos(codPro,nombre,precio,numeroClientes)-- introducimos un producto nuevo por que se han vendido todos los productos
VALUES (2000,'Nuevo',200,3);
CREATE TABLE productosnovendidos(
			id INT AUTO_INCREMENT,
			nombre VARCHAR(50),
			PRIMARY KEY (id)
			)	AS 
			SELECT DISTINCT productos.nombre
			FROM productos
			LEFT JOIN compran
			ON productos.codPro=compran.codPro
			WHERE compran.codPro IS NULL;
			
DELETE FROM productosnovendidos;
TRUNCATE productosnovendidos; -- reseteamos los indices los autoincremento comienza en o desde uno

CREATE OR REPLACE TABLE productosnovendidos(
	id INT AUTO_INCREMENT,
	nombre VARCHAR(50),
	PRIMARY KEY(id)
	)	AS 
			SELECT DISTINCT productos.nombre
			FROM productos
			LEFT JOIN compran
			ON productos.codPro=compran.codPro
			WHERE compran.codPro IS NULL;
	
SELECT * FROM productos;
SELECT * FROM compran;
SELECT * FROM compranp;
SELECT * FROM productosvendidos;
SELECT * FROM productosnovendidos;