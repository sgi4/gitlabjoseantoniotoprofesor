SET NAMES 'utf8';
USE `ajcm8.compranp`;

--- poner todos los totales de compranp a 0
-- consulta de actualizacion
SELECT * FROM compranp; -- miramos la tabla y lo campos de compranp

UPDATE compranp
	SET compranp.total=0;
	
-- colocar los totales de compranp a null
-- consulta de actualizacion
SELECT * FROM compranp;

UPDATE compranp
	SET compranp.total=NULL
WHERE compranp.total=0;

-- EJEMPLO2

ALTER TABLE compran ADD COLUMN numeroClientes INT DEFAULT 0;
SHOW FIELDS FROM compran;

-- herramientas similares a comandos de despuracion simplemente para comprobar
SHOW FIELDS FROM productos;

EXPLAIN productos;

DESCRIBE productos;

-- realizamos la consulta de seleccion que nos muestre lo que nos pide la de actualizacion
SELECT compran.codPro,COUNT(*) AS Numero
FROM compran
GROUP BY compran.codPro;


/*SELECT compran.codPro,COUNT(*) AS Numero
FROM compran
JOIN compranp
ON compran.idCompran=compranp.idCompran
GROUP BY compran.codPro;*/

SHOW FIELDS FROM compran;
SHOW FIELDS FROM compranp;


SELECT * FROM compran
JOIN (
		SELECT compran.codPro,COUNT(*) AS Numero
		FROM compran
		GROUP BY compran.codPro
		)AS c1
ON c1.codPro=compran.codPro;

-- ahora lo convertions en una consulta de actualizacion

UPDATE compran
JOIN (
		SELECT compran.codPro,COUNT(*) AS Numero
		FROM compran
		GROUP BY compran.codPro
		)AS c1
ON c1.codPro=compran.codPro
SET compran.numeroClientes=c1.Numero;

-- pero vemos que en la tabla compran cundosale el mismo producto tambien sale cuantos clientes lo han comprado
-- o sea se repite, lo mejor es meter el campo numeroClientes en la tabla productos
ALTER TABLE productos DROP COLUMN numeroClientes;

ALTER TABLE productos ADD COLUMN numeroClientes INT DEFAULT 0;

UPDATE productos
JOIN (
		SELECT compran.codPro,COUNT(*) AS Numero
		FROM compran
		GROUP BY compran.codPro
		)AS c1
ON c1.codPro=productos.codPro
SET productos.numeroClientes=c1.Numero;
