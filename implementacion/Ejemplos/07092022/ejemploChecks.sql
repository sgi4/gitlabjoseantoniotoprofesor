DROP DATABASE IF EXISTS `ajcm8.checks`;
CREATE DATABASE IF NOT EXISTS `ajcm8.checks`;
-- Seleccionamos la base de datos
USE `ajcm8.checks`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.checks`.`clientes`;
CREATE TABLE `ajcm8.checks`.`clientes`(
	-- campos
	id INTEGER AUTO_INCREMENT COMMENT 'Clave Principal', #campo con un comentario
	nombre VARCHAR(200),
	edad INTEGER DEFAULT 18,
	fechaNacimiento DATE NOT NULL,
	correo VARCHAR(100) NOT NULL,
	-- indices
	PRIMARY KEY(id),
	UNIQUE KEY(correo),
	CHECK(edad>=18 and edad<=65) #formato de check
	-- CHECK(id>edad) #formato de una aserción
);

INSERT INTO `ajcm8.checks`.`clientes`(id,nombre,edad,fechaNacimiento,correo)
	Values
	(1,'Luis Perez Galdos',DEFAULT,'2000/01/01','aa@aa.com'),
	(2,'Rosa Maria Gomez',65,'1979/01/01','ab@ab.com'),
	(3,'Pedro Gomez Suarez',18,'1983/01/01','ac@ac.com');