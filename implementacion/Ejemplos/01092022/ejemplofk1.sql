# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `ajcm8.ejemplofk1`;
CREATE DATABASE IF NOT EXISTS `ajcm8.ejemplofk1`;
-- Seleccionamos la base de datos
USE `ajcm8.ejemplofk1`;

CREATE TABLE if NOT EXISTS `ajcm8.ejemplofk1`.`autores`(
   idAutores INTEGER AUTO_INCREMENT,
   nombre VArCHAR(100),
   apellidos VARCHAR(100),
   PRIMARY KEY (idAutores)
);

CREATE TABLE if NOT EXISTS `ajcm8.ejemplofk1`.`libros`(
	idLibro INTEGER AUTO_INCREMENT,
	titulo VARCHAR(100) NOT NULL,
	autor INTEGER,`ajcm8.ejemplofk1`
	PRIMARY KEY (idLibro),
	   CONSTRAINT fkLibroAutor
	FOREIGN KEY (autor) REFERENCES `ajcm8.ejemplofk1`.`autores`(idAutores)  ON DELETE RESTRICT ON UPDATE RESTRICT  
);

INSERT INTO `ajcm8.ejemplofk1`.`autores`(idAutores,nombre,apellidos)
	VALUES(1,'Galilea','Jimenez'),
			(2,'Betsaida','Perez'),
			(3,'Patmos','Suarez'),
			(4,'Jerusalen','Gomez');

INSERT INTO `ajcm8.ejemplofk1`.`libros`(idLibro,titulo,autor)
	VALUES(1,'El eterno',1),
			(2,'Si volvieras alguna vez',4),
			(3,'Juegos de Adultos',2),
			(4,'La vecina de abajo',3);
	
#SELECT * FROM autores AS c;
#SELECT * FROM libros AS d;