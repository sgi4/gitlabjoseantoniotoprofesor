-- Volcando estructura de base de datos para ajcm8.personal
DROP DATABASE IF EXISTS `ajcm8.personal`;
CREATE DATABASE IF NOT EXISTS `ajcm8.personal` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ajcm8.personal`;

-- Volcando estructura para tabla ajcm8.personal.poblacion
DROP TABLE IF EXISTS `poblacion`;
CREATE TABLE IF NOT EXISTS `poblacion` (
  `idPoblacion` int(11) NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(100) NOT NULL,
  PRIMARY KEY (`idPoblacion`)
);

-- Volcando datos para la tabla ajcm8.personal.poblacion: ~4 rows (aproximadamente)
INSERT INTO `poblacion` (`idPoblacion`, `ciudad`) VALUES
	(1, 'Galilea'),
	(2, 'Betsaida'),
	(3, 'Patmos'),
	(4, 'Jerusalen');
	
-- Volcando estructura para tabla ajcm8.personal.persona
DROP TABLE IF EXISTS `persona`;
CREATE TABLE IF NOT EXISTS `persona` (
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `poblacion` int(11) NOT NULL,
  PRIMARY KEY (`idPersona`),
  CONSTRAINT `fkPersonaPoblacion` FOREIGN KEY (`poblacion`) REFERENCES `poblacion` (`idPoblacion`)
);

-- Volcando datos para la tabla ajcm8.personal.persona: ~4 rows (aproximadamente)
INSERT INTO `persona` (`idPersona`, `nombre`, `apellidos`, `poblacion`) VALUES
	(1, 'Pedro', 'Dominguez', 1),
	(2, 'Juan', 'Gomez', 4),
	(3, 'Rosa', 'Suarez', 2),
	(4, 'Elena', 'Diez', 3);