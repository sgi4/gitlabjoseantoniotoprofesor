# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `ajcm8.ejemplofk2`;
CREATE DATABASE IF NOT EXISTS `ajcm8.ejemplofk2`;
-- Seleccionamos la base de datos
USE `ajcm8.ejemplofk2`;

CREATE TABLE if NOT EXISTS `ajcm8.ejemplofk2`.`alumnos`(
   mat INTEGER AUTO_INCREMENT,
   nombre VARCHAR(100),
   grupo VARCHAR(100),
   PRIMARY KEY (mat) 
);

CREATE TABLE if NOT EXISTS `ajcm8.ejemplofk2`.`practicas`(
	`p#` INTEGER AUTO_INCREMENT,
	titulo VARCHAR(100) NOT NULL,
	dificultad INTEGER,
	PRIMARY KEY (`p#`)
);

CREATE TABLE if NOT EXISTS `ajcm8.ejemplofk2`.`realiza`(
	matAlumno INTEGER ,
	`p#Practica` INTEGER,
	Primary KEY (matAlumno,`p#Practica`),
	CONSTRAINT fkRealizaAlumno
	FOREIGN KEY (matAlumno) REFERENCES `ajcm8.ejemplofk2`.`alumnos`(mat)  ON DELETE RESTRICT ON UPDATE RESTRICT,
	 CONSTRAINT fkRealizaPractica
	FOREIGN KEY (`p#Practica`) REFERENCES `ajcm8.ejemplofk2`.`practicas`(`p#`)  ON DELETE RESTRICT ON UPDATE RESTRICT  
);

INSERT INTO `ajcm8.ejemplofk2`.`alumnos`(mat,nombre,grupo)
	VALUES(1,'Galilea Jimenez','Administracion'),
			(2,'Betsaida Perez','Programacion'),
			(3,'Patmos Suarez','Empresas'),
			(4,'Jerusalen Gomez','Diseño Grafico');

INSERT INTO `ajcm8.ejemplofk2`.`practicas`(`p#`,titulo,dificultad)
	VALUES(1,'El eterno',1),
			(2,'Si volvieras alguna vez',4),
			(3,'Juegos de Adultos',2),
			(4,'La vecina de abajo',3);

INSERT INTO `ajcm8.ejemplofk2`.`realiza`(matAlumno,`p#Practica`)
	VALUES(3,3),
			(2,1),
			(1,4),
			(4,2);

	
#SELECT * FROM autores AS c;
#SELECT * FROM libros AS d;