SET NAMES 'utf8';
USE `ajcm8.ventas1`;

-- consulta de actualizacion
--  añadimos el campo total a la tabla ventas
ALTER TABLE ventas 
	ADD COLUMN total FLOAT DEFAULT 0;
	
UPDATE ventas
	JOIN productos
	ON ventas.CodProducto=productos.IdProducto
	SET ventas.total=productos.Precio * ventas.Kilos;

UPDATE productos
	JOIN ventas
	ON ventas.CodProducto=productos.IdProducto
	SET ventas.total=productos.Precio* ventas.Kilos * (1-0.1)
	WHERE ventas.CodProducto IN (SELECT productos.IdProducto FROM productos WHERE productos.NomProducto='manzana');

-- creacion de tablas y datos anexados	
CREATE OR REPLACE TABLE verduras AS
	SELECT productos.* FROM productos
	WHERE productos.IdGrupo IN (SELECT grupos.IdGrupo FROM grupos WHERE grupos.NombreGrupo='Verduras');
	
-- or

CREATE OR REPLACE TABLE verduras(
	id INTEGER AUTO_INCREMENT,
	nombre VARCHAR(100),
	PRIMARY KEY(id)
) AS SELECT productos.NomProducto as nombre FROM productos WHERE productos.IdGrupo IN (SELECT grupos.IdGrupo FROM grupos WHERE grupos.NombreGrupo='Verduras');

-- or 
CREATE OR REPLACE TABLE verduras(
	id INTEGER AUTO_INCREMENT,
	nombre VARCHAR(100),
	PRIMARY KEY(id)
);

-- or consulta de datos anexados

INSERT INTO verduras(verduras.nombre) 
	SELECT productos.NomProducto as nombre FROM productos WHERE productos.IdGrupo IN (SELECT grupos.IdGrupo FROM grupos WHERE grupos.NombreGrupo='Verduras');
	
-- creacion de tabla

CREATE OR REPLACE TABLE verdurasVendidas(
	id INTEGER AUTO_INCREMENT,
	NombreGrupo VARCHAR(50),
	numeroProductosVendidos INTEGER,
	PRIMARY KEY(id)
) AS
	SELECT grupos.NombreGrupo,COUNT(*) as numeroProductosVendidos FROM grupos
	JOIN productos ON grupos.IdGrupo=productos.IdGrupo
	JOIN ventas ON productos.IdProducto=ventas.CodProducto
	GROUP BY grupos.NombreGrupo;

-- or

INSERT INTO verdurasVendidas(NombreGrupo,numeroProductosVendidos)
	SELECT grupos.NombreGrupo,COUNT(*) as numeroProductosVendidos FROM grupos
	JOIN productos ON grupos.IdGrupo=productos.IdGrupo
	JOIN ventas ON productos.IdProducto=ventas.CodProducto
	GROUP BY grupos.NombreGrupo;

-- consultas de eliminacion

DELETE FROM ventas
 	WHERE ventas.Kilos BETWEEN 300 AND 500;
 	
DELETE ventas.* 
FROM productos
JOIN ventas ON ventas.CodProducto=productos.IdProducto
WHERE productos.NomProducto='lechugas';

DELETE FROM ventas.*
USING productos
JOIN ventas ON ventas.CodProducto=productos.IdProducto
WHERE productos.NomProducto='lechugas';

-- repaso aplicación

DROP DATABASE if EXISTS `ajcm8.cms`;
CREATE DATABASE `ajcm8.cms`;
USE `ajcm8.cms`;

CREATE OR REPLACE TABLE entradas(
	id INTEGER AUTO_INCREMENT,
	titulo VARCHAR(100),
	texto TEXT,
	fecha DATETIME DEFAULT CURRENT_TIMESTAMP,
	foto VARCHAR(100),
	PRIMARY KEY(id)
);

