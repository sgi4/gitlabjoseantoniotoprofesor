SET NAMES 'utf8';
DROP DATABASE if EXISTS `ajcm8.seleccion`;
CREATE DATABASE `ajcm8.seleccion`;
USE `ajcm8.seleccion`;

DROP PROCEDURE if EXISTS p1;
DELIMITER //
CREATE PROCEDURE p1(numero1 INT, numero2 INT)
BEGIN
	DECLARE salida VARCHAR(30) DEFAULT "";
	if (numero1>numero2) then 
		SET salida = "numero 1";
	else
	   SET salida = "numero 2";	
	END if;	
	SELECT salida;
END//
DELIMITER ;

CALL p1(3,4);
CALL p1(4,3);

DROP PROCEDURE if EXISTS p2;
delimiter //
CREATE PROCEDURE p2(n1 INT,n2 int)
BEGIN
	DECLARE salida VARCHAR(30) DEFAULT "";
	if (n1>n2) then 
		SET salida= "es mayor numero 1";
	ELSE 
		SET salida= "es mayor o igual numero 2";
	END if;
	SELECT salida;
END//
delimiter ;

CALL p2(3,4);
CALL p2(4,3);

DROP PROCEDURE if EXISTS p3;
delimiter //
CREATE PROCEDURE p3(n1 INT,n2 int)
BEGIN
	DECLARE salida VARCHAR(30) DEFAULT "";
	if (n1>n2) then 
		SET salida= "es mayor numero 1";
	ELSE if (n1<n2) then SET salida="es mayor numero 2"; ELSE SET salida="los numeros son iguales"; END if;
	END if;
	SELECT salida;
END//
delimiter ;

CALL p3(3,4);
CALL p3(4,3);
CALL p3(4,4);

DROP FUNCTION if EXISTS f1;
delimiter //
CREATE FUNCTION f1(n1 INT,n2 INT)
RETURNS VARCHAR(30)
BEGIN
	DECLARE salida VARCHAR(30) DEFAULT "";
	if (n1>n2) then 
		SET salida= "es mayor numero 1";
	ELSE if (n1<n2) then SET salida="es mayor numero 2"; ELSE SET salida="los numeros son iguales"; END if;
	END if;
	return salida;
END//
delimiter ;

SELECT f1(3,4);
SELECT f1(4,3);
SELECT f1(4,4);

DROP FUNCTION if EXISTS f2;
delimiter //
CREATE FUNCTION f2(n1 float)
RETURNS VARCHAR(20)
BEGIN
  case
  		when ((n1>0) AND (n1<5)) then RETURN "suspenso";
  		when ((n1>=5) AND (n1<=6)) then RETURN "Aprobado";
  		when ((n1>6) AND (n1<=7)) then RETURN "Aprobado alto";
  		when ((n1>7) AND (n1<=8)) then RETURN "notable";
  		when ((n1>8) AND (n1<=9)) then RETURN "notable alto";
  		when ((n1>9) AND (n1<=10)) then RETURN "sobresaliente";
  END case;
END//
delimiter ;

SELECT f2(6.6);

DROP FUNCTION if EXISTS f3;
delimiter //
CREATE FUNCTION f3(n1 float)
RETURNS VARCHAR(20)
BEGIN
  case
  		when ((n1<5)) then RETURN "suspenso";
  		when ((n1<=6)) then RETURN "Aprobado";
  		when ((n1<=7)) then RETURN "Aprobado alto";
  		when ((n1<=8)) then RETURN "notable";
  		when ((n1<=9)) then RETURN "notable alto";
  		when ((n1<=10)) then RETURN "sobresaliente";
  END case;
END//
delimiter ;

SELECT f3(6.6);

-- día:20-12-2022

USE `ajcm8.seleccion`; 

DROP PROCEDURE if EXISTS p4;
delimiter //
CREATE PROCEDURE p4(numero INT)
BEGIN 
	if(numero>0)then
		SELECT numero,"mayor que cero";
	ELSE 
 		SELECT numero,"menor o igual que cero";
 	END if;
END//
delimiter ;

CALL p4(4);
CALL p4(-4);

DROP FUNCTION if EXISTS f4;
delimiter //
CREATE FUNCTION f4(numero INT)
RETURNS VARCHAR(30)
BEGIN 
	if(numero>0)then
		return CONCAT(CONVERT(numero,VARCHAR(3))," ,mayor que cero");
	ELSE 
 		return CONCAT(CONVERT(numero,VARCHAR(3))," ,menor o igual que cero");
 	END if;
END//
delimiter ;

SELECT f4(4);
SELECT f4(-4);

DROP FUNCTION if EXISTS f5;
delimiter //
CREATE FUNCTION f5(caracter VARCHAR(1))
RETURNS VARCHAR(60)
BEGIN
	SET caracter=LOWER(caracter);
	case 
		when (caracter="a") then RETURN "vocal";
		when (caracter="e") then RETURN "vocal";
		when (caracter="i") then RETURN "vocal";
		when (caracter="o") then RETURN "vocal";
		when (caracter="u") then RETURN "vocal";
	ELSE RETURN "consonante, numero o otro tipo de caracter";
	END case;
END//
delimiter ;

SELECT f5("a");
SELECT f5("/");
SELECT f5("M");

DROP FUNCTION if EXISTS f5;
delimiter //
CREATE FUNCTION f5(caracter VARCHAR(1))
RETURNS VARCHAR(60)
BEGIN
	SET caracter=LOWER(caracter);
	case 
		when (caracter IN ("a","e","i","o","u")) then RETURN "vocal";
	ELSE RETURN "consonante, numero o otro tipo de caracter";
	END case;
END//
delimiter ;

SELECT f5("a");
SELECT f5("/");
SELECT f5("M");

CREATE OR REPLACE TABLE vocales(
	id INT AUTO_INCREMENT,
	nombre CHAR(1),
	PRIMARY KEY (id)
);

INSERT INTO vocales(id,nombre) VALUES
(1,"a"),
(2,"e"),
(3,"i"),
(4,"o"),
(5,"u");

SELECT COUNT(*) FROM vocales WHERE nombre="w";

DROP FUNCTION if EXISTS f5;
delimiter //
CREATE FUNCTION f5(caracter CHAR(1))
RETURNS VARCHAR(20)
BEGIN
	DECLARE salida VARCHAR(20);
	DECLARE numero INT DEFAULT 0;

	SET caracter=LOWER(caracter);
	SELECT COUNT(*) INTO numero FROM vocales WHERE nombre=caracter;
	if (numero=1) then SET salida="vocal";
	ELSE SET salida="no vocal";
	END if;
	RETURN salida;	
END//
delimiter ;

SELECT f5("a");
SELECT f5("/");
SELECT f5("M");

DROP FUNCTION if EXISTS f5;
delimiter //
CREATE FUNCTION f5(par integer)
RETURNS VARCHAR(20)
BEGIN
	DECLARE salida VARCHAR(20) DEFAULT "";
	if (par % 2=0) then SET salida= "par";
	ELSE SET salida= "impar";
	END if;
	if(par=0) then SET salida="cero";
	END if;
	RETURN salida;
END//
delimiter ;

SELECT f5(2);
SELECT f5(3);
SELECT f5(0);

DROP FUNCTION if EXISTS f5;
delimiter //
CREATE FUNCTION f5(numero integer)
RETURNS VARCHAR(30)
BEGIN
	case
		when numero=0 then RETURN "cero";
		when numero=1 then RETURN "uno";
		when numero=2 then RETURN "dos";
		when numero=3 then RETURN "tres";
		when numero=4 then RETURN "cuatro";
		when numero=5 then RETURN "cinco";
		when numero=6 then RETURN "seis";
		when numero=7 then RETURN "siete";
		when numero=8 then RETURN "ocho";
		when numero=9 then RETURN "nueve";
		ELSE RETURN "número no valido";
	END case;
END//
delimiter ;

SELECT f5(2);
SELECT f5(3);
SELECT f5(0);
SELECT f5(10);

DROP TABLE if EXISTS numeros;
CREATE TABLE numeros(
	id INT AUTO_INCREMENT,
	valor int,
	texto VARCHAR(20),
	PRIMARY KEY(id)
);
INSERT INTO numeros(id,valor,texto)VALUES
(1,1,'uno'),
(2,2,'dos'),
(3,3,'tres'),
(4,4,'cuatro'),
(5,5,'cinco'),
(6,6,'seis'),
(7,7,'siete'),
(8,8,'ocho'),
(9,9,'nueve');

DROP FUNCTION if EXISTS f6;
delimiter //
CREATE FUNCTION f6(valor INT)
RETURNS VARCHAR(20)
BEGIN

DECLARE salida VARCHAR(20) DEFAULT "";
SELECT numeros.texto INTO salida FROM numeros WHERE numeros.valor=valor;
if salida="" then SET salida="número no valido"; end if;
RETURN salida;

END//
delimiter ;

SELECT f6(1),f6(3),f6(10);

DROP TABLE if EXISTS fechas;
CREATE TABLE fechas(
	id INT AUTO_INCREMENT,
	fechaEntrada DATE,
	fechaSalida DATE,
	dias INT COMMENT 'dias de diferencia entre fecha de entrada y fecha de salida',
	MesFechaEntrada VARCHAR(20) COMMENT 'mes de fecha de entrada como texto',
	diaFechaEntrada VARCHAR(20) COMMENT 'dia de la semana de la fecha de entrada',
	PRIMARY KEY(id)
);

INSERT INTO fechas(id,fechaEntrada,fechaSalida) VALUES
(1,'2022/08/08','2022/08/09'),
(2,'2022/08/08','2022/08/11');

DROP function if EXISTS dias;
delimiter //
CREATE function dias(id INT)
RETURNS integer
BEGIN
	DECLARE fecha1 DATE DEFAULT '2012/12/12';
	DECLARE fecha2 DATE DEFAULT '2012/12/12';
	DECLARE salida INT DEFAULT 0;
	SELECT fechas.fechaEntrada INTO fecha1 FROM fechas WHERE fechas.id=id;
	SELECT fechas.fechaSalida INTO fecha2 FROM fechas WHERE fechas.id=id;
	SET salida= ABS(TO_DAYS(fecha1)- TO_DAYS(fecha2));
	RETURN salida;
END//
delimiter ;

DROP function if EXISTS dia;
delimiter //
CREATE function dia(id INT)
RETURNS VARCHAR(20)
BEGIN
	DECLARE fecha1 DATE DEFAULT '2012/12/12';
	DECLARE salida VARCHAR(20) DEFAULT "";
	SET @@lc_time_names='es_ES';
	SELECT fechas.fechaEntrada INTO fecha1 FROM fechas WHERE fechas.id=id;
	set salida=DAYNAME(fecha1);
	RETURN salida;
END//
delimiter ;

DROP function if EXISTS mes;
delimiter //
CREATE function mes(id INT)
RETURNS VARCHAR(20)
BEGIN
	DECLARE fecha1 DATE DEFAULT '2012/12/12';
	DECLARE salida VARCHAR(20) DEFAULT "";
	SET @@lc_time_names='es_ES';
	SELECT fechas.fechaEntrada INTO fecha1 FROM fechas WHERE fechas.id=id;
	set salida=MONTHNAME(fecha1);
	RETURN salida;
END//
delimiter ;

DROP PROCEDURE if EXISTS p5;
delimiter //
CREATE PROCEDURE p5()
BEGIN
	DECLARE numreg INT DEFAULT 0;
	DECLARE c INT DEFAULT 1;
	SELECT COUNT(*) INTO numreg FROM fechas;
	while (c<=numreg) do
		UPDATE fechas
		SET fechas.dias=dias(c),fechas.MesFechaEntrada=mes(c),fechas.diaFechaEntrada=dia(c) WHERE fechas.id=c LIMIT 1;
	/*	UPDATE fechas
		SET fechas.MesFechaEntrada=mes(c) WHERE fechas.id=c LIMIT 1;
		UPDATE fechas
		SET fechas.diaFechaEntrada=dia(c) WHERE fechas.id=c LIMIT 1;*/
		set c = c + 1;
	END While;
END//
delimiter ;

SELECT dia(1),mes(1),dias(1);
SELECT dia(2),mes(2),dias(2);
CALL p5();

DROP FUNCTION if EXISTS f7;
delimiter //
CREATE FUNCTION f7(num INTEGER)
RETURNS VARCHAR(50)
BEGIN
   DECLARE contador INT DEFAULT 0;
   DECLARE suma INT DEFAULT 0;
   if(num <= 1) then RETURN 'parametro de la función debe ser mayor que uno'; END if;
	while(contador <= num) DO 
		SET suma = suma+contador;
		SET contador= contador +1;
	END while;
	RETURN suma;
END//
delimiter ;

SELECT f7(4),f7(8),f7(1);

DROP FUNCTION if EXISTS f8;
delimiter //
CREATE FUNCTION f8(num integer)
RETURNS varchar(50)
BEGIN
	DECLARE contador INT DEFAULT 0;
   DECLARE suma INT DEFAULT 0;
   if(num <= 1) then RETURN 'parametro de la función debe ser mayor que uno'; END if;
	while(contador <= num) DO 
		SET suma = suma+contador;
		SET contador= contador +2;
	END while;
	RETURN suma;
END//
delimiter ;

SELECT f8(4),f8(8),f8(1);

DROP PROCEDURE if EXISTS letras;
delimiter //
CREATE PROCEDURE letras(frase VARCHAR(100))
BEGIN
	DECLARE vocals INTEGER DEFAULT 0;
	DECLARE len INTEGER DEFAULT 0;
	DECLARE contador INTEGER DEFAULT 0;
	DECLARE caracter CHAR(1) DEFAULT "";
	SET len=LENGTH(frase);
	while(contador <= len) DO	
		SET caracter=SUBSTRING(frase,contador,1);	
		SET caracter=LOWER(caracter);
		case 
			when (caracter="a") then SET vocals=vocals+1;
			when (caracter="e") then SET vocals=vocals+1;
			when (caracter="i") then SET vocals=vocals+1;
			when (caracter="o") then SET vocals=vocals+1;
			when (caracter="u") then SET vocals=vocals+1;
			ELSE SET vocals=vocals+0;
		END case;
		set contador=contador+1;
	END while;
	SELECT frase,vocals;
END//
delimiter ;

CALL letras('ejemplo');
CALL letras('ejEmPlo');
CALL letras('aaaaaaa');

DROP PROCEDURE if EXISTS letras;
delimiter //
CREATE PROCEDURE letras(frase VARCHAR(100))
BEGIN
	DECLARE vocals INTEGER DEFAULT 0;
	DECLARE len INTEGER DEFAULT 0;
	DECLARE contador INTEGER DEFAULT 0;
	DECLARE caracter CHAR(1) DEFAULT "";
	SET len=LENGTH(frase);
	while(contador <= len) DO	
		SET caracter=SUBSTRING(frase,contador,1);	
		SET caracter=LOWER(caracter);
		if(caracter IN ('a','e','i','o','u'))then SET vocals=vocals+1;
		ELSE SET vocals=vocals+0; END if;
		set contador=contador+1;
	END while;
	SELECT frase,vocals;
END//
delimiter ;

CALL letras('ejemplo');
CALL letras('ejEmPlo');
CALL letras('aaaaaaa');