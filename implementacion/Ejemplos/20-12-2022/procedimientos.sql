SET NAMES 'utf8';
USE `ajcm8.compranp`;

CREATE PROCEDURE if NOT EXISTS p1()
BEGIN

END;

-- para llamar al procedimineto

CALL p1();

DROP PROCEDURE if EXISTS p1;
DELIMITER //
CREATE PROCEDURE p1()
BEGIN
	SELECT 1,2;
	SELECT 2;
END//
DELIMITER ;

CALL p1();



-- variables
-- dos tipos de variales locales y globales
-- si estamos fuera del procedimiento o dentro del procedimiento variables no typadas
SET @n1=1;
SELECT @n1;
-- si estamos dentro del procedimiento
-- variales tipadas
DECLARE n1 INT DEFAULT 0;
SELECT n1;

DROP PROCEDURE if EXISTS p3;
DELIMITER //
CREATE PROCEDURE p3()
BEGIN	
	DECLARE n1 INT DEFAULT 1;
	SELECT n1;
END//
DELIMITER ;

CALL p3();

-- 14/11/2022
SET NAMES 'utf8';
USE `ajcm8.compranp`;

-- nombre de los productos cuyo precio esta por encima de la media

-- a.-
SELECT productos.nombre,productos.precio,(SELECT AVG(productos.precio) FROM productos)AS PrecioMedio
FROM productos
GROUP BY productos.nombre
HAVING productos.precio > (SELECT AVG(productos.precio) FROM productos);

SELECT productos.nombre,productos.precio,(SELECT AVG(productos.precio) FROM productos)AS PrecioMedio
FROM productos
WHERE productos.precio > (SELECT AVG(productos.precio) FROM productos);

SET @c1=(SELECT AVG(productos.precio) FROM productos);
SELECT @c1;-- variable que dura de forma temporal en el servidor (variable de sesion), variables declaradas fuera del procedimiento deben empezar por @

SELECT productos.nombre,productos.precio,(@c1)AS PrecioMedio
FROM productos
GROUP BY productos.nombre
HAVING productos.precio > (@c1);

SELECT productos.nombre,productos.precio,(@c1)AS PrecioMedio
FROM productos
WHERE productos.precio > (@c1);

-- usamos la misma consulta anterior pero estavez en un procedimiento
	DROP PROCEDURE if EXISTS procedimiento;
	DELIMITER //
	CREATE PROCEDURE procedimiento()
	BEGIN
		SET @c1=(SELECT AVG(productos.precio) FROM productos);
		SELECT @c1;-- variable que dura de forma temporal en el servidor (variable de sesison), variables declaradas fuera del procedimiento deben empezar por @

		SELECT productos.nombre,productos.precio,(@c1)AS PrecioMedio
		FROM productos
		GROUP BY productos.nombre
		HAVING productos.precio > (@c1);

		SELECT productos.nombre,productos.precio,(@c1)AS PrecioMedio
		FROM productos
		WHERE productos.precio > (@c1); 
	END //
	DELIMITER ;

	CALL procedimiento();
		
	DROP PROCEDURE if EXISTS procedimiento1;
	DELIMITER //
	CREATE PROCEDURE procedimiento1()
	BEGIN
		DECLARE c1 float DEFAULT 0;
		SET c1=(SELECT AVG(productos.precio) FROM productos);
	   SELECT c1;-- variable que dura de forma temporal en el servidor, variables declaradas fuera del procedimiento deben empezar por @

		SELECT productos.nombre,productos.precio,(c1)AS PrecioMedio
		FROM productos
		GROUP BY productos.nombre
		HAVING productos.precio > (c1);

		SELECT productos.nombre,productos.precio,(c1)AS PrecioMedio
		FROM productos
		WHERE productos.precio > (c1); 
	END //
	DELIMITER ;
	
	CALL procedimiento1();
	
--	indicar el codigo los productos cuyos ingresos esten por encima delos ingresos medios
	DROP PROCEDURE if EXISTS procedimiento2;
	DELIMITER //
	CREATE PROCEDURE procedimiento2()
	BEGIN
		DECLARE c2 FLOAT DEFAULT 0;
		SET c2=(SELECT AVG(compranp.total) FROM compranp);
		SELECT c2;
		
					SELECT compran.codPro,(AVG(compranp.total)) AS IngresosMedios
	   		 	FROM compran
	   			INNER JOIN compranp
	   			ON compran.idCompran=compranp.idCompran
					GROUP BY compran.codPro;
					
	   			SELECT compran.codPro,(AVG(compranp.total)) AS IngresosMedios
	   		 	FROM compran
	   			INNER JOIN compranp
	   			ON compran.idCompran=compranp.idCompran
					GROUP BY compran.codPro
					HAVING IngresosMedios >(c2);
	   
	END //
	DELIMITER ;
	
	CALL procedimiento2();