USE `ajcm8.compranp`;

/*CREATE VIEW vista1 AS (consulta de seleccion)

para ver la vista

SELECT * FROM vista1;-- para ver la vista*/

CREATE VIEW vista1 AS
		SELECT * FROM productos;
		
SELECT * FROM vista1;-- para ver la vista

ALTER TABLE productos DROP COLUMN numeroClientes;-- si ejecutamos ahora el select de vista 1 esta no funciona

SELECT * FROM vista1;-- para ver la vista esta no funciona por que nos falta el campo numeroClientes

DROP VIEW if EXISTS vista2;
CREATE VIEW vista2 AS
		SELECT DISTINCT productos.nombre
		FROM productos
		INNER JOIN compran
		ON productos.codPro=compran.codPro;

CREATE OR REPLACE VIEW vista2 AS
		SELECT DISTINCT productos.nombre
		FROM productos
		INNER JOIN compran
		ON productos.codPro=compran.codPro;
		
SELECT * FROM vista2;

DROP VIEW vista2;-- elimina la vista2

SHOW CREATE VIEW vista2;

-- para poder utilizar las vista en un select o consulta

SELECT * FROM vista2;

-- optimizar la consulta de productos vendidos que es la vista2 anterior
-- a.- 
SELECT productos.codPro FROM productos;
-- b.- 
SELECT compran.codPro FROM compran;
-- c.-
SELECT c1.codPro
FROM (
		SELECT productos.codPro FROM productos
     ) AS c1
INNER JOIN (
				SELECT compran.codPro FROM compran
				) AS c2
ON c1.codPro=c2.codPro;
-- d.-
SELECT productos.nombre
FROM productos
WHERE productos.codPro IN (
									SELECT c1.codPro
									FROM (
											SELECT productos.codPro FROM productos
     										) AS c1
									INNER JOIN (
													SELECT compran.codPro FROM compran
													) AS c2
									ON c1.codPro=c2.codPro
									);
									
-- entonces usando a,b,c,d consultas intermedias llegamos a la consulta final optimizada de vista de productos vendidos
									
CREATE OR REPLACE VIEW productosvendidos AS (
															SELECT productos.nombre
															FROM productos
															WHERE productos.codPro IN (
																								SELECT c1.codPro
																								FROM (
																										SELECT productos.codPro FROM productos
     																									) AS c1
																								INNER JOIN (
																												SELECT compran.codPro FROM compran
																												) AS c2
																								ON c1.codPro=c2.codPro
																								)
															);
															
CREATE OR REPLACE VIEW productosvendidos AS (
															SELECT productos.nombre
															FROM productos
															WHERE productos.codPro IN (
																								SELECT c1.codPro
																								FROM (
																										SELECT productos.codPro FROM productos
     																									) AS c1
																								NATURAL JOIN (
																												SELECT compran.codPro FROM compran
																												) AS c2
																								)
															);

SELECT * FROM productosvendidos;

-- Utilizar las vistas mediante restriciones usando checks y asserciones pero en mysql estas se parsean pero si funcionan con vistas

-- en la tabla productos queremos una regla de validacion o restricion para que el precio sea mayor que 1000 euros
ALTER TABLE productos 
	ADD CHECK(productos.precio > 0);

INSERT INTO productos (codPro,nombre,precio,numeroClientes)
VALUES(250,'pepinos',-1,8); -- observamos como la restricion anterior se pone al insertar este producto en la tabla productos

SELECT * FROM productos;-- seguimos teniendo los 10 productos, el producto pepino no ha sido introducido por la restriccion

-- en caso que tengamos un SGBD en que no funciones las restricciones o aserciones debemos utilizar las vistas

CREATE OR REPLACE VIEW restringirprecio AS
SELECT *
FROM productos
WHERE precio>0
WITH LOCAL CHECK OPTION;
-- volvelmos a intentar meter el producto pepino

INSERT INTO restringirPrecio (codPro,nombre,precio,numeroClientes)
VALUES(250,'pepinos',-1,8); -- observamos que el producto no es introducido por la regla de validacion de la consulta anterior con la vista restringirPrecio

