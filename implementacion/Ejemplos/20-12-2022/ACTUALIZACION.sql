/*
  CONSULTAS DE ACCION
 
 */

USE `ajcm8.compranp`;

-- EJEMPLO1 
-- AÑADIR UN CAMPO A LA TABLA PRODUCTOS DENOMINADO numeroProductos 
-- QUE ME INDIQUE EL NUMERO DE VECES QUE SE HA COMPRADO DE ESE PRODUCTO

-- añado el campo
  ALTER TABLE productos ADD COLUMN numeroProductos int DEFAULT 0;

-- consulta de seleccion                                         
  SELECT 
    c.codPro,COUNT(*) numero 
    FROM compran c GROUP BY c.codPro;

 -- consulta de accion
  UPDATE productos p
    JOIN 
      (
        SELECT 
        c.codPro,COUNT(*) numero 
        FROM compran c GROUP BY c.codPro
      ) c1
    USING(codPro)
    SET p.numeroProductos=c1.numero;

-- EJEMPLO2
-- AÑADIR UN CAMPO A LA TABLA PRODUCTOS DENOMINADO numeroUnidades
-- QUE ME INDIQUE EL NUMERO DE UNIDADES QUE SE HA COMPRADO DE ESE PRODUCTO

  -- añadir el campo
  ALTER TABLE productos ADD COLUMN numeroUnidades int DEFAULT 0;

  -- consulta de seleccion
  SELECT 
      c.codPro,SUM(c1.cantidad) numero
    FROM compran c JOIN compranp c1 ON c.idCompran = c1.idCompran
    GROUP BY c.codPro;

  -- consulta de actualizacion

    UPDATE productos p 
      JOIN
      (
        SELECT 
          c.codPro,SUM(c1.cantidad) numero
          FROM compran c JOIN compranp c1 ON c.idCompran = c1.idCompran
          GROUP BY c.codPro
      ) c1
      USING(codPro)
      SET p.numeroUnidades=c1.numero;

    
    -- Consulta de Joaquin
    UPDATE 
      productos p 
        join
          (
            SELECT 
              c.codPro,SUM(c1.numero) numeroTotal
              FROM compran c JOIN 
              (
                SELECT 
                    c.idCompran, sum(c.cantidad) numero 
                      FROM compranp c 
                      GROUP BY c.idCompran
              )c1 
                ON c.idCompran = c1.idCompran
              GROUP BY c.codPro
            ) c2
            USING(codPro)
            SET p.numeroUnidades=c2.numeroTotal;


    SELECT 
      IF(p.precio>10,"CARO","BARATO") campo, p.precio, p.nombre 
      FROM productos p;


  -- EJEMPLO 3
  -- CALCULAR EL PRECIO TOTAL DE LOS PRODUCTOS EN LA TABLA COMPRANP TENIENDO EN CUENTA LO SIGUIENTE:
  -- SI EL NUMERO DE UNIDADES PEDIDO ES MENOR QUE 10 NO HAY DESCUENTO
  -- SI EL NUMERO DE UNIDADES ESTA ENTRE 10 Y 30 EL DESCUENTO ES DEL 10%
  -- SI EL NUMERO DE UNIDADES ES MAYOR QUE 30 EL DESCUENTO ES DEL 20%
  -- PARA CALCULAR EL PRECIO TOTAL ES MULTIPLICAR EL PRECIO DEL PRODUCTO POR LAS UNIDADES

  -- consulta de seleccion

    SELECT *,c.cantidad*p.precio*(1-0)
      FROM 
      compranp c JOIN compran c2 ON c.idCompran = c2.idCompran
      JOIN productos p ON c2.codPro = p.codPro
      WHERE c.cantidad<10;

    SELECT *,c.cantidad*p.precio*(1-0.1)
      FROM 
      compranp c JOIN compran c2 ON c.idCompran = c2.idCompran
      JOIN productos p ON c2.codPro = p.codPro
      WHERE c.cantidad BETWEEN 10 AND 30;

    SELECT *,c.cantidad*p.precio*(1-0.2)
      FROM 
      compranp c JOIN compran c2 ON c.idCompran = c2.idCompran
      JOIN productos p ON c2.codPro = p.codPro
      WHERE c.cantidad>30;

    -- UTILIZO UN IF 
    SELECT *,c.cantidad*p.precio*(1-IF(c.cantidad<10,0,IF(c.cantidad<=30,0.1,0.2))) campo
      FROM 
      compranp c JOIN compran c2 ON c.idCompran = c2.idCompran
      JOIN productos p ON c2.codPro = p.codPro;
      

  -- consulta de ACTUALIZACION
    UPDATE 
      compranp c JOIN compran c2 ON c.idCompran = c2.idCompran
      JOIN productos p ON c2.codPro = p.codPro
      SET c.total=c.cantidad*p.precio*(1-IF(c.cantidad<10,0,IF(c.cantidad<=30,0.1,0.2)));


    SELECT * FROM compranp c;


    -- EJEMPLO 1
    -- ELIMINAR LOS PRODUCTOS CUYO PRECIO ES MAYOR O IGUAL QUE 20 EUROS

    -- CONSULTA DE SELECCION 
     SELECT 
      * 
      FROM productos p 
      WHERE p.precio>=20;


    -- CONSULTA DE ELIMINACION
      DELETE
        FROM productos 
        WHERE precio>=20;
    
    -- OPCION 2
      DELETE
        productos.*
        FROM productos 
        WHERE precio>=20;

      -- OPCION 3
      DELETE
        productos
        FROM productos 
        WHERE precio>=20;

      -- OPCION 4
       DELETE
        p.*
        FROM productos p 
        WHERE p.precio>=20;
    
    -- EJEMPLO 2
    -- Eliminar las compras de los productos cuyo precio es mayor o igual 20

    -- consulta de seleccion

      SELECT 
        * 
        FROM productos p JOIN compran c ON p.codPro = c.codPro
        WHERE p.precio>=20;

      SELECT * FROM productos p;

    -- consulta de actualizacion

      -- OPCION 1
      DELETE compran.*
        FROM productos JOIN compran USING(codPro)
        WHERE precio>=20;

      -- OPCION 2
      DELETE c.*
        FROM productos p JOIN compran c USING(codPro)
        WHERE p.precio>=20;
      
      -- OPCION 3
      DELETE c
        FROM productos p JOIN compran c USING(codPro)
        WHERE p.precio>=20;

     -- OPCION CON SUBCONSULTAS
     
     -- SUBCONSULTA EN WHERE
     DELETE FROM compran 
      WHERE codPro IN (SELECT p.codPro FROM productos p WHERE p.precio>=20);

     -- SUBCONSULTA EN EL FROM    
     DELETE c.* FROM compran c JOIN (SELECT p.codPro FROM productos p WHERE p.precio>=20) c1
      USING(codPro);


   -- EJEMPLO 3
   -- ELIMINAR LAS COMPRAS DE LOS CLIENTES DE SANTANDER

    -- CONSULTA DE SELECCION
    SELECT 
        * 
        FROM compran c 
        JOIN clientes c1 ON c.codCli = c1.codCli 
        JOIN poblacion p ON c1.poblacion = p.idPob
        WHERE p.nombre="Santander";

    -- CONSULTA DE ELIMINACION
     
     -- opcion 1
     DELETE c.* 
      FROM compran c 
        JOIN clientes c1 ON c.codCli = c1.codCli 
        JOIN poblacion p ON c1.poblacion = p.idPob
        WHERE p.nombre="Santander";

    -- opcion 2
     DELETE c 
      FROM compran c 
        JOIN clientes c1 ON c.codCli = c1.codCli 
        JOIN poblacion p ON c1.poblacion = p.idPob
        WHERE p.nombre="Santander";

 
 /* TRANSACCIONES */
 SELECT @@autocommit;
 SET @@autocommit=0;

 SET AUTOCOMMIT=0;

 DELETE FROM clientes;
 SELECT * FROM CLIENTES;
 ROLLBACK;
 SELECT * FROM CLIENTES;

 DELETE FROM clientes;
 SELECT * FROM CLIENTES;
 COMMIT;
 SELECT * FROM CLIENTES;

 





   

     

                     
      
      
  



      





  







