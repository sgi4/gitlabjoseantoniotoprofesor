-- disparadores
DROP DATABASE if EXISTS `ajcm8.disparadores`;
CREATE DATABASE `ajcm8.disparadores`;
USE `ajcm8.disparadores`;

CREATE OR REPLACE TABLE noticias(
	id INTEGER AUTO_INCREMENT,
	texto VARCHAR(100),
	titulo VARCHAR(100),
	longitud INTEGER,
	fecha DATE,
	dia INTEGER,
	mes VARCHAR(20),
	PRIMARY KEY(id)
);

INSERT INTO noticias (texto,titulo,fecha)VALUES
('ejemplo de noticia larga','larga','2022/12/01'),
('ejemplo de noticia corta','corta','2022/01/12');

-- primer disparador

DROP TRIGGER if EXISTS ejemplo1;
delimiter //
CREATE OR REPLACE TRIGGER ejemplo1
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
AFTER INSERT 
	ON noticias
	FOR EACH ROW
	-- trigger order  
BEGIN

END//
delimiter ;

DROP TRIGGER if EXISTS binoticias;
delimiter //
CREATE OR REPLACE TRIGGER binoticias
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
BEFORE INSERT 
	ON noticias
	FOR EACH ROW
	-- trigger order  
BEGIN
	SET NEW.longitud=LENGTH(NEW.texto);
END//
delimiter ;

INSERT INTO noticias (texto,titulo,fecha)VALUES
('ejemplo de noticia larga2 disparador binoticias','larga2','2021/12/01'),
('ejemplo de noticia corta2 disparador binoticias','corta2','2021/01/12');

UPDATE noticias
SET noticias.longitud=LENGTH(noticias.texto)
WHERE noticias.longitud IS NULL;

DROP TRIGGER if EXISTS bimesdianoticias;
delimiter //
CREATE OR REPLACE TRIGGER bimesdianoticias
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
BEFORE INSERT 
	ON noticias
	FOR EACH ROW
	-- trigger order  
BEGIN
  SET @@lc_time_names='es_ES';
  SET NEW.dia=DAY(NEW.fecha);
  SET NEW.mes=MONTHNAME(NEW.fecha);
END//
delimiter ;

INSERT INTO noticias (texto,titulo,fecha)VALUES
('ejemplo de noticia larga3 disparador bimesdianoticias','larga3','2020/12/01'),
('ejemplo de noticia corta3 disparador bimesdianoticias','corta3','2020/01/12');

UPDATE noticias
SET noticias.mes=MONTHNAME(noticias.fecha),noticias.dia=DAY(noticias.fecha)
WHERE noticias.mes IS NULL OR noticias.dia IS NULL;

-- actualizo una de las noticias
UPDATE noticias
  SET noticias.texto="ALGO DIFERENTE"
  WHERE noticias.id=4;
  
DROP TRIGGER if EXISTS bunoticias;
delimiter //
CREATE OR REPLACE TRIGGER bunoticias
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
BEFORE UPDATE 
	ON noticias
	FOR EACH ROW
	-- trigger order  
BEGIN
  SET new.longitud=LENGTH(NEW.texto);
  SET @@lc_time_names='es_ES';
  SET NEW.dia=DAY(NEW.fecha);
  SET NEW.mes=MONTHNAME(NEW.fecha);
END//
delimiter ;

UPDATE noticias
  SET noticias.texto="ALGO DIFERENTE"
  WHERE noticias.id=4;

CREATE OR REPLACE TABLE historico(
	id INTEGER AUTO_INCREMENT,
	dia INTEGER,
	numero INTEGER DEFAULT 0,
	PRIMARY KEY(id)
);

-- necesitamos un update que inicialize la tabla historico
SELECT noticias.dia,COUNT(noticias.dia)
	FROM noticias
	GROUP BY noticias.dia;
	
-- debemos meter los datos anteriores en la tabla historico
CREATE OR REPLACE TABLE historico(
	id INTEGER AUTO_INCREMENT,
	dia INTEGER,
	numero INTEGER DEFAULT 0,
	PRIMARY KEY(id)
) AS SELECT noticias.dia AS dia, COUNT(noticias.dia) AS numero
	  FROM noticias
	  GROUP BY noticias.dia;
-- comprobar la tabla historico
SELECT * FROM historico;
-- tedriamos que ejecutar un procedimiento constantemente para actualizar la tabla historico cada vez que borramos una noticia
DROP PROCEDURE if EXISTS actualizaHistorico;
delimiter //
CREATE PROCEDURE actualizaHistorico()
BEGIN
	CREATE OR REPLACE TABLE historico(
	id INTEGER AUTO_INCREMENT,
	dia INTEGER,
	numero INTEGER DEFAULT 0,
	PRIMARY KEY(id)
) AS SELECT noticias.dia AS dia, COUNT(noticias.dia) AS numero
	  FROM noticias
	  GROUP BY noticias.dia;
END//
delimiter ;

-- por lo tanto es mejor utilizar disparadores
-- ejecutamos primero el procedimientos actualizaHistorico() para tener la tabla historico inicializada
-- despues creamos el disparador para tener siempre la tabla historico al dia
-- y añadimos otravez dos disparadores de insercion y actualizacion para actualizar la tabla historico
DROP DATABASE if EXISTS `ajcm8.disparadores`;
CREATE DATABASE `ajcm8.disparadores`;
USE `ajcm8.disparadores`;

CREATE OR REPLACE TABLE noticias(
	id INTEGER AUTO_INCREMENT,
	texto VARCHAR(100),
	titulo VARCHAR(100),
	longitud INTEGER,
	fecha DATE,
	dia INTEGER,
	mes VARCHAR(20),
	PRIMARY KEY(id)
);

DROP PROCEDURE if EXISTS actualizaHistorico;
delimiter //
CREATE PROCEDURE actualizaHistorico()
BEGIN
	CREATE OR REPLACE TABLE historico(
	id INTEGER AUTO_INCREMENT,
	dia INTEGER,
	numero INTEGER DEFAULT 0,
	PRIMARY KEY(id)
) AS SELECT noticias.dia AS dia, COUNT(noticias.dia) AS numero
	  FROM noticias
	  GROUP BY noticias.dia;
END//
delimiter ;

CALL actualizaHistorico;-- en este caso lo uso para crear unicamente la tabla historico

DROP TRIGGER if EXISTS adnoticias;
delimiter //
CREATE OR REPLACE TRIGGER adnoticias
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
AFTER DELETE
	ON noticias
	FOR EACH ROW
	-- trigger order  
BEGIN
	UPDATE historico
	SET historico.numero= historico.numero-1
	WHERE historico.dia=OLD.dia;
END//
delimiter ;

DROP TRIGGER if EXISTS binoticias;
delimiter //
CREATE OR REPLACE TRIGGER binoticias
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
BEFORE INSERT 
	ON noticias
	FOR EACH ROW
	-- trigger order  
BEGIN
  SET NEW.longitud=LENGTH(NEW.texto);
  SET @@lc_time_names='es_ES';
  SET NEW.dia=DAY(NEW.fecha);
  SET NEW.mes=MONTHNAME(NEW.fecha);
END//
delimiter ;

DROP TRIGGER if EXISTS ainoticias;
delimiter //
CREATE OR REPLACE TRIGGER ainoticias
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
AFTER INSERT 
	ON noticias
	FOR EACH ROW
	-- trigger order  
BEGIN
  DECLARE contador INTEGER DEFAULT 0;
  SELECT COUNT(*) INTO contador FROM historico WHERE historico.dia=NEW.dia;
  if(contador>=1)then
  		UPDATE historico
  		SET historico.numero=historico.numero+1
  		WHERE historico.dia=NEW.dia;
  ELSE
 		insert INTO historico(dia,numero)values
		(NEW.dia,1); 	  
  END if;  	
END//
delimiter ;

DROP TRIGGER if EXISTS bunoticias;
delimiter //
CREATE OR REPLACE TRIGGER bunoticias
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
BEFORE UPDATE 
	ON noticias
	FOR EACH ROW
	-- trigger order  
BEGIN
  SET new.longitud=LENGTH(NEW.texto);
  SET @@lc_time_names='es_ES';
  SET NEW.dia=DAY(NEW.fecha);
  SET NEW.mes=MONTHNAME(NEW.fecha);
END//
delimiter ;

DROP TRIGGER if EXISTS aunoticias;
delimiter //
CREATE OR REPLACE TRIGGER aunoticias
-- BEFORE|AFTER INSERT|UPDATE|DELETE 
AFTER UPDATE 
	ON noticias
	FOR EACH ROW
	-- trigger order  
BEGIN
  DECLARE contador INTEGER DEFAULT 0;
  SELECT COUNT(noticias.dia) INTO contador FROM noticias WHERE noticias.dia=NEW.dia;
  if(contador>=1)then
  		UPDATE historico
  		SET historico.numero=contador
  		WHERE historico.dia=NEW.dia;
  ELSE
 		insert INTO historico(dia,numero)values
		(NEW.dia,1); 	  
  END if;  	
END//
delimiter ;
INSERT INTO noticias (texto,titulo,fecha)VALUES
('ejemplo de noticia larga','larga','2022/12/01'),
('ejemplo de noticia corta','corta','2022/01/12');
INSERT INTO noticias (texto,titulo,fecha)VALUES
('ejemplo de noticia larga2 disparador binoticias','larga2','2021/12/01'),
('ejemplo de noticia corta2 disparador binoticias','corta2','2021/01/12');
INSERT INTO noticias (texto,titulo,fecha)VALUES
('ejemplo de noticia larga3 disparador bimesdianoticias','larga3','2020/12/01'),
('ejemplo de noticia corta3 disparador bimesdianoticias','corta3','2020/01/12');
INSERT INTO noticias (texto,titulo,fecha)VALUES
('ejemplo de noticia larga3 disparador bimesdianoticias','larga3','2020/08/08'),
('ejemplo de noticia corta3 disparador bimesdianoticias','corta3','2020/08/08');
-- CALL actualizaHistorico();
UPDATE noticias
  SET noticias.texto="ALGO DIFERENTE"
  WHERE noticias.id=4;
DELETE FROM noticias
WHERE noticias.id=4;