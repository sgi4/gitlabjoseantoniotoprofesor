SET NAMES 'utf8';
USE `ajcm8.compranp`;

-- Argumentos para las funciones unimamente hay argumentos de entrada
-- para procedimientos tenemos todas las posibilidades (in,inout,out)

DROP PROCEDURE if EXISTS p1;
DELIMITER //
CREATE PROCEDURE p1(n1 INT,n2 INT,OUT n3 INT)
BEGIN
	SET n3=n1+n2;
END//
DELIMITER ;

SET @suma=0;
CALL p1(3,5,@suma);
SELECT @suma;

DROP PROCEDURE if EXISTS p2;
DELIMITER //
CREATE PROCEDURE p2(n1 INT,n2 INT,OUT n3 INT, OUT n4 INT,OUT n5 INT ,OUT n6 FLOAT)
BEGIN
	SET n3=n1+n2;
	SET n4=n1*n2;
	SET n5=n1-n2;
	SET n6=n1/n2;
END//
DELIMITER ;

SET @suma=0;
SET @producto=0;
SET @resta=0;
SET @cociente=0;
CALL p2(4,2,@suma,@producto,@resta,@cociente);
SELECT @suma,@producto,@resta,@cociente;

DROP PROCEDURE if EXISTS p3;
DELIMITER //
CREATE PROCEDURE p3(cadena VARCHAR(10),OUT longitud int)
BEGIN
	SET longitud=LENGTH(cadena);	
END//
DELIMITER ;
SET @cadena='';
SET @longitud=0;
SET @cadena='Hola Mundo';
CALL p3(@cadena,@longitud);
SELECT @cadena AS texto,@longitud AS NumeroCaracteres;

DROP PROCEDURE if EXISTS p4;
DELIMITER //
CREATE PROCEDURE p4(numero1 float,numero2 FLOAT,numero3 float,OUT resultado1 float,OUT resultado2 float,OUT resultado3 float)
BEGIN
	SET resultado1=POWER(numero2,numero2);
	SET resultado2=numero1-numero3;
	SET resultado3=SQRT(numero1);
END//
DELIMITER ;

SET @numero1=4;
SET @numero2=2;
SET @numero3=2;
SET @resultado1=0;
SET @resultado2=0;
SET @resultado3=0;
CALL p4(@numero1,@numero2,@numero3,@resultado1,@resultado2,@resultado3);

SELECT @resultado1 AS cuadrado, @resultado2 AS resta, @resultado3 AS Raiz;