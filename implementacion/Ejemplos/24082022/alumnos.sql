/*Ejemplo de clase sobre la base de datos alumnos del alumno ajcm8
Extructura de las consultas y minimos que no piden para realizarlas
en cuanto a extructura*/

# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `ajcm8.alumnos`;
CREATE DATABASE IF NOT EXISTS `ajcm8.alumnos`;
-- Seleccionamos la base de datos
USE `ajcm8.alumnos`;

CREATE TABLE IF NOT EXISTS `ajcm8.alumnos`.`cursos`(
	#campos
	idCurso INTEGER,
	nombre VARCHAR(100) NOT NULL,#null es el valor de defecto, al no ponerlo
	fechaComienzo DATE, #DEFAULT CURRENT_TIMESTAMP, por defecto valor hora actual del sistema 
	precio FLOAT DEFAULT 0,
	#claves
	PRIMARY KEY(idCurso),
	UNIQUE KEY(nombre),
	KEY(fechaComienzo)
	-- Y claves foraneas  relacionadas tambien como clave que no hemos puesto porque no tenemos otra tabla para relacionar
);
