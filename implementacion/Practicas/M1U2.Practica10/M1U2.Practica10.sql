SET NAMES 'utf8';
USE `ajcm8.pract9.ciclistas`;
-- _____________________________________________________________________________________
-- Ejercicio 1,1
SELECT COUNT(*)
FROM ciclista;
-- _____________________________________________________________________________________
-- Ejercicio 1.2
SELECT COUNT(*)
FROM ciclista
WHERE nomequipo='Banesto';
-- _____________________________________________________________________________________
-- Ejercicio 1.3
SELECT DISTINCT AVG(edad)
FROM ciclista;
-- _____________________________________________________________________________________
-- Ejercicio 1.4
SELECT AVG(edad)
FROM ciclista
WHERE nomequipo='Banesto';
-- _____________________________________________________________________________________
-- Ejercicio 1.5
SELECT nomequipo, AVG(edad) AS 'Media de Edad'
FROM ciclista
GROUP BY nomequipo;
_____________________________________________________________________________________
-- Ejercicio 1.6
SELECT nomequipo,COUNT(*) AS 'Equipo formado por:'
FROM ciclista
GROUP BY nomequipo;
-- _____________________________________________________________________________________
-- Ejercicio 1.7
SELECT COUNT(*)
FROM puerto;
-- _____________________________________________________________________________________
-- Ejercicio 1.8
SELECT COUNT(*)
FROM puerto
WHERE altura>1500;
-- _____________________________________________________________________________________
-- Ejercicio 1.9
SELECT nomequipo,COUNT(*) AS 'Equipo formado por:'
FROM ciclista
GROUP BY nomequipo
HAVING COUNT(*)>4;
-- _____________________________________________________________________________________
-- Ejericicio 1.10
SELECT nomequipo,COUNT(*) AS 'Equipo formado por:'
FROM ciclista c
GROUP BY nomequipo, edad
HAVING (COUNT(*)>4) AND (edad BETWEEN 28 AND 32) ;
-- _____________________________________________________________________________________
-- Ejercicio 1.11
SELECT COUNT(dorsal)
FROM etapa
GROUP BY dorsal;
-- _____________________________________________________________________________________
-- Ejercicio 1.12
SELECT dorsal
FROM etapa
GROUP BY dorsal
HAVING COUNT(dorsal)>1