SET NAMES 'utf8';
USE `ajcm8.ciclistas`;

-- Ejercicio 1.1
SELECT DISTINCT * 
FROM (SELECT etapa.dorsal
       FROM etapa)AS c1
NATURAL JOIN (SELECT puerto.dorsal 
              FROM puerto) AS c2;

SELECT DISTINCT etapa.dorsal
FROM etapa
INNER JOIN puerto ON etapa.dorsal=puerto.dorsal;

SELECT DISTINCT etapa.dorsal
FROM etapa
WHERE dorsal IN (SELECT DISTINCT * 
FROM (SELECT distinct etapa.dorsal
       FROM etapa) AS c1
NATURAL JOIN (SELECT DISTINCT puerto.dorsal 
              FROM puerto) AS c2) ;

SELECT DISTINCT c1.dorsal
FROM (            
		SELECT etapa.dorsal FROM etapa
		) AS c1 
WHERE EXISTS (
					SELECT TRUE FROM puerto WHERE c1.dorsal=puerto.dorsal
					);
-- Ejercicio 1.2

SELECT DISTINCT ciclista.nombre,ciclista.edad
FROM ciclista
LEFT JOIN etapa ON ciclista.dorsal =etapa.dorsal
WHERE etapa.dorsal IS NULL;
 
SELECT DISTINCT ciclista.nombre, ciclista.edad
FROM ciclista
WHERE ciclista.dorsal NOT IN (SELECT DISTINCT ciclista.dorsal
FROM ciclista
INNER JOIN etapa ON ciclista.dorsal =etapa.dorsal);

SELECT DISTINCT ciclista.nombre,ciclista.edad
FROM ciclista
INNER JOIN (SELECT DISTINCT c1.dorsal
				FROM (SELECT DISTINCT ciclista.dorsal
						FROM ciclista) AS c1
				LEFT JOIN (
								SELECT DISTINCT etapa.dorsal
								FROM etapa
								) AS c2
				ON c1.dorsal=c2.dorsal
				WHERE c2.dorsal IS NULL 
				)AS c3
ON ciclista.dorsal= c3.dorsal;

SELECT DISTINCT ciclista.nombre,ciclista.edad
FROM ciclista
WHERE NOT EXISTS (SELECT TRUE FROM (
												SELECT DISTINCT etapa.dorsal
												FROM etapa)AS c1
					   WHERE c1.dorsal=ciclista.dorsal);
-- Ejercicio 1.3

-- a.-
SELECT *
FROM ciclista
INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal;

-- b.- 

SELECT equipo.director
FROM equipo
WHERE equipo.nomequipo IN(SELECT DISTINCT ciclista.nomequipo
FROM ciclista
INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal);

-- Ejercicio 1.4

-- a.-
SELECT distinct ciclista.nombre,ciclista.dorsal
from ciclista
INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal;

-- Ejercicio 1.5

SELECT ciclista.nombre, ciclista.dorsal
FROM ciclista
LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal
WHERE puerto.dorsal IS NULL;

-- Ejericio 1.6

-- a.-
SELECT *
FROM ciclista
INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal;

-- b.-
SELECT *
FROM equipo
INNER JOIN ciclista ON equipo.nomequipo=ciclista.nomequipo
INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal;

SELECT equipo.director
FROM equipo
WHERE equipo.nomequipo IN (SELECT equipo.nomequipo
FROM equipo
INNER JOIN ciclista ON equipo.nomequipo=ciclista.nomequipo
INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal); 

-- Ejercicio 1.7

SELECT DISTINCT ciclista.nombre,ciclista.dorsal
FROM ciclista
LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal
WHERE lleva.dorsal IS NULL;

-- Ejercicio 1.8
-- C1
SELECT DISTINCT lleva.dorsal
FROM lleva
INNER JOIN maillot ON lleva.`código`=maillot.`código`
WHERE maillot.color="amarillo";

SELECT DISTINCT ciclista.nombre,ciclista.dorsal
FROM ciclista
LEFT JOIN(
  			SELECT DISTINCT lleva.dorsal
			FROM lleva
			INNER JOIN maillot ON lleva.`código`=maillot.`código`
			WHERE maillot.color="amarillo"
         )AS c1 ON ciclista.dorsal=c1.dorsal
WHERE c1.dorsal IS NULL;

-- Ejercicio 1.9
-- c1
SELECT DISTINCT etapa.dorsal
FROM etapa
LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
WHERE puerto.numetapa IS NULL;

SELECT DISTINCT ciclista.nombre,ciclista.dorsal
FROM ciclista
WHERE ciclista.dorsal IN (
									SELECT DISTINCT etapa.dorsal
									FROM etapa
									LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
									WHERE puerto.numetapa IS NULL
									);
-- Ejercicio 1.10
-- c1 
SELECT DISTINCT etapa.dorsal
FROM puerto
INNER JOIN etapa ON etapa.numetapa=puerto.numetapa;
-- c2

SELECT DISTINCT ciclista.dorsal
FROM ciclista
INNER  JOIN (
				SELECT DISTINCT etapa.dorsal
				FROM etapa
				INNER JOIN puerto ON etapa.numetapa=puerto.numetapa
				) AS c1
ON ciclista.dorsal=c1.dorsal;

--c3
SELECT DISTINCT ciclista.dorsal
FROM ciclista
LEFT JOIN (
				SELECT DISTINCT etapa.dorsal
				FROM etapa
				INNER JOIN puerto ON etapa.numetapa=puerto.numetapa
				) AS c1
ON ciclista.dorsal=c1.dorsal
WHERE c1.dorsal IS null;

SELECT c2.dorsal
FROM(
	  SELECT DISTINCT ciclista.dorsal
	  FROM ciclista
	  LEFT JOIN (
					 SELECT DISTINCT etapa.dorsal
					 FROM etapa
					 INNER JOIN puerto ON etapa.numetapa=puerto.numetapa
				) AS c3
	  ON ciclista.dorsal=c3.dorsal
	  WHERE c3.dorsal IS null
	 ) AS c2
LEFT JOIN (
			 SELECT DISTINCT etapa.dorsal
			 FROM puerto
			 INNER JOIN etapa ON etapa.numetapa=puerto.numetapa
			 ) AS c1 
ON c1.dorsal=c2.dorsal
WHERE c1.dorsal IS NULL;