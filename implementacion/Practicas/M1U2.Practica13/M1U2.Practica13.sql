SET NAMES 'utf8';
USE `ajcm8.empleados`;
-- _____________________________________________________________________________________
-- Ejercicio 1,1
SELECT DISTINCT *
FROM emple;
-- _____________________________________________________________________________________
-- Ejercicio 1.2
SELECT DISTINCT *
FROM depart;
-- _____________________________________________________________________________________
-- Ejercicio 1.3
SELECT DISTINCT apellido,oficio
FROM emple;
-- _____________________________________________________________________________________
-- Ejercicio 1.4
SELECT DISTINCT dept_no,loc
FROM depart;
-- _________________________________
-- _____________________________________________________________________________________
-- Ejercicio 1.5
SELECT DISTINCT dept_no,dnombre,LEFT(loc,1)AS `Primer Caracter`
FROM depart;
-- _____________________________________________________________________________________
-- Ejercicio 1.6
SELECT COUNT(*)
FROM emple;
-- _____________________________________________________________________________________
-- Ejercicio 1.7
SELECT DISTINCT *
FROM emple
ORDER BY apellido ASC;
-- _____________________________________________________________________________________
-- Ejercicio 1.8
SELECT DISTINCT *
FROM emple
ORDER BY oficio asc,apellido DESC;
-- _____________________________________________________________________________________
-- Ejercicio 1.9
SELECT COUNT(*)
FROM depart;
-- _____________________________________________________________________________________
-- Ejericicio 1.10
SELECT SUM(n1)
FROM 
(SELECT COUNT(apellido)AS n1
FROM emple
UNION
SELECT COUNT(dept_no)AS n1
FROM depart) AS c1;
-- _____________________________________________________________________________________
-- Ejercicio 1.11
SELECT DISTINCT *
FROM emple
ORDER BY dept_no ASC
LIMIT 2;
-- _____________________________________________________________________________________
-- Ejercicio 1.12
SELECT DISTINCT *
FROM emple
ORDER BY dept_no DESC,oficio ASC;
-- _____________________________________________________________________________________
-- Ejercicio 1.13
SELECT DISTINCT *
FROM emple
ORDER BY dept_no DESC,LEFT(apellido,1) asc;
-- _____________________________________________________________________________________
-- Ejercicio 1.14
SELECT DISTINCT emp_no,salario
FROM emple
WHERE salario>2000;
-- _____________________________________________________________________________________
-- Ejercicio 1.15
SELECT DISTINCT emp_no,apellido
FROM emple
WHERE (salario<2000)AND comision IS null;
-- _____________________________________________________________________________________
-- Ejercicio 1.16
SELECT DISTINCT *
FROM emple
WHERE salario BETWEEN 1500 AND 2500;
-- _____________________________________________________________________________________
-- Ejercicio 1.17
SELECT DISTINCT *
FROM emple
WHERE Upper(oficio)='ANALISTA';
-- _____________________________________________________________________________________
-- Ejercicio 1.18
SELECT DISTINCT emp_no,apellido
FROM emple
WHERE (salario>2000)AND (Upper(oficio)='ANALISTA');
-- _____________________________________________________________________________________
-- Ejercicio 1.19
SELECT DISTINCT apellido,oficio
FROM emple
WHERE dept_no=20;
-- _____________________________________________________________________________________
-- Ejercicio 1.20
SELECT DISTINCT COUNT(*)
FROM emple
WHERE UPPER(oficio)='VENDEDOR';
-- _____________________________________________________________________________________
-- Ejercicio 1.21
SELECT DISTINCT *
FROM emple
WHERE LEFT(apellido,1) BETWEEN 'M' AND 'N';
-- _____________________________________________________________________________________
-- Ejercicio 1.22
SELECT DISTINCT *
FROM emple
WHERE UPPER(oficio)='VENDEDOR'
ORDER BY apellido asc;
-- _____________________________________________________________________________________
-- Ejercicio 1.23
SELECT DISTINCT apellido,Max(salario)
FROM emple;
-- _____________________________________________________________________________________
-- Ejercicio 1.24
SELECT DISTINCT emp_no,apellido
FROM emple
WHERE (dept_no=20)AND (Upper(oficio)='ANALISTA')
ORDER BY apellido ASC,oficio ASC;
-- _____________________________________________________________________________________
-- Ejercicio 1.25
SELECT DISTINCT MONTH(fecha_alt)
FROM emple;
-- _____________________________________________________________________________________
-- Ejercicio 1.26
SELECT DISTINCT YEAR(fecha_alt)
FROM emple;
-- _____________________________________________________________________________________
-- Ejercicio 1.27
SELECT DISTINCT DAY(fecha_alt)
FROM emple;
-- _____________________________________________________________________________________
-- Ejercicio 1.28
SELECT DISTINCT emp_no,apellido
FROM emple
WHERE (salario>2000)OR(dept_no=20);