DROP DATABASE IF EXISTS `ajcm8.M1U2.Pract3`;
CREATE DATABASE IF NOT EXISTS `ajcm8.M1U2.Pract3`;
-- Seleccionamos la base de datos
USE `ajcm8.M1U2.Pract3`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract3`.`clientes`;
CREATE TABLE `ajcm8.M1U2.Pract3`.`clientes`(
	-- campos
	codigo INTEGER AUTO_INCREMENT COMMENT 'Clave Principal', #campo con un comentario
	nombre VARCHAR(200),
	-- indices
	PRIMARY KEY(codigo)
);

INSERT INTO `ajcm8.M1U2.Pract3`.`clientes`(codigo,nombre)
	Values
	(1,'Luis Perez Galdos'),
	(2,'Rosa Maria Gomez'),
	(3,'Pedro Gomez Suarez');
	
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract3`.`coches`;
CREATE TABLE `ajcm8.M1U2.Pract3`.`coches`(
	-- campos
	matricula VARCHAR(10),
	-- indices
	PRIMARY KEY(matricula)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract3`.`coches`(matricula)
	Values
	('45678-BLM'),
	('55555-ESP'),
	('66666-ERD');

# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract3`.`poblacion`;
CREATE TABLE `ajcm8.M1U2.Pract3`.`poblacion`(
	-- campos
	codPob INTEGER AUTO_INCREMENT,
	nombre VARCHAR(100),
	-- indices
	PRIMARY KEY(codPob)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract3`.`poblacion`(codPob,nombre)
	Values
	(1,'Cantabria'),
	(2,'Madrid'),
	(3,'Bilbao');

# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract3`.`relacion`;
CREATE TABLE `ajcm8.M1U2.Pract3`.`relacion`(
	-- campos
	codCliente INTEGER,
	codPob INTEGER,
	matricula VARCHAR(10),
	-- indices
	PRIMARY KEY(codCliente,codPob,matricula),
	CONSTRAINT fkTblRelacionTblClientes
		FOREIGN KEY (codCliente) REFERENCES `ajcm8.M1U2.Pract3`.`clientes`(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT, 
	CONSTRAINT fkTblRelacionTblPoblacion
		FOREIGN KEY (codPob) REFERENCES `ajcm8.M1U2.Pract3`.`poblacion`(codPob) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkTblRelacionTblTables
		FOREIGN KEY (matricula) REFERENCES `ajcm8.M1U2.Pract3`.`coches`(matricula) ON DELETE RESTRICT ON UPDATE RESTRICT 		
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract3`.`relacion`(codCliente,codPob,matricula)
	Values
	(1,3,'45678-BLM'),
	(3,1,'55555-ESP'),
	(2,2,'66666-ERD');

