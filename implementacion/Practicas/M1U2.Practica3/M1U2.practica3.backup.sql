-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.22-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para ajcm8.m1u2.pract3
DROP DATABASE IF EXISTS `ajcm8.m1u2.pract3`;
CREATE DATABASE IF NOT EXISTS `ajcm8.m1u2.pract3` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ajcm8.m1u2.pract3`;

-- Volcando estructura para tabla ajcm8.m1u2.pract3.clientes
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave Principal',
  `nombre` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla ajcm8.m1u2.pract3.clientes: ~3 rows (aproximadamente)
INSERT INTO `clientes` (`codigo`, `nombre`) VALUES
	(1, 'Luis Perez Galdos'),
	(2, 'Rosa Maria Gomez'),
	(3, 'Pedro Gomez Suarez');

-- Volcando estructura para tabla ajcm8.m1u2.pract3.coches
DROP TABLE IF EXISTS `coches`;
CREATE TABLE IF NOT EXISTS `coches` (
  `matricula` varchar(10) NOT NULL,
  PRIMARY KEY (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla ajcm8.m1u2.pract3.coches: ~3 rows (aproximadamente)
INSERT INTO `coches` (`matricula`) VALUES
	('45678-BLM'),
	('55555-ESP'),
	('66666-ERD');

-- Volcando estructura para tabla ajcm8.m1u2.pract3.poblacion
DROP TABLE IF EXISTS `poblacion`;
CREATE TABLE IF NOT EXISTS `poblacion` (
  `codPob` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codPob`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla ajcm8.m1u2.pract3.poblacion: ~3 rows (aproximadamente)
INSERT INTO `poblacion` (`codPob`, `nombre`) VALUES
	(1, 'Cantabria'),
	(2, 'Madrid'),
	(3, 'Bilbao');

-- Volcando estructura para tabla ajcm8.m1u2.pract3.relacion
DROP TABLE IF EXISTS `relacion`;
CREATE TABLE IF NOT EXISTS `relacion` (
  `codCliente` int(11) NOT NULL,
  `codPob` int(11) NOT NULL,
  `matricula` varchar(10) NOT NULL,
  PRIMARY KEY (`codCliente`,`codPob`,`matricula`),
  KEY `fkTblRelacionTblPoblacion` (`codPob`),
  KEY `fkTblRelacionTblTables` (`matricula`),
  CONSTRAINT `fkTblRelacionTblClientes` FOREIGN KEY (`codCliente`) REFERENCES `clientes` (`codigo`),
  CONSTRAINT `fkTblRelacionTblPoblacion` FOREIGN KEY (`codPob`) REFERENCES `poblacion` (`codPob`),
  CONSTRAINT `fkTblRelacionTblTables` FOREIGN KEY (`matricula`) REFERENCES `coches` (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla ajcm8.m1u2.pract3.relacion: ~3 rows (aproximadamente)
INSERT INTO `relacion` (`codCliente`, `codPob`, `matricula`) VALUES
	(1, 3, '45678-BLM'),
	(2, 2, '66666-ERD'),
	(3, 1, '55555-ESP');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
