-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.30-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para ajcm8.m1u2.pract6
DROP DATABASE IF EXISTS `ajcm8.m1u2.pract6`;
CREATE DATABASE IF NOT EXISTS `ajcm8.m1u2.pract6` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ajcm8.m1u2.pract6`;

-- Volcando estructura para tabla ajcm8.m1u2.pract6.cliente
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `nif` varchar(12) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  `tfno` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`nif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract6.cliente: ~3 rows (aproximadamente)
DELETE FROM `cliente`;
INSERT INTO `cliente` (`nif`, `nombre`, `direccion`, `ciudad`, `tfno`) VALUES
	('nif-1', 'Eduardo Aute', 'Gran Via, 1', 'Santander', '942-344-344'),
	('nif-2', 'Juan Guerra', 'Calle Alta, 108', 'Madrid', '94-345-342'),
	('nif-3', 'Rodolfo Langostino', 'Las Ramblas, 86', 'Caceres', '65-345-234');

-- Volcando estructura para tabla ajcm8.m1u2.pract6.coche
DROP TABLE IF EXISTS `coche`;
CREATE TABLE IF NOT EXISTS `coche` (
  `matricula` varchar(10) NOT NULL,
  `marca` varchar(100) DEFAULT NULL,
  `modelo` varchar(100) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `Precio` float DEFAULT NULL,
  PRIMARY KEY (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract6.coche: ~3 rows (aproximadamente)
DELETE FROM `coche`;
INSERT INTO `coche` (`matricula`, `marca`, `modelo`, `color`, `Precio`) VALUES
	('2323-FF', 'Mercedes', 'Mercedes Venon', 'Azul', 10000),
	('8976-BM', 'Seat', 'Seat Rondeo', 'blanco', 3000),
	('9834-CD', 'Citroen', 'Citroen Gesta', 'chocolate', 2500);

-- Volcando estructura para tabla ajcm8.m1u2.pract6.compra
DROP TABLE IF EXISTS `compra`;
CREATE TABLE IF NOT EXISTS `compra` (
  `nifCliente` varchar(12) NOT NULL,
  `matricula-coche` varchar(10) NOT NULL,
  PRIMARY KEY (`nifCliente`,`matricula-coche`),
  UNIQUE KEY `matricula-coche` (`matricula-coche`),
  CONSTRAINT `fkTblCompraTblCliente` FOREIGN KEY (`nifCliente`) REFERENCES `cliente` (`nif`),
  CONSTRAINT `fkTblCompraTblCoche` FOREIGN KEY (`matricula-coche`) REFERENCES `coche` (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract6.compra: ~3 rows (aproximadamente)
DELETE FROM `compra`;
INSERT INTO `compra` (`nifCliente`, `matricula-coche`) VALUES
	('nif-1', '8976-BM'),
	('nif-1', '9834-CD'),
	('nif-2', '2323-FF');

-- Volcando estructura para tabla ajcm8.m1u2.pract6.revision
DROP TABLE IF EXISTS `revision`;
CREATE TABLE IF NOT EXISTS `revision` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `filtro` varchar(2) DEFAULT NULL,
  `aceite` varchar(2) DEFAULT NULL,
  `frenos` varchar(2) DEFAULT NULL,
  `matricula-coche` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fkTblRevisionTblCoches` (`matricula-coche`),
  CONSTRAINT `fkTblRevisionTblCoches` FOREIGN KEY (`matricula-coche`) REFERENCES `coche` (`matricula`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract6.revision: ~3 rows (aproximadamente)
DELETE FROM `revision`;
INSERT INTO `revision` (`codigo`, `filtro`, `aceite`, `frenos`, `matricula-coche`) VALUES
	(1, 'si', 'si', 'si', '8976-BM'),
	(2, 'si', 'si', 'si', '9834-CD'),
	(3, 'si', 'si', 'si', '2323-FF');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
