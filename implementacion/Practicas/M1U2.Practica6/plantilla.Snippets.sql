/*Ejemplo de clase sobre la base de datos alumnos del alumno ajcm8*/
-- ------------------------------------------------------------------
# Snippet CrearTodo

# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `nombredatabase`;
CREATE DATABASE IF NOT EXISTS `nombredatabase`;
-- Seleccionamos la base de datos
USE `nombredatabase`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `nombredatabase`.`nombretable`;
CREATE TABLE `nombredatabase`.`nombretable`(
	-- campos
	campo1 INTEGER AUTO_INCREMENT,
	campo2 VARCHAR(100),
	campoN DATE,
	-- indices
	PRIMARY KEY alias(campo1,campo3),
	UNIQUE KEY alias(campo2,campo3),
	KEY alias(campo1,campo2),
	CONSTRAINT fkTblForeingKeyTblclaveprimaria
		FOREIGN KEY (campo2) REFERENCES `nombredatabase`.`nombretable2`(campotbl2) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `nombredatabase`.`nombretable`(campo1,campo2,campo3,campoN)
	Values
	('DatoR1campo1','DatoR1campo2','DatoR1campo3','DatoR1campoN'),
	('DatoR2campo1','DatoR2campo2','DatoR2campo3','DatoR2campoN'),
	('DatoR3campo1','DatoR3campo2','DatoR3campo3','DatoR3campoN');

