# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `ajcm8.M1U2.Pract6`;
CREATE DATABASE IF NOT EXISTS `ajcm8.M1U2.Pract6`;
-- Seleccionamos la base de datos
USE `ajcm8.M1U2.Pract6`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract6`.`cliente`;
CREATE TABLE `ajcm8.M1U2.Pract6`.`cliente`(
	-- campos
	nif VARCHAR(12),
	nombre VARCHAR(100),
	direccion VARCHAR(100),
	ciudad VARCHAR(50),
	tfno VARCHAR(12),
	-- indices
	PRIMARY KEY(nif)
	);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract6`.`cliente`(nif,nombre,direccion,ciudad,tfno)
	Values
	('nif-1','Eduardo Aute','Gran Via, 1','Santander','942-344-344'),
	('nif-2','Juan Guerra','Calle Alta, 108','Madrid','94-345-342'),
	('nif-3','Rodolfo Langostino','Las Ramblas, 86','Caceres','65-345-234');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract6`.`coche`;
CREATE TABLE `ajcm8.M1U2.Pract6`.`coche`(
	-- campos
	matricula VARCHAR(10),
	marca VARCHAR(100),
	modelo VARCHAR(100),
	color VARCHAR(50),
	Precio FLOAT,
	-- indices
	PRIMARY KEY(matricula)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract6`.`coche`(matricula,marca,modelo,color,precio)
	Values
	('8976-BM','Seat','Seat Rondeo','blanco',3000),
	('9834-CD','Citroen','Citroen Gesta','chocolate',2500),
	('2323-FF','Mercedes','Mercedes Venon','Azul',10000);
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract6`.`compra`;
CREATE TABLE `ajcm8.M1U2.Pract6`.`compra`(
	-- campos
	nifCliente VARCHAR(12),
	`matricula-coche` VARCHAR(10),
	-- indices
	PRIMARY KEY(nifCliente,`matricula-coche`),
	UNIQUE KEY uk1 (`matricula-coche`),
	CONSTRAINT fkTblCompraTblCliente
		FOREIGN KEY (nifCliente) REFERENCES `ajcm8.M1U2.Pract6`.`cliente`(nif) ON DELETE RESTRICT ON UPDATE RESTRICT, 
	CONSTRAINT fkTblCompraTblCoche
		FOREIGN KEY (`matricula-coche`) REFERENCES `ajcm8.M1U2.Pract6`.`coche`(matricula) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract6`.`compra`(nifCliente,`matricula-coche`)
	Values
	('nif-1','8976-BM'),
	('nif-1','9834-CD'),
	('nif-2','2323-FF');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract6`.`revision`;
CREATE TABLE `ajcm8.M1U2.Pract6`.`revision`(
	-- campos
	codigo INTEGER AUTO_INCREMENT,
	filtro VARCHAR(2),
	aceite VARCHAR(2),
	frenos VARCHAR(2),
	`matricula-coche` VARCHAR(10),
	-- indices
	PRIMARY KEY(codigo),
	CONSTRAINT fkTblRevisionTblCoches
		FOREIGN KEY (`matricula-coche`) REFERENCES `ajcm8.M1U2.Pract6`.`coche`(matricula) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract6`.`revision`(codigo,filtro,aceite,frenos,`matricula-coche`)
	Values
	(1,'si','si','si','8976-BM'),
	(2,'si','si','si','9834-CD'),
	(3,'si','si','si','2323-FF');