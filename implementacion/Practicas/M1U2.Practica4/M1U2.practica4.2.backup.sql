-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.30-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para ajcm8.m1u2.pract4
DROP DATABASE IF EXISTS `ajcm8.m1u2.pract4`;
CREATE DATABASE IF NOT EXISTS `ajcm8.m1u2.pract4` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ajcm8.m1u2.pract4`;

-- Volcando estructura para tabla ajcm8.m1u2.pract4.departamento
DROP TABLE IF EXISTS `departamento`;
CREATE TABLE IF NOT EXISTS `departamento` (
  `numero-d` int(11) NOT NULL,
  `nombre-d` varchar(40) NOT NULL,
  `numdeempleados` int(11) DEFAULT NULL,
  `nss-empleado-dirige` varchar(30) DEFAULT NULL,
  `fecha-inicio-jefe` date DEFAULT NULL,
  PRIMARY KEY (`nombre-d`,`numero-d`),
  UNIQUE KEY `nss-empleado-dirige` (`nss-empleado-dirige`),
  CONSTRAINT `fkTblDepartamentoTblEmpleado` FOREIGN KEY (`nss-empleado-dirige`) REFERENCES `empleado` (`nss-empleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract4.departamento: ~3 rows (aproximadamente)
DELETE FROM `departamento`;
INSERT INTO `departamento` (`numero-d`, `nombre-d`, `numdeempleados`, `nss-empleado-dirige`, `fecha-inicio-jefe`) VALUES
	(1, 'Administracion', 5, 'nss-1', '2000-01-01'),
	(3, 'Informatica', 3, 'nss-3', '2000-01-01'),
	(2, 'Mantenimiento', 4, 'nss-2', '2000-01-01');

-- Volcando estructura para tabla ajcm8.m1u2.pract4.dependiente
DROP TABLE IF EXISTS `dependiente`;
CREATE TABLE IF NOT EXISTS `dependiente` (
  `nombre_dependiente` varchar(100) NOT NULL,
  `nss-empleado` varchar(30) NOT NULL,
  `sexo` varchar(6) DEFAULT 'varon',
  `fecha_ncto` date DEFAULT NULL,
  `parentesco` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`nombre_dependiente`,`nss-empleado`),
  KEY `fkTblDependienteTblEmpleado` (`nss-empleado`),
  CONSTRAINT `fkTblDependienteTblEmpleado` FOREIGN KEY (`nss-empleado`) REFERENCES `empleado` (`nss-empleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract4.dependiente: ~3 rows (aproximadamente)
DELETE FROM `dependiente`;
INSERT INTO `dependiente` (`nombre_dependiente`, `nss-empleado`, `sexo`, `fecha_ncto`, `parentesco`) VALUES
	('Elena Rubio Gomez', 'nss-1', 'hembra', '1971-01-01', 'aa'),
	('Emilio del Castillo', 'nss-2', 'varon', '1982-01-01', 'ab'),
	('Miguel Angel Perez', 'nss-3', 'varon', '1990-01-01', 'ac');

-- Volcando estructura para tabla ajcm8.m1u2.pract4.empleado
DROP TABLE IF EXISTS `empleado`;
CREATE TABLE IF NOT EXISTS `empleado` (
  `nss-empleado` varchar(30) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `iniciales` varchar(30) DEFAULT NULL,
  `fecha_ncto` date DEFAULT NULL,
  `sexo` varchar(6) DEFAULT 'varón',
  `direccion` varchar(100) DEFAULT NULL,
  `salario` float(8,2) DEFAULT NULL,
  `nombre-d` varchar(50) DEFAULT NULL,
  `numero-d` int(11) DEFAULT NULL,
  PRIMARY KEY (`nss-empleado`),
  KEY `fkTblEmpleadoTblDepartamento` (`nombre-d`,`numero-d`),
  CONSTRAINT `fkTblEmpleadoTblDepartamento` FOREIGN KEY (`nombre-d`, `numero-d`) REFERENCES `departamento` (`nombre-d`, `numero-d`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract4.empleado: ~3 rows (aproximadamente)
DELETE FROM `empleado`;
INSERT INTO `empleado` (`nss-empleado`, `nombre`, `apellido`, `iniciales`, `fecha_ncto`, `sexo`, `direccion`, `salario`, `nombre-d`, `numero-d`) VALUES
	('nss-1', 'Juan', 'Perez,Gomez', 'JuPeGO', '1965-01-01', 'varon', 'Gran Vía,6', 1000.99, 'Informatica', 3),
	('nss-2', 'Maria', 'Vazquez Montes', 'MarVaMon', '1971-01-01', 'hembra', 'General Mola, 34', 799.99, 'Administracion', 1),
	('nss-3', 'Siam', 'Diario Juanes', 'SDJ', '1983-01-01', 'varon', 'Cardenal Cisneros, 34', 600.45, 'Mantenimiento', 2);

-- Volcando estructura para tabla ajcm8.m1u2.pract4.localizaciones
DROP TABLE IF EXISTS `localizaciones`;
CREATE TABLE IF NOT EXISTS `localizaciones` (
  `nombre-d` varchar(40) NOT NULL,
  `numero-d` int(11) NOT NULL,
  `localizacion-dept` varchar(100) NOT NULL,
  PRIMARY KEY (`nombre-d`,`numero-d`,`localizacion-dept`),
  CONSTRAINT `fkTblLocalizacionesTblDepartamento1` FOREIGN KEY (`nombre-d`, `numero-d`) REFERENCES `departamento` (`nombre-d`, `numero-d`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract4.localizaciones: ~3 rows (aproximadamente)
DELETE FROM `localizaciones`;
INSERT INTO `localizaciones` (`nombre-d`, `numero-d`, `localizacion-dept`) VALUES
	('Administracion', 1, 'calle 1'),
	('Informatica', 3, 'calle 3'),
	('Mantenimiento', 2, 'calle 2');

-- Volcando estructura para tabla ajcm8.m1u2.pract4.proyecto
DROP TABLE IF EXISTS `proyecto`;
CREATE TABLE IF NOT EXISTS `proyecto` (
  `numerop` int(11) NOT NULL,
  `nombrep` varchar(100) NOT NULL,
  `localizacion` varchar(100) DEFAULT NULL,
  `nombre-dcontrola` varchar(40) DEFAULT NULL,
  `numero-dcontrola` int(11) DEFAULT NULL,
  PRIMARY KEY (`nombrep`,`numerop`),
  KEY `fkTblProyectoTblDeparatamento1` (`nombre-dcontrola`,`numero-dcontrola`),
  CONSTRAINT `fkTblProyectoTblDeparatamento1` FOREIGN KEY (`nombre-dcontrola`, `numero-dcontrola`) REFERENCES `departamento` (`nombre-d`, `numero-d`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract4.proyecto: ~3 rows (aproximadamente)
DELETE FROM `proyecto`;
INSERT INTO `proyecto` (`numerop`, `nombrep`, `localizacion`, `nombre-dcontrola`, `numero-dcontrola`) VALUES
	(1, 'Proyecto Administracion', 'calle 1', 'Administracion', 1),
	(3, 'Proyecto Informatica', 'calle 3', 'Informatica', 3),
	(2, 'Proyecto Mantenimiento', 'calle 2', 'Mantenimiento', 2);

-- Volcando estructura para tabla ajcm8.m1u2.pract4.supervisa
DROP TABLE IF EXISTS `supervisa`;
CREATE TABLE IF NOT EXISTS `supervisa` (
  `nss-empleado` varchar(30) NOT NULL,
  `nss-supervisor` varchar(30) NOT NULL,
  PRIMARY KEY (`nss-empleado`,`nss-supervisor`),
  UNIQUE KEY `nss-empleado` (`nss-empleado`),
  KEY `fkTblSupervisaEmpleado2` (`nss-supervisor`),
  CONSTRAINT `fkTblSupervisaEmpleado1` FOREIGN KEY (`nss-empleado`) REFERENCES `empleado` (`nss-empleado`),
  CONSTRAINT `fkTblSupervisaEmpleado2` FOREIGN KEY (`nss-supervisor`) REFERENCES `empleado` (`nss-empleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract4.supervisa: ~3 rows (aproximadamente)
DELETE FROM `supervisa`;
INSERT INTO `supervisa` (`nss-empleado`, `nss-supervisor`) VALUES
	('nss-1', 'nss-2'),
	('nss-2', 'nss-2'),
	('nss-3', 'nss-2');

-- Volcando estructura para tabla ajcm8.m1u2.pract4.trabaja_en
DROP TABLE IF EXISTS `trabaja_en`;
CREATE TABLE IF NOT EXISTS `trabaja_en` (
  `nss-empleado` varchar(30) NOT NULL,
  `nombre-p` varchar(100) NOT NULL,
  `numero-p` int(11) NOT NULL,
  `horas` int(11) DEFAULT NULL,
  PRIMARY KEY (`nss-empleado`,`nombre-p`,`numero-p`),
  KEY `fkTblTrabajo_enTblProyecto` (`nombre-p`,`numero-p`),
  CONSTRAINT `fkTblTrabajo_enTblEmpleado` FOREIGN KEY (`nss-empleado`) REFERENCES `empleado` (`nss-empleado`),
  CONSTRAINT `fkTblTrabajo_enTblProyecto` FOREIGN KEY (`nombre-p`, `numero-p`) REFERENCES `proyecto` (`nombrep`, `numerop`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ajcm8.m1u2.pract4.trabaja_en: ~3 rows (aproximadamente)
DELETE FROM `trabaja_en`;
INSERT INTO `trabaja_en` (`nss-empleado`, `nombre-p`, `numero-p`, `horas`) VALUES
	('nss-1', 'Proyecto Informatica', 3, 3),
	('nss-2', 'Proyecto Administracion', 1, 10),
	('nss-3', 'Proyecto Mantenimiento', 2, 20);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
