DROP DATABASE IF EXISTS `ajcm8.M1U2.Pract4`;
CREATE DATABASE IF NOT EXISTS `ajcm8.M1U2.Pract4`;
-- Seleccionamos la base de datos
USE `ajcm8.M1U2.Pract4`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract4`.`empleado`;
CREATE TABLE `ajcm8.M1U2.Pract4`.`empleado`(
	-- campos
	`nss-empleado` VARCHAR(30),
	nombre VARCHAR(100),
	apellido VARCHAR(100),
	iniciales VARCHAR(30),
	fecha_ncto DATE,
	sexo VARCHAR(6) DEFAULT 'varón',
	direccion VARCHAR(100),
	salario FLOAT(8,2),
	`nombre-d` VARCHAR(50),
	-- indices
	PRIMARY KEY(`nss-empleado`)
/*	CONSTRAINT fkTblForeingKeyTblclaveprimaria
		FOREIGN KEY (campo2) REFERENCES `ajcm8.M1U2.Pract4`.`empleado2`(campotbl2) ON DELETE RESTRICT ON UPDATE RESTRICT */
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract4`.`empleado`(`nss-empleado`,nombre,apellido,iniciales,fecha_ncto,sexo,direccion,salario,`nombre-d`)
	Values
	('nss-1','Juan','Perez,Gomez','JuPeGO','1965/01/01','varon','Gran Vía,6',1000.99,'Informatica'),
	('nss-2','Maria','Vazquez Montes','MarVaMon','1971/01/01','hembra','General Mola, 34',799.99,'Administracion'),
	('nss-3','Siam','Diario Juanes','SDJ','1983/01/01','varon','Cardenal Cisneros, 34',600.45,'Mantenimiento');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract4`.`supervisa`;
CREATE TABLE `ajcm8.M1U2.Pract4`.`supervisa`(
	-- campos
	`nss-empleado` VARCHAR(30),
   `nss-supervisor` VARCHAR(30),
	-- indices
	PRIMARY KEY(`nss-empleado`,`nss-supervisor`),
	UNIQUE KEY(`nss-empleado`),
	CONSTRAINT fkTblSupervisaEmpleado1
		FOREIGN KEY (`nss-empleado`) REFERENCES `ajcm8.M1U2.Pract4`.`empleado`(`nss-empleado`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkTblSupervisaEmpleado2
		FOREIGN KEY (`nss-supervisor`) REFERENCES `ajcm8.M1U2.Pract4`.`empleado`(`nss-empleado`) ON DELETE RESTRICT ON UPDATE RESTRICT
	
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract4`.`supervisa`(`nss-empleado`,`nss-supervisor`)
	Values
	('nss-1','nss-2'),
	('nss-2','nss-2'),
	('nss-3','nss-2');	
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract4`.`dependiente`;
CREATE TABLE `ajcm8.M1U2.Pract4`.`dependiente`(
	-- campos
	nombre_dependiente VARCHAR(100),
	`nss-empleado` VARCHAR(30),
	sexo VARCHAR(6) DEFAULT 'varon',
	fecha_ncto DATE,
	parentesco VARCHAR(40),
	-- indices
	PRIMARY KEY(nombre_dependiente,`nss-empleado`),
	CONSTRAINT fkTblDependienteTblEmpleado
		FOREIGN KEY (`nss-empleado`) REFERENCES `ajcm8.M1U2.Pract4`.`empleado`(`nss-empleado`) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract4`.`dependiente`(nombre_dependiente,`nss-empleado`,sexo,fecha_ncto,parentesco)
	Values
	('Elena Rubio Gomez','nss-1','hembra','1971/01/01','aa'),
	('Emilio del Castillo','nss-2','varon','1982/01/01','ab'),
	('Miguel Angel Perez','nss-3','varon','1990/01/01','ac');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract4`.`departamento`;
CREATE TABLE `ajcm8.M1U2.Pract4`.`departamento`(
	-- campos
	`numero-d` INTEGER,
	`nombre-d` VARCHAR(40),
	numdeempleados INTEGER,
	`nss-empleado-dirige` VARCHAR(30),
	`fecha-inicio-jefe` DATE,
	-- indices
   PRIMARY KEY(`nombre-d`,`numero-d`),
	UNIQUE KEY(`nss-empleado-dirige`),
	CONSTRAINT fkTblDepartamentoTblEmpleado
		FOREIGN KEY (`nss-empleado-dirige`) REFERENCES `ajcm8.M1U2.Pract4`.`empleado`(`nss-empleado`) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract4`.`departamento`(`nombre-d`,`numero-d`,numdeempleados,`nss-empleado-dirige`,`fecha-inicio-jefe`)
	Values
	('Administracion',1,5,'nss-1','2000/01/01'),
	('Mantenimiento',2,4,'nss-2','2000/01/01'),
	('Informatica',3,3,'nss-3','2000/01/01');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract4`.`localizaciones`;
CREATE TABLE `ajcm8.M1U2.Pract4`.`localizaciones`(
	-- campos
	`nombre-d` VARCHAR(40),
	`numero-d` INTEGER,
	`localizacion-dept` VARCHAR(100),
	-- indices
	PRIMARY KEY(`nombre-d`,`numero-d`,`localizacion-dept`),
	CONSTRAINT fkTblLocalizacionesTblDepartamento1
		FOREIGN KEY (`nombre-d`,`numero-d`) REFERENCES `ajcm8.M1U2.Pract4`.`departamento`(`nombre-d`,`numero-d`) ON DELETE RESTRICT ON UPDATE RESTRICT
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract4`.`localizaciones`(`nombre-d`,`numero-d`,`localizacion-dept`)
	Values
	('Administracion',1,'calle 1'),
	('Mantenimiento',2,'calle 2'),
	('Informatica',3,'calle 3');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract4`.`proyecto`;
CREATE TABLE `ajcm8.M1U2.Pract4`.`proyecto`(
	-- campos
	numerop INTEGER,
	nombrep VARCHAR(100),
	localizacion VARCHAR(100),
	`nombre-dcontrola` VARCHAR(40),
	`numero-dcontrola` INTEGER,
	-- indices
	PRIMARY KEY(nombrep,numerop),
	CONSTRAINT fkTblProyectoTblDeparatamento1
		FOREIGN KEY (`nombre-dcontrola`,`numero-dcontrola`) REFERENCES `ajcm8.M1U2.Pract4`.`departamento`(`nombre-d`,`numero-d`) ON DELETE RESTRICT ON UPDATE RESTRICT
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract4`.`proyecto`(`numerop`,`nombrep`,`localizacion`,`nombre-dcontrola`,`numero-dcontrola`)
	Values
	(1,'Proyecto Administracion','calle 1','Administracion',1),
	(2,'Proyecto Mantenimiento','calle 2','Mantenimiento',2),
	(3,'Proyecto Informatica','calle 3','Informatica',3);
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract4`.`trabaja_en`;
CREATE TABLE `ajcm8.M1U2.Pract4`.`trabaja_en`(
	-- campos
	`nss-empleado` VARCHAR(30),
	`nombre-p` VARCHAR(100),
	`numero-p` INTEGER,
   horas INTEGER,
	-- indices
	PRIMARY KEY(`nss-empleado`,`nombre-p`,`numero-p`),
	CONSTRAINT fkTblTrabajo_enTblEmpleado
		FOREIGN KEY (`nss-empleado`) REFERENCES `ajcm8.M1U2.Pract4`.`empleado`(`nss-empleado`) ON DELETE RESTRICT ON UPDATE RESTRICT, 
   CONSTRAINT fkTblTrabajo_enTblProyecto
		FOREIGN KEY (`nombre-p`,`numero-p`) REFERENCES `ajcm8.M1U2.Pract4`.`proyecto`(`nombrep`,`numerop`) ON DELETE RESTRICT ON UPDATE RESTRICT
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract4`.`trabaja_en`(`nss-empleado`,`nombre-p`,`numero-p`,horas)
	Values
	('nss-2','Proyecto Administracion',1,10),
	('nss-3','Proyecto Mantenimiento',2,20),
	('nss-1','Proyecto Informatica',3,3);

ALTER TABLE empleado
   ADD COLUMN `numero-d` INTEGER,
	ADD CONSTRAINT fkTblEmpleadoTblDepartamento
		FOREIGN KEY (`nombre-d`,`numero-d`) REFERENCES `ajcm8.M1U2.Pract4`.`departamento`(`nombre-d`,`numero-d`) ON DELETE RESTRICT ON UPDATE RESTRICT;

