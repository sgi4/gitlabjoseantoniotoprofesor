-- ------------------------------------------------------------------
# Snippet CrearTodo

# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `ajcm8.M1U2.Pract5`;
CREATE DATABASE IF NOT EXISTS `ajcm8.M1U2.Pract5`;
-- Seleccionamos la base de datos
USE `ajcm8.M1U2.Pract5`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract5`.`alumno`;
CREATE TABLE `ajcm8.M1U2.Pract5`.`alumno`(
	-- campos
	expediente INTEGER AUTO_INCREMENT,
	nombre VARCHAR(100),
	apellidos VARCHAR(100),
	fecha_nac DATE,
	-- indices
	PRIMARY KEY(expediente)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract5`.`alumno`(expediente,nombre,apellidos,fecha_nac)
	Values
	(1,'Ana Maria','Perez Galdos','1971/01/01'),
	(2,'Juan Carlos','Gomez del Rio','1980/01/01'),
	(3,'Sonia','Grande Suarez','1973/01/01');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract5`.`esdelegado`;
CREATE TABLE `ajcm8.M1U2.Pract5`.`esdelegado`(
	-- campos
	`expediente-delegado` INTEGER,
	`expediente-alumno` INTEGER,
	-- indices
	PRIMARY KEY(`expediente-delegado`,`expediente-alumno`),
	UNIQUE KEY(`expediente-alumno`),
	CONSTRAINT fkTblEsdelegadoTblAlumno1
		FOREIGN KEY (`expediente-delegado`) REFERENCES `ajcm8.M1U2.Pract5`.`alumno`(`expediente`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkTblEsdelegadoTblAlumno2
		FOREIGN KEY (`expediente-alumno`) REFERENCES `ajcm8.M1U2.Pract5`.`alumno`(`expediente`) ON DELETE RESTRICT ON UPDATE RESTRICT 
		
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract5`.`esdelegado`(`expediente-delegado`,`expediente-alumno`)
	Values
	(1,1),
	(1,2),
	(1,3);
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract5`.`modulo`;
CREATE TABLE `ajcm8.M1U2.Pract5`.`modulo`(
	-- campos
	codigo INTEGER AUTO_INCREMENT,
	nombre VARCHAR(100),
	-- indices
	PRIMARY KEY(codigo)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract5`.`modulo`(codigo,nombre)
	Values
	(1,'Administracion'),
	(2,'Informatica'),
	(3,'Contabilidad');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract5`.`cursa`;
CREATE TABLE `ajcm8.M1U2.Pract5`.`cursa`(
	-- campos
	`expediente-alumno` INTEGER,
	`codigo-modulo` INTEGER,
	-- indices
	PRIMARY KEY(`expediente-alumno`,	`codigo-modulo`),
	CONSTRAINT fkTblCursaTblAlumno
		FOREIGN KEY (`expediente-alumno`) REFERENCES `ajcm8.M1U2.Pract5`.`alumno`(`expediente`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkTblCursaTblModulo
		FOREIGN KEY (`codigo-modulo`) REFERENCES `ajcm8.M1U2.Pract5`.`modulo`(`codigo`) ON DELETE RESTRICT ON UPDATE RESTRICT
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract5`.`cursa`(`expediente-alumno`,`codigo-modulo`)
	Values
	(1,3),
	(2,2),
	(3,3);
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract5`.`profesor`;
CREATE TABLE `ajcm8.M1U2.Pract5`.`profesor`(
	-- campos
	dni VARCHAR(12),
	nombre VARCHAR(100),
	direccion VARCHAR(100),
	tfno VARCHAR(12),
	-- indices
	PRIMARY KEY(dni)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract5`.`profesor`(dni,nombre,direccion,tfno)
	Values
	('dni-1','Profesor-1','calle-1','telefono-1'),
	('dni-2','Profesor-2','calle-2','telefono-2'),
	('dni-3','Profesor-3','calle-3','telefono-3');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract5`.`imparte`;
CREATE TABLE `ajcm8.M1U2.Pract5`.`imparte`(
	-- campos
	`dni-profesor` VARCHAR(12),
	`codigo-modulo` INTEGER,
	-- indices
	PRIMARY KEY(`dni-profesor`,`codigo-modulo`),
	UNIQUE KEY(`codigo-modulo`),
	CONSTRAINT fkTblImparteTblProfesor
		FOREIGN KEY (`dni-profesor`) REFERENCES `ajcm8.M1U2.Pract5`.`profesor`(dni) ON DELETE RESTRICT ON UPDATE RESTRICT, 
	CONSTRAINT fkTblImparteTblModulo
		FOREIGN KEY (`codigo-modulo`) REFERENCES `ajcm8.M1U2.Pract5`.`modulo`(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT 

);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract5`.`imparte`(`dni-profesor`,`codigo-modulo`)
	Values
	('dni-1',1),
	('dni-1',2),
	('dni-2',3);