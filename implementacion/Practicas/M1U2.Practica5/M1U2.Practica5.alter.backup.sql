-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.22-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para ajcm8.m1u2.pract5
DROP DATABASE IF EXISTS `ajcm8.m1u2.pract5`;
CREATE DATABASE IF NOT EXISTS `ajcm8.m1u2.pract5` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ajcm8.m1u2.pract5`;

-- Volcando estructura para tabla ajcm8.m1u2.pract5.alumno
DROP TABLE IF EXISTS `alumno`;
CREATE TABLE IF NOT EXISTS `alumno` (
  `expediente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  PRIMARY KEY (`expediente`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla ajcm8.m1u2.pract5.alumno: ~3 rows (aproximadamente)
INSERT INTO `alumno` (`expediente`, `nombre`, `apellidos`, `fecha_nac`) VALUES
	(1, 'Ana Maria', 'Perez Galdos', '1971-01-01'),
	(2, 'Juan Carlos', 'Gomez del Rio', '1980-01-01'),
	(3, 'Sonia', 'Grande Suarez', '1973-01-01');

-- Volcando estructura para tabla ajcm8.m1u2.pract5.cursa
DROP TABLE IF EXISTS `cursa`;
CREATE TABLE IF NOT EXISTS `cursa` (
  `expediente-alumno` int(11) NOT NULL,
  `codigo-modulo` int(11) NOT NULL,
  PRIMARY KEY (`expediente-alumno`,`codigo-modulo`),
  KEY `fkTblCursaTblModulo` (`codigo-modulo`),
  CONSTRAINT `fkTblCursaTblAlumno` FOREIGN KEY (`expediente-alumno`) REFERENCES `alumno` (`expediente`),
  CONSTRAINT `fkTblCursaTblModulo` FOREIGN KEY (`codigo-modulo`) REFERENCES `modulo` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla ajcm8.m1u2.pract5.cursa: ~3 rows (aproximadamente)
INSERT INTO `cursa` (`expediente-alumno`, `codigo-modulo`) VALUES
	(1, 3),
	(2, 2),
	(3, 3);

-- Volcando estructura para tabla ajcm8.m1u2.pract5.esdelegado
DROP TABLE IF EXISTS `esdelegado`;
CREATE TABLE IF NOT EXISTS `esdelegado` (
  `expediente-delegado` int(11) NOT NULL,
  `expediente-alumno` int(11) NOT NULL,
  PRIMARY KEY (`expediente-delegado`,`expediente-alumno`),
  UNIQUE KEY `expediente-alumno` (`expediente-alumno`),
  CONSTRAINT `fkTblEsdelegadoTblAlumno1` FOREIGN KEY (`expediente-delegado`) REFERENCES `alumno` (`expediente`),
  CONSTRAINT `fkTblEsdelegadoTblAlumno2` FOREIGN KEY (`expediente-alumno`) REFERENCES `alumno` (`expediente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla ajcm8.m1u2.pract5.esdelegado: ~3 rows (aproximadamente)
INSERT INTO `esdelegado` (`expediente-delegado`, `expediente-alumno`) VALUES
	(1, 1),
	(1, 2),
	(1, 3);

-- Volcando estructura para tabla ajcm8.m1u2.pract5.imparte
DROP TABLE IF EXISTS `imparte`;
CREATE TABLE IF NOT EXISTS `imparte` (
  `dni-profesor` varchar(12) NOT NULL,
  `codigo-modulo` int(11) NOT NULL,
  PRIMARY KEY (`dni-profesor`,`codigo-modulo`),
  UNIQUE KEY `codigo-modulo` (`codigo-modulo`),
  CONSTRAINT `fkTblImparteTblModulo` FOREIGN KEY (`codigo-modulo`) REFERENCES `modulo` (`codigo`),
  CONSTRAINT `fkTblImparteTblProfesor` FOREIGN KEY (`dni-profesor`) REFERENCES `profesor` (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla ajcm8.m1u2.pract5.imparte: ~3 rows (aproximadamente)
INSERT INTO `imparte` (`dni-profesor`, `codigo-modulo`) VALUES
	('dni-1', 1),
	('dni-1', 2),
	('dni-2', 3);

-- Volcando estructura para tabla ajcm8.m1u2.pract5.modulo
DROP TABLE IF EXISTS `modulo`;
CREATE TABLE IF NOT EXISTS `modulo` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla ajcm8.m1u2.pract5.modulo: ~3 rows (aproximadamente)
INSERT INTO `modulo` (`codigo`, `nombre`) VALUES
	(1, 'Administracion'),
	(2, 'Informatica'),
	(3, 'Contabilidad');

-- Volcando estructura para tabla ajcm8.m1u2.pract5.profesor
DROP TABLE IF EXISTS `profesor`;
CREATE TABLE IF NOT EXISTS `profesor` (
  `dni` varchar(12) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `tfno` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla ajcm8.m1u2.pract5.profesor: ~3 rows (aproximadamente)
INSERT INTO `profesor` (`dni`, `nombre`, `direccion`, `tfno`) VALUES
	('dni-1', 'Profesor-1', 'calle-1', 'telefono-1'),
	('dni-2', 'Profesor-2', 'calle-2', 'telefono-2'),
	('dni-3', 'Profesor-3', 'calle-3', 'telefono-3');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
