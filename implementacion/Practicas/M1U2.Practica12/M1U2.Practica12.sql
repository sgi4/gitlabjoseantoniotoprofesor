SET NAMES 'utf8';
USE `ajcm8.ciclistas`;
-- _____________________________________________________________________________________
-- Ejercicio 1,1
SELECT DISTINCT nombre,edad
FROM ciclista
JOIN etapa
ON ciclista.dorsal=etapa.dorsal;
-- _____________________________________________________________________________________
-- Ejercicio 1.2
SELECT DISTINCT nombre,edad
FROM ciclista
JOIN puerto
ON ciclista.dorsal=puerto.dorsal;
-- _____________________________________________________________________________________
-- Ejercicio 1.3
SELECT DISTINCT nombre,edad
FROM ciclista
JOIN etapa
ON (ciclista.dorsal=etapa.dorsal)
JOIN puerto
ON (ciclista.dorsal=puerto.dorsal);
-- _____________________________________________________________________________________
-- Ejercicio 1.4
SELECT DISTINCT director
FROM equipo
JOIN ciclista
ON (ciclista.nomequipo=equipo.nomequipo)
JOIN etapa
ON (ciclista.dorsal=etapa.dorsal);
-- _________________________________
-- _____________________________________________________________________________________
-- Ejercicio 1.5
SELECT DISTINCT ciclista.dorsal,ciclista.nombre
FROM ciclista
JOIN lleva
ON (ciclista.dorsal=lleva.dorsal)
JOIN maillot
ON (lleva.código=maillot.código);
_____________________________________________________________________________________
-- Ejercicio 1.6
SELECT DISTINCT ciclista.dorsal,ciclista.nombre
FROM ciclista
JOIN lleva
ON (ciclista.dorsal=lleva.dorsal)
JOIN maillot
ON (lleva.código=maillot.código)
WHERE lleva.código='MGE';
-- _____________________________________________________________________________________
-- Ejercicio 1.7
SELECT DISTINCT ciclista.dorsal
FROM ciclista
JOIN lleva
ON (ciclista.dorsal=lleva.dorsal)
JOIN etapa
ON (ciclista.dorsal=etapa.dorsal);
-- _____________________________________________________________________________________
-- Ejercicio 1.8
SELECT DISTINCT etapa.numetapa,etapa.kms
FROM etapa
JOIN puerto
ON (etapa.numetapa=puerto.numetapa);
-- _____________________________________________________________________________________
-- Ejercicio 1.9
SELECT DISTINCT ciclista.nombre,etapa.numetapa,etapa.kms
FROM ciclista
JOIN etapa
ON (etapa.dorsal=ciclista.dorsal)
JOIN puerto
ON (puerto.numetapa=etapa.numetapa)
WHERE ciclista.nomequipo='Banesto';
-- _____________________________________________________________________________________
-- Ejericicio 1.10
SELECT DISTINCT COUNT(*)
FROM ciclista
JOIN etapa
ON (etapa.dorsal=ciclista.dorsal)
JOIN puerto
ON (puerto.numetapa=etapa.numetapa);
-- _____________________________________________________________________________________
-- Ejercicio 1.11
SELECT DISTINCT puerto.nompuerto
FROM ciclista
JOIN etapa
ON (etapa.dorsal=ciclista.dorsal)
JOIN puerto
ON (puerto.numetapa=etapa.numetapa)
WHERE ciclista.nomequipo='Banesto';
