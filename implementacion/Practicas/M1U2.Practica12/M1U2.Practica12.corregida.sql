SET NAMES 'utf8';
USE `ajcm8.ciclistas`;
-- _____________________________________________________________________________________
-- Ejercicio 1,1
SELECT DISTINCT nombre,edad
FROM ciclista
JOIN etapa
ON ciclista.dorsal=etapa.dorsal;
-- mejora a la consulta anterior
-- creamos una subconsulta c1 de dorsales ciclistas que han ganado etapas
SELECT DISTINCT etapa.dorsal
from etapa;
-- combinamos la tabla ciclista con c1
SELECT ciclista.nombre,ciclista.edad
FROM ciclista
NATURAL JOIN (SELECT DISTINCT etapa.dorsal
from etapa) AS c1;

SELECT ciclista.nombre,ciclista.edad
FROM ciclista
INNER JOIN (SELECT DISTINCT etapa.dorsal
from etapa) AS c1 ON ciclista.dorsal=c1.dorsal;

SELECT ciclista.nombre,ciclista.edad
FROM ciclista,(SELECT DISTINCT etapa.dorsal
from etapa) AS c1 
where ciclista.dorsal=c1.dorsal;

SELECT ciclista.nombre,ciclista.edad
FROM ciclista
where ciclista.dorsal IN (SELECT DISTINCT etapa.dorsal
from etapa);
-- _____________________________________________________________________________________
-- Ejercicio 1.2
SELECT DISTINCT nombre,edad
FROM ciclista
JOIN puerto
ON ciclista.dorsal=puerto.dorsal;
-- como en la consulta anterior podemmos optimizar la consulta
-- pero para no peder tiempo lo analizaremos cuando tomemos tiempos entre consultas.
-- _____________________________________________________________________________________
-- Ejercicio 1.3
SELECT DISTINCT nombre,edad
FROM ciclista
JOIN etapa
ON (ciclista.dorsal=etapa.dorsal)
JOIN puerto
ON (ciclista.dorsal=puerto.dorsal);
-- esta tambien se puede optimizar una posible de las muchas consultas posibles que hay
SELECT ciclista.nombre,ciclista.edad
FROM ciclista
WHERE ciclista.dorsal IN (SELECT * FROM (SELECT etapa.dorsal FROM etapa) AS c1
NATURAL JOIN (SELECT puerto.dorsal FROM puerto) AS c2);
-- _____________________________________________________________________________________
-- Ejercicio 1.4
SELECT DISTINCT director
FROM equipo
JOIN ciclista
ON (ciclista.nomequipo=equipo.nomequipo)
JOIN etapa
ON (ciclista.dorsal=etapa.dorsal);
-- esta tambien se puede optimizar
SELECT DISTINCT equipo.director
FROM equipo
INNER JOIN (SELECT DISTINCT * FROM (SELECT DISTINCT ciclista.nomequipo,ciclista.dorsal FROM ciclista)AS c1
NATURAL JOIN (SELECT DISTINCT etapa.dorsal FROM etapa) AS c2) AS c3 ON equipo.nomequipo=c3.nomequipo;
-- _________________________________
-- _____________________________________________________________________________________
-- Ejercicio 1.5
SELECT DISTINCT ciclista.dorsal,ciclista.nombre
FROM ciclista
JOIN lleva
ON (ciclista.dorsal=lleva.dorsal);
/*JOIN maillot
ON (lleva.código=maillot.código);*/
-- esta tambien se puede optimizar con subconsultas.
_____________________________________________________________________________________
-- Ejercicio 1.6
SELECT DISTINCT ciclista.dorsal,ciclista.nombre
FROM ciclista
JOIN lleva
ON (ciclista.dorsal=lleva.dorsal)
JOIN maillot
ON (lleva.código=maillot.código)
WHERE maillot.`código`='MGE';

SELECT DISTINCT ciclista.dorsal,ciclista.nombre
FROM ciclista
JOIN lleva
ON (ciclista.dorsal=lleva.dorsal)
JOIN maillot
ON (lleva.código=maillot.código)
WHERE maillot.color='amarillo';
-- esta tambien se puede optimizar con subconsultas
-- _____________________________________________________________________________________
-- Ejercicio 1.7
SELECT DISTINCT ciclista.dorsal
FROM ciclista
JOIN lleva
ON (ciclista.dorsal=lleva.dorsal)
JOIN etapa
ON (ciclista.dorsal=etapa.dorsal);

SELECT DISTINCT lleva.dorsal
FROM lleva
INNER JOIN etapa ON lleva.dorsal=etapa.dorsal;
-- tambien se puede optimizar con subconsultas

SELECT DISTINCT c1.dorsal
FROM (SELECT lleva.dorsal FROM lleva)AS c1
INNER JOIN (SELECT etapa.dorsal FROM etapa) AS c2 ON c1.dorsal=c2.dorsal;

SELECT DISTINCT c1.dorsal
FROM (SELECT lleva.dorsal FROM lleva)AS c1
NATURAL JOIN (SELECT etapa.dorsal FROM etapa) AS c2;

SELECT DISTINCT c1.dorsal
FROM (SELECT lleva.dorsal FROM lleva)AS c1
where c1.dorsal IN (SELECT etapa.dorsal FROM etapa);
-- _____________________________________________________________________________________
-- Ejercicio 1.8
SELECT DISTINCT etapa.numetapa,etapa.kms
FROM etapa
JOIN puerto
ON (etapa.numetapa=puerto.numetapa);
-- tambien se puede optimizar con subconsultas
-- _____________________________________________________________________________________
-- Ejercicio 1.9
SELECT DISTINCT ciclista.nombre,etapa.numetapa,etapa.kms
FROM ciclista
JOIN etapa
ON (etapa.dorsal=ciclista.dorsal)
JOIN puerto
ON (puerto.numetapa=etapa.numetapa)
WHERE ciclista.nomequipo='Banesto';
-- tambien se puede optimizar
SELECT DISTINCT ciclista.dorsal FROM ciclista WHERE ciclista.nomequipo="Banesto";

SELECT DISTINCT puerto.numetapa FROM puerto;

SELECT DISTINCT c1.nombre,etapa.numetapa,etapa.kms
FROM etapa
NATURAL JOIN (
SELECT DISTINCT ciclista.nombre,ciclista.dorsal FROM ciclista WHERE ciclista.nomequipo="Banesto"
) AS c1
NATURAL JOIN (
SELECT DISTINCT puerto.numetapa FROM puerto
) AS c2;
-- _____________________________________________________________________________________
-- Ejericicio 1.10
SELECT DISTINCT COUNT(DISTINCT ciclista.dorsal)
FROM ciclista
JOIN etapa
ON (etapa.dorsal=ciclista.dorsal)
JOIN puerto
ON (puerto.numetapa=etapa.numetapa);

SELECT DISTINCT COUNT(DISTINCT etapa.dorsal)
FROM etapa
JOIN puerto ON (puerto.numetapa=etapa.numetapa);

SELECT DISTINCT COUNT(DISTINCT etapa.dorsal)
FROM etapa
NATURAL JOIN (SELECT DISTINCT puerto.numetapa FROM puerto) AS c1;

SELECT DISTINCT COUNT(DISTINCT etapa.dorsal)
FROM etapa
WHERE etapa.numetapa in (SELECT DISTINCT puerto.numetapa FROM puerto);
-- _____________________________________________________________________________________
-- Ejercicio 1.11
SELECT DISTINCT puerto.nompuerto
FROM ciclista
JOIN etapa
ON (etapa.dorsal=ciclista.dorsal)
JOIN puerto
ON (puerto.numetapa=etapa.numetapa)
WHERE ciclista.nomequipo='Banesto';

SELECT DISTINCT puerto.nompuerto
FROM ciclista
JOIN puerto
ON (ciclista.dorsal=puerto.dorsal)
WHERE ciclista.nomequipo='Banesto';

--consulta optimizada
-- c1
SELECT DISTINCT ciclista.dorsal FROM ciclista WHERE ciclista.nomequipo="Banesto";

SELECT DISTINCT puerto.nompuerto
FROM puerto
INNER JOIN (
SELECT DISTINCT ciclista.dorsal FROM ciclista WHERE ciclista.nomequipo="Banesto"
) AS c1 ON puerto.dorsal=c1.dorsal;
