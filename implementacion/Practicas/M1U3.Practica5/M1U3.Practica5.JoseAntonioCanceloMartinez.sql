-- Modulo-1Unidad-3 Practica 5
DROP DATABASE if EXISTS `ajcm8.M1U3.Practica5`;
CREATE DATABASE `ajcm8.M1U3.Practica5`;
USE `ajcm8.M1U3.Practica5`;

-- Ejercicio 1

DROP PROCEDURE if EXISTS ejercicio1;
delimiter //
CREATE PROCEDURE ejercicio1(IN lado float)
BEGIN
	DECLARE perimetro FLOAT;
	DECLARE superficie FLOAT;
	
	SET superficie=POW(lado,2);
	SET perimetro=lado*4;
	
	CREATE TABLE if NOT EXISTS ejercicio1(
		numero INTEGER AUTO_INCREMENT,
		lado FLOAT,
		perimetro FLOAT,
		superficie FLOAT,
		PRIMARY KEY(numero)
	);
	INSERT INTO ejercicio1(numero,lado,perimetro,superficie)VALUES
	(DEFAULT,lado,perimetro,superficie);
END//
delimiter ;

CALL ejercicio1(2);
CALL ejercicio1(4);

-- Ejercicio 1

DROP PROCEDURE if EXISTS ejercicio2;
delimiter //
CREATE PROCEDURE ejercicio2(IN lado float)
BEGIN
	DECLARE perimetro FLOAT;
	DECLARE superficie FLOAT;
	DECLARE volumen FLOAT;
	
	SET superficie=POW(lado,2);
	SET perimetro=lado*4;
	SET volumen=POW(lado,3);
	
	CREATE TABLE if NOT EXISTS ejercicio2(
		numero INTEGER AUTO_INCREMENT,
		lado FLOAT,
		perimetro FLOAT,
		superficie FLOAT,
		volumen FLOAT,
		PRIMARY KEY(numero)
	);
	INSERT INTO ejercicio2(numero,lado,perimetro,superficie,volumen)VALUES
	(DEFAULT,lado,perimetro,superficie,volumen);
END//
delimiter ;

CALL ejercicio2(2);
CALL ejercicio2(4);

-- Ejercicio 3

DROP PROCEDURE if EXISTS ejercicio3;
delimiter //
CREATE PROCEDURE ejercicio3(IN lado float)
BEGIN
	DECLARE volumen FLOAT;
	DECLARE superficie FLOAT;
	
	SET superficie=POW(lado,2)*6;
	SET volumen=POW(lado,3);
	
	CREATE TABLE if NOT EXISTS ejercicio3(
		numero INTEGER AUTO_INCREMENT,
		lado FLOAT,
		volumen FLOAT,
		superficie FLOAT,
		PRIMARY KEY(numero)
	);
	INSERT INTO ejercicio3(numero,lado,volumen,superficie)VALUES
	(DEFAULT,lado,volumen,superficie);
END//
delimiter ;

CALL ejercicio3(2);
CALL ejercicio3(4);

-- Ejercicio 4

DROP FUNCTION if EXISTS ejercicio4;
delimiter //
CREATE FUNCTION ejercicio4(lado FLOAT)
RETURNS FLOAT
BEGIN
	DECLARE volumen FLOAT;
	SET volumen=POW(lado,3);
	RETURN volumen;
END//
delimiter ;

SELECT ejercicio4(2),ejercicio4(4),ejercicio4(3);

-- Ejercicio5

DROP FUNCTION if EXISTS ejercicio5;
delimiter //
CREATE FUNCTION ejercicio5(lado FLOAT)
RETURNS FLOAT
BEGIN
	DECLARE areaTotal FLOAT;
	SET areaTotal=POW(lado,2)*6;
	RETURN areaTotal;
END//
delimiter ;

SELECT ejercicio5(2),ejercicio5(4),ejercicio5(3);

-- Ejercicio 6

DROP PROCEDURE if EXISTS ejercicio6;
delimiter //
CREATE PROCEDURE ejercicio6(IN lado float)
BEGIN
	DECLARE volumen FLOAT;
	DECLARE superficie FLOAT;
	
	SET superficie=ejercicio5(lado);
	SET volumen=ejercicio4(lado);
	
	CREATE TABLE if NOT EXISTS ejercicio6(
		numero INTEGER AUTO_INCREMENT,
		lado FLOAT,
		volumen FLOAT,
		superficie FLOAT,
		PRIMARY KEY(numero)
	);
	INSERT INTO ejercicio6(numero,lado,volumen,superficie)VALUES
	(DEFAULT,lado,volumen,superficie);
END//
delimiter ;

CALL ejercicio6(2);
CALL ejercicio6(4);

-- Ejercicio 7

DROP FUNCTION if EXISTS ejercicio7;
delimiter //
CREATE FUNCTION ejercicio7(radio FLOAT,altura float)
RETURNS FLOAT
BEGIN
	DECLARE volumen FLOAT;
	SET volumen=PI()*POW(radio,2)*altura;
	RETURN volumen;
END//
delimiter ;

SELECT ejercicio7(2,2),ejercicio7(4,2),ejercicio7(3,2);

-- ejercicio 8

DROP FUNCTION if EXISTS ejercicio8;
delimiter //
CREATE FUNCTION ejercicio8(radio FLOAT,altura float)
RETURNS FLOAT
BEGIN
	DECLARE areaLateral FLOAT;
	SET areaLateral=2*PI()*radio*altura;
	RETURN areaLateral;
END//
delimiter ;

SELECT ejercicio8(2,2),ejercicio8(4,2),ejercicio8(3,2);

-- ejercicio 9

DROP FUNCTION if EXISTS ejercicio9;
delimiter //
CREATE FUNCTION ejercicio9(radio FLOAT,altura float)
RETURNS FLOAT
BEGIN
	DECLARE areaTotal FLOAT;
	SET areaTotal=2*PI()*radio*(radio+altura);
	RETURN areaTotal;
END//
delimiter ;

SELECT ejercicio9(2,2),ejercicio9(4,2),ejercicio9(3,2);

-- ejercicio 10

DROP PROCEDURE if EXISTS ejercicio10;
delimiter //
CREATE PROCEDURE ejercicio10(IN radio FLOAT,IN altura float)
COMMENT 'calcula datos de un cilindro'
BEGIN
	DECLARE areaLateral FLOAT;
	DECLARE areaTotal FLOAT;
	DECLARE volumen FLOAT;
	
	SET areaLateral=ejercicio8(radio,altura);
	SET areaTotal=ejercicio9(radio,altura);
	SET volumen=ejercicio7(radio,altura);
	
	CREATE TABLE if NOT EXISTS ejercicio10(
		numero INTEGER AUTO_INCREMENT,
		radio FLOAT,
		altura FLOAT,
		areaLateral FLOAT,
		areaTotal FLOAT,
		volumen FLOAT,
		PRIMARY KEY(numero)
	);
	INSERT INTO ejercicio10(numero,radio,altura,areaLateral,areaTotal,volumen)VALUES
	(DEFAULT,radio,altura,areaLateral,areaTotal,volumen);
END//
delimiter ;

CALL ejercicio10(2,2);
CALL ejercicio10(4,2);