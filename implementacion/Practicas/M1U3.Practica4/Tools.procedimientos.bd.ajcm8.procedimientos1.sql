-- En clase..

-- Ejercicio 1
SET NAMES 'utf8';
DROP DATABASE if EXISTS `ajcm8.procedimientos1`;
CREATE DATABASE `ajcm8.procedimientos1`;
USE `ajcm8.procedimientos1`;

-- Ejercicio 2
SET NAMES 'utf8';
USE `ajcm8.procedimientos1`;
-- DROP PROCEDURE if EXISTS ejercicio1;
DELIMITER //
CREATE PROCEDURE ejercicio1()
COMMENT 'ejemplo de procedimiento almacenado'
BEGIN
  DROP TABLE if EXISTS ciudades;
  CREATE TABLE ciudades(
  		id INTEGER AUTO_INCREMENT,
  		nombre VARCHAR(100),
  		PRIMARY KEY(id)
  );
END //
DELIMITER ;

CALL ejercicio1();

-- Ejercicio 3

SHOW CREATE PROCEDURE ejercicio1;

-- Ejercicio 4
SHOW PROCEDURE STATUS WHERE db="ajcm8.procedimientos1";

-- or

SELECT * FROM mysql.proc p WHERE p.type="procedure" AND p.db="ajcm8.procedimientos1";

-- Ejercicio 5

CALL ejercicio1();

SHOW TABLES FROM `ajcm8.procedimientos1`;

DESCRIBE ciudades;
EXPLAIN ciudades;

-- Ejercicio 6 y 7

SET NAMES 'utf8';
USE `ajcm8.procedimientos1`;
DROP PROCEDURE if EXISTS ejercicio2;
DELIMITER //
CREATE PROCEDURE ejercicio2()
COMMENT 'ejemplo de procedimiento almacenado'
BEGIN
	DROP TABLE if EXISTS personas;
	CREATE TABLE if NOT EXISTS personas(
		id INTEGER AUTO_INCREMENT,
		nombre VARCHAR(100),
		ciudad integer,
		PRIMARY KEY(id),
		CONSTRAINT fktblpersonasTOtblciudades FOREIGN KEY (ciudad) REFERENCES ciudades(id) ON UPDATE RESTRICT ON DELETE RESTRICT
	);
END //
DELIMITER ;

CALL ejercicio2();

-- Ejercicio 8
SHOW PROCEDURE STATUS WHERE db="ajcm8.procedimientos1";

-- or

SELECT * FROM mysql.proc p WHERE p.type="procedure" AND p.db="ajcm8.procedimientos1";

-- Ejercicio 9

DROP FUNCTION if EXISTS f1;
DELIMITER //
CREATE FUNCTION f1()
RETURNS VARCHAR(50)
BEGIN
  RETURN CONCAT_WS(" ","hoy es ",CURDATE()," y son ",CURTIME());
END//
DELIMITER ;

-- Ejercicio 10

SELECT f1();

-- Ejercicio 11

SHOW FUNCTION STATUS WHERE db="ajcm8.procedimientos1";

-- Ejercicio 12

SHOW CREATE FUNCTION f1;

-- -----------------------------------------------------------------------------------------
-- Realizar la documentacion de la base de datos
-- -----------------------------------------------------------------------------------------

-- dbforge comando de menu crear documentacion