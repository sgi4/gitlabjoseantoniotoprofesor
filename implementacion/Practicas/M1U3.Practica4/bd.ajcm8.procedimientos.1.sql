-- en casa (parece que todo funciona)
-- Ejercicios Modulo1-Unidad3
-- Practica4

-- Ejercicio 1

DROP DATABASE if EXISTS `ajcm8.procedimientos1`;
CREATE DATABASE `ajcm8.procedimientos1`;
USE  `ajcm8.procedimientos1`;

-- Ejercicio 2

DROP PROCEDURE if EXISTS uno;
delimiter //
CREATE PROCEDURE uno()
COMMENT 'ejemplo de procedimiento almacenado'
BEGIN 
	DROP TABLE if EXISTS ciudades;
	
	CREATE TABLE ciudades(
		id INTEGER AUTO_INCREMENT,
		nombre VARCHAR(100),
		PRIMARY KEY(id)
	);

END//
delimiter ;

-- Ejercicio 3

SHOW CREATE PROCEDURE uno;

-- Ejercicio 4

SHOW PROCEDURE STATUS WHERE db='ajcm8.procedimientos1';

SELECT * FROM mysql.proc AS p WHERE p.type='procedure' AND p.db='ajcm8.procedimientos1';

-- Ejercicio 5

CALL uno();

SHOW TABLES FROM `ajcm8.procedimientos1`;

DESCRIBE ciudades;

EXPLAIN ciudades;

-- Ejercicio 6

DROP PROCEDURE if EXISTS dos;
delimiter //
CREATE PROCEDURE dos()
COMMENT 'ejemplo de procedimiento almacenado'
BEGIN 
	DROP TABLE if EXISTS personas;
	
	CREATE TABLE personas(
		id INTEGER AUTO_INCREMENT,
		nombre VARCHAR(100),
		edad INTEGER,
		PRIMARY KEY(id)
	);

END//
delimiter ;

-- Ejercicicio 7

CALL dos;

SHOW CREATE PROCEDURE dos;

-- Ejercicio 8

SELECT * FROM mysql.proc AS p WHERE p.type='procedure' AND p.db='ajcm8.procedimientos1';

-- Ejercicio 9

DROP FUNCTION if EXISTS f1;
delimiter //
CREATE FUNCTION f1()
RETURNS VARCHAR(50)
BEGIN
	RETURN CONCAT_WS(" ","hoy es",CURDATE(),"y son",CURTIME());
END//
delimiter ;

-- Ejercicio 10

SELECT f1();

-- Ejercicio 11

SHOW FUNCTION STATUS WHERE db='ajcm8.procedimientos1';

-- Ejercicio 12

SHOW CREATE FUNCTION f1;
