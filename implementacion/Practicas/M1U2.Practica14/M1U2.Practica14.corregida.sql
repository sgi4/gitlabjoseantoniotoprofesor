SET NAMES 'utf8';
USE `ajcm8.ciclistas`;

-- Ejercicio 1
SELECT DISTINCT *
FROM ciclista
LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal
WHERE etapa.dorsal IS NULL;
-- optimizada
-- c1
SELECT ciclista.dorsal FROM ciclista;

-- c2
SELECT DISTINCT etapa.dorsal FROM etapa;

--Restamos c1-c2  c1 EXCEPT c2
SELECT c1.dorsal
FROM (SELECT ciclista.dorsal FROM ciclista) AS c1
LEFT JOIN (SELECT etapa.dorsal FROM etapa) AS c2
ON c1.dorsal=c2.dorsal
WHERE c2.dorsal IS NULL;

-- sacamos nombre y edad de los cilistas

SELECT ciclista.nombre,ciclista.edad
FROM ciclista
WHERE ciclista.dorsal IN (SELECT c1.dorsal
									FROM (SELECT ciclista.dorsal FROM ciclista) AS c1
									LEFT JOIN (SELECT etapa.dorsal FROM etapa) AS c2
									ON c1.dorsal=c2.dorsal
									WHERE c2.dorsal IS NULL);

-- resta con un not in
                           SELECT c1.nombre,c1.edad
									FROM (SELECT * FROM ciclista) AS c1
									WHERE c1.dorsal NOT IN (SELECT etapa.dorsal FROM etapa);
									
									SELECT ciclista.nombre,ciclista.edad
									FROM ciclista
									WHERE ciclista.dorsal NOT IN (SELECT etapa.dorsal FROM etapa);
									
-- sacando con la anterior nobre y edad de los cilistas
SELECT ciclista.nombre,ciclista.edad
FROM ciclista
WHERE ciclista.dorsal IN ( SELECT c1.dorsal
									FROM (SELECT ciclista.dorsal FROM ciclista) AS c1
									WHERE c1.dorsal NOT IN (SELECT etapa.dorsal FROM etapa));

-- Ejercicio 2
SELECT *
FROM ciclista
LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal
WHERE puerto.dorsal IS NULL;

-- consulta optimizada en una de sus formas
-- c1
SELECT ciclista.dorsal FROM ciclista;
-- c2
SELECT puerto.dorsal FROM puerto;
-- c3
SELECT c1.dorsal
FROM(
     SELECT ciclista.dorsal FROM ciclista
     )AS c1
LEFT JOIN (
            SELECT puerto.dorsal FROM puerto
            ) AS c2
ON c1.dorsal=c2.dorsal
WHERE c2.dorsal IS NULL;

-- consulta optimizada utilizando la subconsulta anterior
SELECT ciclista.nombre,ciclista.edad
FROM ciclista
INNER JOIN (
              SELECT c1.dorsal
					FROM(
     						SELECT ciclista.dorsal FROM ciclista
     					)AS c1
					LEFT JOIN (
            					SELECT puerto.dorsal FROM puerto
           		 ) AS c2
					ON c1.dorsal=c2.dorsal
					WHERE c2.dorsal IS NULL
              ) AS c3 
ON ciclista.dorsal=c3.dorsal;

-- Ejercicio 3

SELECT DISTINCT equipo.director
FROM ciclista
LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal
JOIN equipo ON equipo.nomequipo=ciclista.nomequipo
WHERE (etapa.dorsal IS NULL);

--es mejor realizar la consulta anterior dividiendola en dos consultas
-- c1
SELECT ciclista .dorsal,equipo.director
FROM ciclista
JOIN equipo ON equipo.nomequipo=ciclista.nomequipo;

-- consulta del ejercicio 3 final

SELECT DISTINCT c1.director
FROM (
		SELECT ciclista.dorsal,equipo.director
		FROM ciclista
		JOIN equipo ON equipo.nomequipo=ciclista.nomequipo
      ) AS c1
LEFT JOIN etapa ON c1.dorsal=etapa.dorsal
WHERE etapa.dorsal IS NULL;

-- optimizacion del ejercicio 3 con la formula algebraica que no a dado el Profesor

-- c1
SELECT etapa.dorsal FROM etapa;
-- c2
SELECT ciclista.dorsal FROM ciclista;
-- c3= c2-c1 o c3= c2 except c1
SELECT c2.dorsal
FROM (
      SELECT ciclista.dorsal FROM ciclista
   ) AS c2
LEFT JOIN (
            SELECT etapa.dorsal FROM etapa
           ) AS c1
ON c1.dorsal=c2.dorsal
WHERE c1.dorsal IS NULL;

-- c4
SELECT distinct ciclista.nomequipo
FROM ciclista
INNER JOIN (
            SELECT c2.dorsal
				FROM (
      		SELECT ciclista.dorsal FROM ciclista
   					) AS c2
				LEFT JOIN (
            				SELECT etapa.dorsal FROM etapa
          				 ) AS c1
				ON c1.dorsal=c2.dorsal
				WHERE c1.dorsal IS NULL
				)AS c3
ON ciclista.dorsal=c3.dorsal;

-- c5
SELECT equipo.director
FROM equipo
JOIN (
					SELECT distinct ciclista.nomequipo
					FROM ciclista
					INNER JOIN (
            					SELECT c2.dorsal
									FROM (
      									SELECT ciclista.dorsal FROM ciclista
   										) AS c2
									LEFT JOIN (
            								SELECT etapa.dorsal FROM etapa
          				 						) AS c1
									ON c1.dorsal=c2.dorsal
									WHERE c1.dorsal IS NULL
									)AS c3
					ON ciclista.dorsal=c3.dorsal
         )AS c4
ON equipo.nomequipo=c4.nomequipo;
-- Ejercicio 4
SELECT DISTINCT ciclista.nombre, ciclista.dorsal
FROM ciclista
LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal
WHERE lleva.dorsal IS NULL;

-- esta consulta se puede optimizar tambien con subconsultas.

-- Ejercicio 5 corregido
-- esta solucion no es la correcta
SELECT distinct ciclista.nombre, ciclista.dorsal
FROM ciclista
LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal
WHERE lleva.`código` <>'MGE';
-- corregida
-- c1
SELECT distinct ciclista.nombre, ciclista.dorsal
FROM ciclista
JOIN lleva ON ciclista.dorsal=lleva.dorsal
WHERE lleva.`código` ='MGE';

SELECT DISTINCT ciclista.nombre,ciclista.dorsal
FROM ciclista
WHERE ciclista.dorsal NOT IN (
										SELECT DISTINCT ciclista.dorsal
										FROM ciclista
										JOIN lleva ON ciclista.dorsal=lleva.dorsal
										WHERE lleva.`código` ='MGE'	
								   );
-- se puede optimizar un poco mas la consulta anterior
-- Ejercicio 6

SELECT etapa.numetapa
FROM etapa
LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
WHERE puerto.numetapa IS NULL;
-- optimizada
-- c1
SELECT etapa.numetapa FROM etapa;
-- c2
SELECT puerto.numetapa FROM puerto;
-- consulta del ejercicio 6 optimizada o final
SELECT c1.numetapa
FROM 
   (
    SELECT etapa.numetapa FROM etapa
   ) AS c1
LEFT JOIN
   (
    SELECT puerto.numetapa FROM puerto
   ) AS c2
ON c1.numetapa=c2.numetapa
WHERE c2.numetapa IS NULL;
-- Ejercicio 7

SELECT AVG(etapa.kms)
FROM etapa
LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
WHERE puerto.numetapa IS NULL;
-- consulta final optimizada con un not in
SELECT etapa.numetapa
FROM etapa 
WHERE etapa.numetapa NOT IN (SELECT puerto.numetapa FROM puerto);

SELECT AVG(etapa.kms)
FROM etapa
WHERE etapa.numetapa IN (SELECT etapa.numetapa
									FROM etapa 
									WHERE etapa.numetapa NOT IN (SELECT puerto.numetapa FROM puerto));
-- Ejercicio 8

SELECT COUNT(ciclista.dorsal)
FROM ciclista
LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal
WHERE etapa.dorsal IS NULL;

-- Implementamos la resta de la consulta con un not exits (anti-semi-join)

SELECT COUNT(c1.dorsal)
FROM(
     SELECT ciclista.dorsal FROM ciclista
     ) AS c1
WHERE NOT EXISTS (SELECT TRUE FROM etapa WHERE c1.dorsal=etapa.dorsal);

-- Ejercicio 9

SELECT DISTINCT ciclista.nombre,ciclista.dorsal
FROM etapa
LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
JOIN ciclista ON etapa.dorsal = ciclista.dorsal
WHERE puerto.numetapa IS NULL;
-- la consulta realizada directamente esta claro que se debe optimizar para tablas o base de datos con muchos datos
-- para optimizar la consulta
-- restar etapas de puertos realizando de puertos proyeccios de las tablas por numetapa
-- de etapa proyectamos dorsal 

-- Ejercicio 10
SELECT DISTINCT ciclista.nombre,ciclista.dorsal
FROM etapa
LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
JOIN ciclista ON etapa.dorsal=ciclista.dorsal
WHERE puerto.numetapa IS NULL; -- esta mal

-- consulra del ejercicio 10 optimizada
-- c1
SELECT DISTINCT p.numetapa FROM puerto;
-- c2
SELECT * 
FROM etapa
LEFT JOIN (
				SELECT DISTINCT p.numetapa FROM puerto
				)AS c1
ON c1.numetapa=etapa.numetapa
WHERE c1.numetapa IS NULL;
-- c3
SELECT DISTINCT etapa.dorsal
FROM etapa;
-- c3-c2
SELECT *
FROM
    (
		SELECT DISTINCT etapa.dorsal FROM etapa
		) AS c3
LEFT JOIN (
				SELECT etapa.dorsal 
				FROM etapa
				LEFT JOIN (
				SELECT DISTINCT p.numetapa 
				FROM puerto
				)AS c1
				ON c1.numetapa=etapa.numetapa
				WHERE c1.numetapa IS NULL
				) AS c2
USING(dorsal)
WHERE c2.dorsal IS NULL;

-- consulta final

SELECT ciclista.dorsal,ciclista.nombre
FROM ciclista
INNER JOIN (
					SELECT c3.dorsal
					FROM
    						(
								SELECT DISTINCT etapa.dorsal FROM etapa
							) AS c3
					LEFT JOIN (
									SELECT etapa.dorsal 
									FROM etapa
									JOIN (
													SELECT DISTINCT puerto.numetapa 
													FROM puerto
													)AS c1
									ON c1.numetapa=etapa.numetapa
									) AS c2
					USING(dorsal)
					WHERE c2.dorsal IS NULL
				) AS Resta
USING(dorsal);