SET NAMES 'utf8';
USE `ajcm8.ciclistas`;

-- Ejercicio 1
SELECT *
FROM ciclista
LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal
WHERE etapa.dorsal IS NULL;

-- Ejercicio 2
SELECT *
FROM ciclista
LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal
WHERE puerto.dorsal IS NULL;

-- Ejercicio 3

SELECT DISTINCT equipo.director
FROM ciclista
LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal
JOIN equipo ON equipo.nomequipo=ciclista.nomequipo
WHERE (etapa.dorsal IS NULL);

-- Ejercicio 4
SELECT DISTINCT ciclista.nombre, ciclista.dorsal
FROM ciclista
LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal
WHERE lleva.dorsal IS NULL;

-- Ejercicio 5

SELECT distinct ciclista.nombre, ciclista.dorsal
FROM ciclista
LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal
WHERE lleva.`código` <> 'MGE';

-- Ejercicio 6

SELECT etapa.numetapa
FROM etapa
LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
WHERE puerto.numetapa IS NULL;

-- Ejercicio 7

SELECT etapa.numetapa, AVG(etapa.kms)
FROM etapa
LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
WHERE puerto.numetapa IS NULL;

-- Ejercicio 8

SELECT COUNT(ciclista.dorsal)
FROM ciclista
LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal
WHERE etapa.dorsal IS NULL;

-- Ejercicio 9

SELECT DISTINCT ciclista.nombre,ciclista.dorsal
FROM etapa
LEFT JOIN puerto ON etapa.numetapa=puerto.nompuerto
JOIN ciclista ON etapa.dorsal <> ciclista.dorsal
WHERE puerto.numetapa IS NULL;

-- Ejercicio 10
SELECT DISTINCT ciclista.nombre,ciclista.dorsal
FROM etapa
LEFT JOIN puerto ON etapa.numetapa=puerto.nompuerto
JOIN ciclista ON etapa.dorsal=ciclista.dorsal
WHERE puerto.numetapa IS NULL;
