SET NAMES 'utf8';
DROP DATABASE IF EXISTS `ajcm8.M1U2.Pract1`;
CREATE DATABASE IF NOT EXISTS `ajcm8.M1U2.Pract1`;
-- Seleccionamos la base de datos
USE `ajcm8.M1U2.Pract1`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract1`.`clientes`;
CREATE TABLE `ajcm8.M1U2.Pract1`.`clientes`(
	-- campos
	cod INTEGER AUTO_INCREMENT COMMENT 'Clave Principal', #campo con un comentario
	cliente VARCHAR(200),
	-- indices
	PRIMARY KEY(cod)
);

INSERT INTO `ajcm8.M1U2.Pract1`.`clientes`(cod,cliente)
	Values
	(1,'Luis Perez Galdos'),
	(2,'Rosa Maria Gomez'),
	(3,'Pedro Gomez Suarez');
	
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract1`.`coches`;
CREATE TABLE `ajcm8.M1U2.Pract1`.`coches`(
	-- campos
	id INTEGER AUTO_INCREMENT,
	`cod-Cliente` INTEGER,
	marca VARCHAR(100),
	fecha DATE,
	precio FLOAT,
	-- indices
	PRIMARY KEY(id),
	CONSTRAINT fkTblCochesTblClientes
		FOREIGN KEY (`cod-Cliente`) REFERENCES `ajcm8.M1U2.Pract1`.`clientes`(cod) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract1`.`coches`(id,`cod-Cliente`,marca,fecha,precio)
	Values
	(1,2,'Seat 600','1960/01/01',1000.99),
	(2,3,'ferrary k4000','2022/01/01',19999.99),
	(3,1,'Seat Supermiriaflori','1999/01/01',2999.99);
