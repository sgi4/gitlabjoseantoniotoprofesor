-- ------------------------------------------------------------------
# Snippet CrearTodo

# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `ajcm8.M1U2.Pract8`;
CREATE DATABASE IF NOT EXISTS `ajcm8.M1U2.Pract8`;
-- Seleccionamos la base de datos
USE `ajcm8.M1U2.Pract8`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`camionero`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`camionero`(
	-- campos
	dni VARCHAR(12),
	nombre VARCHAR(100),
	poblacion VARCHAR(50),
	tfno VARCHAR(12),
	direccion VARCHAR(100),
	salario float,
	-- indices
	PRIMARY KEY(dni)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`camionero`(dni,nombre,poblacion,tfno,direccion,salario)
	Values
	('dni-1','Raul Perez Lopez','Santander','942-010101','Rua Percebe, 12',1000.99),
	('dni-2','Luis Garcia Gutierrez','Santander','942-020202','Gran Via,6',800.44),
	('dni-3','Sonia Maria Marquez','Santander','942-030303','Las Ramblas',900.23);
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`camion`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`camion`(
	-- campos
	matricula VARCHAR(12),
	modelo VARCHAR(12),
	potencia INTEGER,
	tipo VARCHAR(12),
	-- indices
	PRIMARY KEY(matricula)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`camion`(matricula,modelo,potencia,tipo)
	Values
	('mat-1','modelo-1',800,'tipo-1'),
	('mat-2','modelo-2',900,'tipo-2'),
	('mat-3','modelo-3',500,'tipo-3');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`provincia`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`provincia`(
	-- campos
	codigo INTEGER AUTO_INCREMENT,
	nombre VARCHAR(50),
	-- indices
	PRIMARY KEY(codigo)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`provincia`(codigo,nombre)
	Values
	(1,'Cantabria'),
	(2,'Madrid'),
	(3,'Bilbao');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`conduce`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`conduce`(
	-- campos
	`dni-camionero` VARCHAR(12),
	`mat-camion` VARCHAR(12),
	-- indices
	PRIMARY KEY(`dni-camionero`,`mat-camion`),
	CONSTRAINT fkTblCoduceTblCamionero
		FOREIGN KEY (`dni-camionero`) REFERENCES `ajcm8.M1U2.Pract8`.`camionero`(dni) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkTblConduceTblCamion
		FOREIGN KEY (`mat-camion`) REFERENCES `ajcm8.M1U2.Pract8`.`camion`(matricula) ON DELETE RESTRICT ON UPDATE RESTRICT 

);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`conduce`(`dni-camionero`,`mat-camion`)
	Values
	('dni-1','mat-1'),
	('dni-1','mat-2'),
	('dni-2','mat-1');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`paquete`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`paquete`(
	-- campos
	codigo INTEGER AUTO_INCREMENT,
	descripcion VARCHAR(100),
	destinatario VARCHAR(100),
	direccion VARCHAR(100),
   `cod-provincia` INTEGER,
	-- indices
	PRIMARY KEY(codigo),
	CONSTRAINT fkTblPaqueteTblProvincia
		FOREIGN KEY (`cod-provincia`) REFERENCES `ajcm8.M1U2.Pract8`.`provincia`(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`paquete`(codigo,descripcion,destinatario,direccion,`cod-provincia`)
	Values
	(1,'paquete-1','destrio-1','direccion-1',3),
	(2,'paquete-2','destrio-2','direccion-2',3),
	(3,'paquete-3','destrio-3','direccion-3',3);
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`distribuye`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`distribuye`(
	-- campos
	`dni-camionero` VARCHAR(12),
	`cod-paquete` integer,
	-- indices
	PRIMARY KEY(`dni-camionero`,`cod-paquete`),
	UNIQUE KEY(`cod-paquete`),
	CONSTRAINT fkTblDistribuyeTblCamionero
		FOREIGN KEY (`dni-camionero`) REFERENCES `ajcm8.M1U2.Pract8`.`camionero`(dni) ON DELETE RESTRICT ON UPDATE RESTRICT, 
	CONSTRAINT fkTblDistribuyeTblPaquete
		FOREIGN KEY (`cod-paquete`) REFERENCES `ajcm8.M1U2.Pract8`.`paquete`(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT 
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`distribuye`(`dni-camionero`,`cod-paquete`)
	Values
	('dni-1',1),
	('dni-2',2),
	('dni-2',3);


