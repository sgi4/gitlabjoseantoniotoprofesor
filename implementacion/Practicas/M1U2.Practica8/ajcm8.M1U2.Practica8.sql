-- ------------------------------------------------------------------
# Snippet CrearTodo

# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `ajcm8.M1U2.Pract8`;
CREATE DATABASE IF NOT EXISTS `ajcm8.M1U2.Pract8`;
-- Seleccionamos la base de datos
USE `ajcm8.M1U2.Pract8`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`cliente`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`cliente`(
	-- campos
	dni VARCHAR(12),
	nombre VARCHAR(100),
	apellidos VARCHAR(100),
	fech_nac DATE,
	tfno VARCHAR(12),
	-- indices
	PRIMARY KEY(dni)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`cliente`(dni,nombre,apellidos,fech_nac,tfno)
	Values
	('dni-1','Raul','Perez Lopez','1971/01/01','942-030303'),
	('dni-2','Rosa','Garcia Juarez','1969/01/01','942-010101'),
	('dni-3','Sonia','Mendez Lopez','1973/01/01','942-040404');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`producto`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`producto`(
	-- campos
	codigo INTEGER AUTO_INCREMENT,
	nombre VARCHAR(100),
	precio float,
	-- indices
	PRIMARY KEY(codigo)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`producto`(codigo,nombre,precio)
	Values
	(1,'producto-1',30.45),
	(2,'producto-2',10.23),
	(3,'producto-3',23.23);
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`proveedor`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`proveedor`(
	-- campos
	nif VARCHAR(12),
	nombre VARCHAR(100),
	direccion VARCHAR(100),
	-- indices
	PRIMARY KEY(nif)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`proveedor`(nif,nombre,direccion)
	Values
	('dni-pro-1','Jose','calle lopez, 35'),
	('dni-pro-2','Rosario','Gran Via, 5'),
	('dni-pro-3','Isabel','General Mola, 35');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`compra`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`compra`(
	-- campos
	`dni-cliente` VARCHAR(12),
	`codigo-producto` INTEGER,
	-- indices
	PRIMARY KEY(`dni-cliente`,`codigo-producto`),
	CONSTRAINT fkTblCompraTblCliente
		FOREIGN KEY (`dni-cliente`) REFERENCES `ajcm8.M1U2.Pract8`.`cliente`(dni) ON DELETE RESTRICT ON UPDATE RESTRICT, 
	CONSTRAINT fkTblCompraTblProducto
		FOREIGN KEY (`codigo-producto`) REFERENCES `ajcm8.M1U2.Pract8`.`producto`(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT 

);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`compra`(`dni-cliente`,`codigo-producto`)
	Values
	('dni-1',1),
	('dni-2',2),
	('dni-1',2);
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract8`.`suministra`;
CREATE TABLE `ajcm8.M1U2.Pract8`.`suministra`(
	-- campos
	`codigo-producto` INTEGER,
	`nif-proveedor` VARCHAR(12),
	-- indices
	PRIMARY KEY(`codigo-producto`,`nif-proveedor`),
	CONSTRAINT fkTblSuministraTblProducto
		FOREIGN KEY (`codigo-producto`) REFERENCES `ajcm8.M1U2.Pract8`.`producto`(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT, 
	CONSTRAINT fkTblSuministraTblProveedor
		FOREIGN KEY (`nif-proveedor`) REFERENCES `ajcm8.M1U2.Pract8`.`proveedor`(nif) ON DELETE RESTRICT ON UPDATE RESTRICT 

);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract8`.`suministra`(`codigo-producto`,`nif-proveedor`)
	Values
	(1,'dni-pro-1'),
	(2,'dni-pro-1'),
	(1,'dni-pro-2');
