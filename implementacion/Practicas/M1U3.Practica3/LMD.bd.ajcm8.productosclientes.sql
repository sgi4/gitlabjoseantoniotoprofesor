SET NAMES 'utf8';
USE `ajcm8.productosclientes`;

-- Ejercicio 2

CREATE OR REPLACE VIEW ejercicio2 AS
	SELECT productos.`CÓDIGO ARTÍCULO`,productos.`SECCIÓN`,productos.PRECIO
	FROM productos
	WHERE productos.PRECIO>100;
	
SELECT * FROM ejercicio2;

-- Ejercicio 3

CREATE OR REPLACE VIEW ejercicio3 AS
	SELECT productos.`CÓDIGO ARTÍCULO`,productos.`SECCIÓN`,productos.PRECIO
	FROM productos
	WHERE productos.PRECIO>100
	ORDER BY productos.PRECIO ASC;
	
SELECT * FROM ejercicio3;

-- Ejercicio 4

CREATE OR REPLACE VIEW consulta2 AS
	SELECT ejercicio2.`CÓDIGO ARTÍCULO`,ejercicio2.`SECCIÓN`,ejercicio2.PRECIO
	FROM ejercicio2
	WHERE ejercicio2.`SECCIÓN`='deportes';
	
SELECT * FROM consulta2;

-- Ejercicio 5
ALTER TABLE productos
	CHANGE COLUMN importado importado BOOL NOT NULL DEFAULT FALSE;

INSERT INTO ejercicio2(ejercicio2.`CÓDIGO ARTÍCULO`,ejercicio2.`SECCIÓN`,ejercicio2.PRECIO) 
VALUES ('AR90','NOVEDADES',5);

-- Ejercicio 6

CREATE OR REPLACE VIEW ejercicio2 AS
	SELECT productos.`CÓDIGO ARTÍCULO`,productos.`SECCIÓN`,productos.PRECIO
	FROM productos
	WHERE productos.PRECIO>100
	WITH LOCAL CHECK OPTION;
	
SELECT * FROM ejercicio2;

-- Ejercicio 7 

ALTER TABLE productos
	CHANGE COLUMN importado importado BOOL NOT NULL DEFAULT FALSE;

INSERT INTO ejercicio2(ejercicio2.`CÓDIGO ARTÍCULO`,ejercicio2.`SECCIÓN`,ejercicio2.PRECIO) 
VALUES ('AR91','NOVEDADES',5);

-- No nos deja introducir el dato por que ahora la vista ejercicio2 contiene
-- una restriccion o check donde solo nos deja introductir productos con un
-- precio mayor que 100


-- Ejercicio 8

INSERT INTO consulta2(consulta2.`CÓDIGO ARTÍCULO`,consulta2.`SECCIÓN`,consulta2.PRECIO) 
VALUES ('AR92','NOVEDADES',5);

-- Ejercicio 9

CREATE OR REPLACE VIEW consulta2 AS
	SELECT ejercicio2.`CÓDIGO ARTÍCULO`,ejercicio2.`SECCIÓN`,ejercicio2.PRECIO
	FROM ejercicio2
	WHERE ejercicio2.`SECCIÓN`='deportes'
	WITH LOCAL CHECK OPTION;
	
INSERT INTO consulta2(consulta2.`CÓDIGO ARTÍCULO`,consulta2.`SECCIÓN`,consulta2.PRECIO) 
VALUES ('AR93','NOVEDADES',5);

REPLACE INTO consulta2(consulta2.`CÓDIGO ARTÍCULO`,consulta2.`SECCIÓN`,consulta2.PRECIO) VALUES('AR93','DEPORTES',5);

-- Ejercicio 10

CREATE OR REPLACE VIEW consulta2 AS
	SELECT ejercicio2.`CÓDIGO ARTÍCULO`,ejercicio2.`SECCIÓN`,ejercicio2.PRECIO
	FROM ejercicio2
	WHERE ejercicio2.`SECCIÓN`='deportes'
	WITH CASCADED CHECK OPTION;
	
-- Ejercicio 11
INSERT INTO consulta2(consulta2.`CÓDIGO ARTÍCULO`,consulta2.`SECCIÓN`,consulta2.PRECIO) 
VALUES ('AR94','DEPORTES',5);

-- Tenemos un check en productos con la restricion de no poder meter productos
-- con precios menores que 100 al estar el check en cascade este entra en accion
-- si insertamos los datos pero ahora con un precio mayor que 100, logramos
-- introductirlo en la tabla

INSERT INTO consulta2(consulta2.`CÓDIGO ARTÍCULO`,consulta2.`SECCIÓN`,consulta2.PRECIO) 
VALUES ('AR94','DEPORTES',200);
