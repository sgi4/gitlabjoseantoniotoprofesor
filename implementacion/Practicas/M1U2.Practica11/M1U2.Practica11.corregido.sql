SET NAMES 'utf8';
USE `ajcm8.ciclistas`;
-- _____________________________________________________________________________________
-- Ejercicio 1,1
SELECT distinct edad
FROM ciclista
WHERE nomequipo='Banesto';
-- _____________________________________________________________________________________
-- Ejercicio 1.2
SELECT distinct edad
FROM ciclista
WHERE nomequipo IN ('Banesto','Navigare');
-- _____________________________________________________________________________________
-- Ejercicio 1.3
SELECT DISTINCT dorsal
FROM ciclista
WHERE (nomequipo='Banesto')AND(edad BETWEEN 28 AND 32);
-- _____________________________________________________________________________________
-- Ejercicio 1.4
SELECT DISTINCT dorsal
FROM ciclista
WHERE (nomequipo='Banesto')OR(edad BETWEEN 28 AND 32);
-- _____________________________________________________________________________________
-- Ejercicio 1.5
SELECT distinct nomequipo
FROM equipo
WHERE nomequipo LIKE 'R%';-- like consume mucho tiempo procurar evitar

SELECT distinct nomequipo
FROM equipo
WHERE substring(nomequipo,1,1)='R';

SELECT distinct nomequipo
FROM equipo
WHERE LEFT(nomequipo,1)='R';

SELECT distinct nomequipo
FROM equipo
WHERE UPPER(LEFT(nomequipo,1))='R';

SELECT distinct nomequipo
FROM equipo
WHERE LOWER(LEFT(nomequipo,1))='r';
_____________________________________________________________________________________
-- Ejercicio 1.6
SELECT numetapa
FROM etapa
where salida=llegada;
-- _____________________________________________________________________________________
-- Ejercicio 1.7
SELECT numetapa,dorsal
FROM etapa
where salida<>llegada AND dorsal IS NOT null;
-- _____________________________________________________________________________________
-- Ejercicio 1.8
SELECT nompuerto
FROM puerto
WHERE (altura>2400)OR(altura BETWEEN 1000 AND 2000);
-- _____________________________________________________________________________________
-- Ejercicio 1.9
SELECT nompuerto,dorsal
FROM puerto
WHERE (altura>2400)OR(altura BETWEEN 1000 AND 2000);
-- _____________________________________________________________________________________
-- Ejericicio 1.10
SELECT COUNT(DISTINCT dorsal)
FROM etapa;
-- _____________________________________________________________________________________
-- Ejercicio 1.11
SELECT COUNT(DISTINCT numetapa)
FROM puerto;
-- _____________________________________________________________________________________
-- Ejercicio 1.12
SELECT COUNT(DISTINCT dorsal)
FROM puerto;
-- _____________________________________________________________________________________
-- Ejercicio 1.13
SELECT numetapa,COUNT(*)
FROM puerto
GROUP BY numetapa;
-- _____________________________________________________________________________________
-- Ejercicio 1.14
SELECT AVG(altura)
FROM puerto;
-- _____________________________________________________________________________________
-- Ejercicio 1.15
SELECT numetapa,AVG(altura)
FROM puerto
GROUP BY numetapa 
HAVING AVG(altura)>1500;
-- _____________________________________________________________________________________
-- Ejercicio 1.16
SELECT COUNT(*)
FROM (SELECT numetapa,AVG(altura)
FROM puerto
GROUP BY numetapa
HAVING AVG(altura)>1500 )AS C1;
-- _____________________________________________________________________________________
-- Ejercicio 1.17
SELECT dorsal,COUNT(*)
FROM lleva
GROUP BY dorsal;
-- _____________________________________________________________________________________
-- Ejercicio 1.18
SELECT dorsal,código,COUNT(*)
FROM lleva
GROUP BY dorsal,código;
-- _____________________________________________________________________________________
-- Ejercicio 1.19
SELECT numetapa,dorsal,COUNT(código)
FROM (SELECT numetapa,dorsal,código
FROM lleva
GROUP BY dorsal,código) AS c1
GROUP BY dorsal;