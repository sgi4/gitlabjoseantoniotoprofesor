SET NAMES 'utf8';
USE `ajcm8.compañias`;

-- Ejercicio 2
CREATE OR REPLACE VIEW ejercicio2 AS 
	SELECT COUNT(*) FROM ciudad;

SELECT * FROM ejercicio2;

-- Ejercicio 3
CREATE OR REPLACE VIEW ejercicio3 AS
	SELECT *,(SELECT AVG(ciudad.`población`)FROM ciudad)AS PobMedia FROM ciudad
	WHERE ciudad.`población`> (SELECT AVG(ciudad.`población`) FROM ciudad);
	
SELECT * FROM ejercicio3;

-- Ejercicio 4
CREATE OR REPLACE VIEW ejercicio4 AS
	SELECT *,(SELECT AVG(ciudad.`población`)FROM ciudad)AS PobMedia FROM ciudad
	WHERE ciudad.`población`< (SELECT AVG(ciudad.`población`) FROM ciudad);
	
SELECT * FROM ejercicio4;

-- Ejercicio 5
CREATE OR REPLACE VIEW ejercicio5 AS
	SELECT ciudad.nombre
	FROM ciudad
	WHERE ciudad.`población`=(Select MAX(ciudad.`población`)FROM ciudad);
	
SELECT * FROM ejercicio5;

-- Ejercicio 6
CREATE OR REPLACE VIEW ejercicio6 AS
	SELECT COUNT(*)FROM ciudad
	WHERE ciudad.`población`> (SELECT AVG(ciudad.`población`) FROM ciudad);
	
SELECT * FROM ejercicio6;

-- Ejercicio 7
CREATE OR REPLACE VIEW ejercicio7 AS
	SELECT persona.ciudad,COUNT(persona.nombre)FROM persona
	GROUP BY persona.ciudad;
	
SELECT * FROM ejercicio7;

-- Ejercicio 8
CREATE OR REPLACE VIEW ejercicio8 AS
	SELECT trabaja.`compañia`,COUNT(trabaja.persona)FROM trabaja
	GROUP BY trabaja.`compañia`;
	
SELECT * FROM ejercicio8;

-- Ejercicio 9
CREATE OR REPLACE VIEW ejercicio9 AS
   SELECT trabaja.`compañia` FROM trabaja
  	GROUP BY trabaja.`compañia`
   having count(trabaja.persona)=(SELECT MAX(c1.aa) FROM (SELECT COUNT(trabaja.persona)AS aa FROM trabaja
												GROUP BY trabaja.`compañia`)AS c1);
		
SELECT * FROM ejercicio9;

-- Ejercicio 10

CREATE OR REPLACE VIEW ejercicio10 AS
	SELECT persona.nombre,persona.calle,persona.ciudad
	FROM persona;
	
SELECT * FROM ejercicio10;

-- Ejercicio 11

CREATE OR REPLACE VIEW ejercicio11 AS
   SELECT c1.nombre,c1.calle,c1.ciudad,compañia.nombre AS ComNombre,compañia.ciudad AS ComCiudad
   FROM (
			SELECT persona.nombre,persona.calle,persona.ciudad,trabaja.`compañia`
			FROM persona
			INNER JOIN trabaja
			ON persona.nombre=trabaja.persona) AS c1
	INNER JOIN compañia
	ON compañia.nombre=c1.compañia;

SELECT * FROM ejercicio11;
