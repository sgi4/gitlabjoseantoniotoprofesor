SET NAMES 'utf8';

DROP DATABASE IF EXISTS `ajcm8.compañias`;

CREATE DATABASE IF NOT EXISTS `ajcm8.compañias`;

USE `ajcm8.compañias`;


--
-- Create table `ciudad`
--
CREATE TABLE IF NOT EXISTS ciudad (
  nombre varchar(30) NOT NULL,
  población int(11) DEFAULT NULL,
  PRIMARY KEY (nombre)
);
-- 
-- Dumping data for table ciudad
--
INSERT INTO ciudad VALUES
('AB', 70000),
('CR', 60000),
('CU', 50000),
('GU', 75000),
('TO', 80000);
--
-- Create table `persona`
--
CREATE TABLE IF NOT EXISTS persona (
  nombre varchar(30) NOT NULL,
  calle varchar(30) DEFAULT NULL,
  ciudad varchar(30) DEFAULT NULL,
  PRIMARY KEY (nombre),
  CONSTRAINT fktblpersonaTotblciudad FOREIGN KEY (ciudad)
  		REFERENCES ciudad(nombre) ON DELETE RESTRICT ON UPDATE RESTRICT
);
-- 
-- Dumping data for table persona
--
INSERT INTO persona VALUES
('Ana', 'Calle5', 'AB'),
('Antonio', 'Calle7', 'CU'),
('Blas', 'Calle6', 'AB'),
('Casimiro', 'Calle11', 'CR'),
('Eva', 'Calle2', 'CR'),
('Jose', 'Calle3', 'TO'),
('Juan', 'Calle1', 'CR'),
('Marga', 'Calle9', 'GU'),
('Maria', 'Calle4', 'TO'),
('Paco', 'Calle9', 'GU'),
('Wendy', 'Calle8', 'CU');
--
-- Create table `supervisa`
--
CREATE TABLE IF NOT EXISTS supervisa (
  supervisor varchar(30) NOT NULL,
  persona varchar(30) NOT NULL,
  PRIMARY KEY (supervisor, persona),
  CONSTRAINT fktblsupervisaTotblpersona FOREIGN KEY (supervisor)
  		REFERENCES persona(nombre) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT gktblsupervisaTotblpersonal FOREIGN KEY (persona)
  		REFERENCES persona(nombre) ON DELETE RESTRICT ON UPDATE RESTRICT
);
-- 
-- Dumping data for table supervisa
--
INSERT INTO supervisa VALUES
('Blas', 'Antonio'),
('Jose', 'Maria'),
('Marga', 'Blas'),
('Marga', 'Paco'),
('Maria', 'Ana'),
('Wendy', 'Eva'),
('Wendy', 'Juan');
--
-- Create table `compañia`
--
CREATE TABLE IF NOT EXISTS compañia (
  nombre varchar(30) NOT NULL,
  ciudad varchar(30) DEFAULT NULL,
  PRIMARY KEY (nombre),
  CONSTRAINT fktblcompañiaTotblciudad FOREIGN KEY (ciudad)
  		REFERENCES ciudad(nombre) ON DELETE RESTRICT ON UPDATE RESTRICT
);
-- 
-- Dumping data for table compañia
--
INSERT INTO compañia VALUES
('FAGOR', 'TO'),
('INDRA', 'CR'),
('NESTLE', 'AB');
--
-- Create table `trabaja`
--
CREATE TABLE IF NOT EXISTS trabaja (
  persona varchar(30) NOT NULL,
  compañia varchar(30) DEFAULT NULL,
  salario int(11) DEFAULT NULL,
  PRIMARY KEY (persona),
  CONSTRAINT fktbltrabajaTotblcompañia FOREIGN KEY (`compañia`)
		REFERENCES `compañia`(nombre)ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT fktbltrabajaTotblpersona FOREIGN KEY (persona)
  		REFERENCES persona(nombre) ON DELETE RESTRICT ON UPDATE RESTRICT
);
-- 
-- Dumping data for table trabaja
--
INSERT INTO trabaja VALUES
('Ana', 'FAGOR', 18000),
('Antonio', 'NESTLE', 15000),
('Blas', 'NESTLE', 25000),
('Eva', 'INDRA', 30000),
('Jose', 'FAGOR', 50000),
('Juan', 'INDRA', 40000),
('Marga', 'NESTLE', 50000),
('Maria', 'FAGOR', 40000),
('Paco', 'NESTLE', 25000),
('Wendy', 'INDRA', 50000);
