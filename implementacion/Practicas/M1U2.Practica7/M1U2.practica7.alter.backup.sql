-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.30-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para ajcm8.m1u2.pract7
DROP DATABASE IF EXISTS `ajcm8.m1u2.pract7`;
CREATE DATABASE IF NOT EXISTS `ajcm8.m1u2.pract7` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ajcm8.m1u2.pract7`;

-- Volcando estructura para tabla ajcm8.m1u2.pract7.alumno
DROP TABLE IF EXISTS `alumno`;
CREATE TABLE IF NOT EXISTS `alumno` (
  `mat` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `grupo` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`mat`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Alumnos de la academia';

-- Volcando datos para la tabla ajcm8.m1u2.pract7.alumno: ~3 rows (aproximadamente)
DELETE FROM `alumno`;
INSERT INTO `alumno` (`mat`, `nombre`, `grupo`) VALUES
	(1, 'Juan Perez Galdos', 'grupo a'),
	(2, 'Ana Maria Golpes Mendez', 'grupo b'),
	(3, 'Rosa Garcia Garcia', 'grupo c');

-- Volcando estructura para tabla ajcm8.m1u2.pract7.fechap
DROP TABLE IF EXISTS `fechap`;
CREATE TABLE IF NOT EXISTS `fechap` (
  `mat-alumno` int(11) NOT NULL,
  `p#practica` int(11) NOT NULL,
  `nota` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`mat-alumno`,`p#practica`),
  CONSTRAINT `fkTblFechapTblRealiza` FOREIGN KEY (`mat-alumno`, `p#practica`) REFERENCES `realiza` (`mat-alumno`, `p#practica`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Las notas y las fechas de cada una de las practicas entregadas por los alumnos';

-- Volcando datos para la tabla ajcm8.m1u2.pract7.fechap: ~3 rows (aproximadamente)
DELETE FROM `fechap`;
INSERT INTO `fechap` (`mat-alumno`, `p#practica`, `nota`, `fecha`) VALUES
	(1, 1, 8, '2022-01-01'),
	(1, 2, 5, '2022-07-01'),
	(2, 3, 5, '2022-03-03');

-- Volcando estructura para tabla ajcm8.m1u2.pract7.practica
DROP TABLE IF EXISTS `practica`;
CREATE TABLE IF NOT EXISTS `practica` (
  `p#` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `dificultad` int(11) DEFAULT NULL,
  PRIMARY KEY (`p#`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Datos sobre las practicas preparadas';

-- Volcando datos para la tabla ajcm8.m1u2.pract7.practica: ~3 rows (aproximadamente)
DELETE FROM `practica`;
INSERT INTO `practica` (`p#`, `titulo`, `dificultad`) VALUES
	(1, 'Practica Administracion', 5),
	(2, 'Practica Informatica', 5),
	(3, 'Practica Mantenimiento', 5);

-- Volcando estructura para tabla ajcm8.m1u2.pract7.realiza
DROP TABLE IF EXISTS `realiza`;
CREATE TABLE IF NOT EXISTS `realiza` (
  `mat-alumno` int(11) NOT NULL,
  `p#practica` int(11) NOT NULL,
  PRIMARY KEY (`mat-alumno`,`p#practica`),
  KEY `fkTblRealizaTblPractica` (`p#practica`),
  CONSTRAINT `fkTblRealizaTblAlumno` FOREIGN KEY (`mat-alumno`) REFERENCES `alumno` (`mat`),
  CONSTRAINT `fkTblRealizaTblPractica` FOREIGN KEY (`p#practica`) REFERENCES `practica` (`p#`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Practicas realizadas por cada alumno';

-- Volcando datos para la tabla ajcm8.m1u2.pract7.realiza: ~3 rows (aproximadamente)
DELETE FROM `realiza`;
INSERT INTO `realiza` (`mat-alumno`, `p#practica`) VALUES
	(1, 1),
	(1, 2),
	(2, 3);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
