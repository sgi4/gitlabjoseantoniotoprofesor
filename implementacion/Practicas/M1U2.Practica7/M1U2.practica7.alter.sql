# Borramos la base de datos if existe;
DROP DATABASE IF EXISTS `ajcm8.M1U2.Pract7`;
CREATE DATABASE IF NOT EXISTS `ajcm8.M1U2.Pract7`;
-- Seleccionamos la base de datos
USE `ajcm8.M1U2.Pract7`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract7`.`alumno`;
CREATE TABLE `ajcm8.M1U2.Pract7`.`alumno`(
	-- campos
	mat INTEGER AUTO_INCREMENT,
	nombre VARCHAR(100),
	grupo VARCHAR(10),
	-- indices
	PRIMARY KEY(mat)
) COMMENT 'Alumnos de la academia';
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract7`.`alumno`(mat,nombre,grupo)
	Values
	(1,'Juan Perez Galdos','grupo a'),
	(2,'Ana Maria Golpes Mendez','grupo b'),
	(3,'Rosa Garcia Garcia','grupo c');
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract7`.`practica`;
CREATE TABLE `ajcm8.M1U2.Pract7`.`practica`(
	-- campos
	`p#` INTEGER AUTO_INCREMENT,
	titulo VARCHAR(100),
	dificultad Integer,
	-- indices
	PRIMARY KEY(`p#`)
) COMMENT 'Datos sobre las practicas preparadas';
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract7`.`practica`(`p#`,titulo,dificultad)
	Values
	(1,'Practica Administracion',5),
	(2,'Practica Informatica',5),
	(3,'Practica Mantenimiento',5);
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract7`.`realiza`;
CREATE TABLE `ajcm8.M1U2.Pract7`.`realiza`(
	-- campos
	`mat-alumno` INTEGER,
	`p#practica` INTEGER,
	-- indices
	PRIMARY KEY(`mat-alumno`,`p#practica`)
) COMMENT 'Practicas realizadas por cada alumno';
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract7`.`realiza`(`mat-alumno`,`p#practica`)
	Values
	(1,1),
	(1,2),
	(2,3);
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract7`.`fechap`;
CREATE TABLE `ajcm8.M1U2.Pract7`.`fechap`(
	-- campos
	`mat-alumno` INTEGER,
	`p#practica` INTEGER,
	nota INTEGER,
	fecha DATE,
	-- indices
	PRIMARY KEY(`mat-alumno`,`p#practica`)
) COMMENT 'Las notas y las fechas de cada una de las practicas entregadas por los alumnos';
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract7`.`fechap`(`mat-alumno`,`p#practica`,nota,fecha)
	Values
	(1,1,8,'2022/01/01'),
	(1,2,5,'2022/07/01'),
	(2,3,5,'2022/03/03');
	
ALTER TABLE realiza
  ADD	CONSTRAINT fkTblRealizaTblAlumno
		FOREIGN KEY (`mat-alumno`) REFERENCES `ajcm8.M1U2.Pract7`.`alumno`(mat) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkTblRealizaTblPractica
		FOREIGN KEY (`p#practica`) REFERENCES `ajcm8.M1U2.Pract7`.`practica`(`p#`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE fechap
  ADD CONSTRAINT fkTblFechapTblRealiza
		FOREIGN KEY(`mat-alumno`,`p#practica`)REFERENCES `ajcm8.M1U2.Pract7`.`realiza`(`mat-alumno`,`p#practica`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
