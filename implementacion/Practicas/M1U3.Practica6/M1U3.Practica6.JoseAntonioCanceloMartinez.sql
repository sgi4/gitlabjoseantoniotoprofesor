DROP DATABASE if EXISTS `ajcm8.m1u3.practica6`;
CREATE DATABASE `ajcm8.m1u3.practica6`;
USE `ajcm8.m1u3.practica6`;

-- Ejercicio 1

/* creamos un procedimiento el cual crea una tabla con los campos nombre,edad, fecha_nacimiento
*/
DROP PROCEDURE if EXISTS f1;
delimiter //
CREATE PROCEDURE f1()
BEGIN
	CREATE OR REPLACE  TABLE TABLE1(
		nombre VARCHAR(10) DEFAULT NULL,
		edad INTEGER DEFAULT 0,
		fecha_Nacimiento DATE DEFAULT NULL,
		PRIMARY KEY(nombre)
	);
END//
delimiter ;

CALL f1();

-- ejercicio 2

DROP function if EXISTS f2;
delimiter //
CREATE FUNCTION f2()
RETURNS boolean
BEGIN
	CREATE OR REPLACE  TABLE TABLE1(
		nombre VARCHAR(10) DEFAULT NULL,
		edad INTEGER DEFAULT 0,
		fecha_Nacimiento DATE DEFAULT NULL,
		PRIMARY KEY(nombre)
	);
	RETURN 1;
END//
delimiter ;

-- la función no puede crear la tabla

-- Ejercicio 3

DROP PROCEDURE if EXISTS crear_tabla;
delimiter //
CREATE PROCEDURE crear_tabla()
BEGIN
	CREATE OR REPLACE  TABLE personas(
		nombre VARCHAR(10) DEFAULT NULL,
		dni VARCHAR(30),
		edad INTEGER DEFAULT 0,
		apellidos VARCHAR(50),
		fecha_Nacimiento DATE DEFAULT NULL,
		PRIMARY KEY(nombre)
	);
END//
delimiter ;

CALL crear_tabla();

-- el procedimiento crea la tabla personas con los campos pedidos

-- Ejercicio 4

DROP PROCEDURE if EXISTS crear_tabla;
delimiter //
CREATE PROCEDURE crear_tabla(IN dime integer)
BEGIN
	if(dime=1) then
		CREATE OR REPLACE  TABLE personas(
			nombre VARCHAR(10) DEFAULT NULL,
			dni VARCHAR(30),
			edad INTEGER DEFAULT 0,
			apellidos VARCHAR(50),
			fecha_Nacimiento DATE DEFAULT NULL,
			PRIMARY KEY(nombre)
		);
	else
		CREATE OR REPLACE  TABLE TABLE1(
			nombre VARCHAR(10) DEFAULT NULL,
			edad INTEGER DEFAULT 0,
			fecha_Nacimiento DATE DEFAULT NULL,
			PRIMARY KEY(nombre)
		);
	END if;	
	if ((dime<0)OR(dime>1)) then SELECT 'El paramatro introducido unicamento toma los valores 0,1';END if;
END//
delimiter ;

CALL crear_tabla(1);
CALL crear_tabla(0);
CALL crear_tabla(20);
CALL crear_tabla(-1);

-- ejercicio 5

/*
	el procedimiento cuenta los registro de la tabla1 si nohay registros indica que la tabla esta vacia
	e inserta un registro con los valores por defecto, si la tabla tiene un numero de registros entre 10 y 100
	muestra los registros hasta el 100 sino limita la salida de registros a los 100 primeros.
*/

DROP PROCEDURE if EXISTS f3;
delimiter //
CREATE PROCEDURE f3()
BEGIN
	DECLARE v1 INTEGER;
	SET v1=(SELECT COUNT(*) FROM table1);
	if(v1=0) then
		SELECT 'La tabla esta vacia';
		INSERT INTO table1 VALUES (DEFAULT,DEFAULT,DEFAULT);
	ELSEIF (v1 BETWEEN 10 AND 100)  then
		SELECT * FROM table1;
	else
		SELECT * FROM table1 LIMIT 1,100;
	END if;
END//
delimiter ;

CALL f3();

-- ejercicio 6

DROP function if EXISTS f4;
delimiter //
CREATE function f4()
RETURNS boolean
BEGIN
	DECLARE v1 INTEGER;
	SET v1=(SELECT COUNT(*) FROM table1);
	if(v1=0) then
		SELECT 'La tabla esta vacia';
		INSERT INTO table1 VALUES (DEFAULT,DEFAULT,DEFAULT);
	ELSEIF (v1 BETWEEN 10 AND 100)  then
		SELECT * FROM table1;
	else
		SELECT * FROM table1 LIMIT 1,100;
	END if;
	RETURN 1;
END//
delimiter ;

SELECT f4();

-- que no podemos mostar valores intermedios con un select

DROP function if EXISTS f4;
delimiter //
CREATE function f4()
RETURNS integer
BEGIN
	DECLARE v1 INTEGER;
	SET v1=(SELECT COUNT(*) FROM table1);
	RETURN v1;
END//
delimiter ;

SELECT f4();

-- no hemos tenido problemas

-- ejercicio 7

DROP PROCEDURE if EXISTS crear_claves;
delimiter //
CREATE PROCEDURE crear_claves()
BEGIN
	CREATE OR REPLACE  TABLE personas(
		nombre VARCHAR(10) DEFAULT NULL,
		dni VARCHAR(30),
		edad INTEGER DEFAULT 0,
		apellidos VARCHAR(50),
		fecha_Nacimiento DATE DEFAULT NULL,
		PRIMARY KEY(dni),
		UNIQUE KEY(nombre),
		KEY(fecha_Nacimiento)
	);
	INSERT INTO personas(nombre,dni,apellidos,fecha_Nacimiento)VALUES
	('aa','1','ab','1971/12/12'),
	('ac','2','ad','1965/3/3');
END//
delimiter ;

CALL crear_claves();

-- Ejercicio 8

DROP PROCEDURE if EXISTS crear_edad;
delimiter //
CREATE PROCEDURE crear_edad()
BEGIN
	DECLARE edad INTEGER DEFAULT 0;
	DECLARE fecha DATE;
	DECLARE dni VARCHAR(30);
	SELECT dni,fecha_Nacimiento INTO dni,fecha FROM personas LIMIT 1;
	SET edad=Abs(TIMESTAMPDIFF(YEAR, NOW(), fecha));
	SELECT edad,dni;
	UPDATE personas
	 	SET personas.edad=edad
		 WHERE personas.dni=dni;
END//
delimiter ;

CALL crear_claves();
CALL crear_edad();

-- Ejercicio 9

CREATE OR REPLACE VIEW vista1 AS
	SELECT nombre,apellidos,edad 
	FROM personas
	WHERE edad<18;
	
-- Ejercicio 10

CREATE OR REPLACE TABLE menores AS 	SELECT nombre,apellidos,edad 
	FROM personas
	WHERE edad<18;
	
CREATE OR REPLACE TABLE no_menores AS 	SELECT nombre,apellidos,edad 
	FROM personas
	WHERE edad>=18;
	
-- Ejercicio 11

CREATE OR REPLACE VIEW nombre_completo AS 
	SELECT nombre,apellidos
	FROM personas;
-- Ejercicio 12


DROP PROCEDURE if EXISTS alfabeto1;
delimiter //
CREATE PROCEDURE alfabeto1()
BEGIN
	DECLARE contador INTEGER DEFAULT 0;
	CREATE OR REPLACE TABLE alfabeto(
		id INTEGER AUTO_INCREMENT,
		alfa VARCHAR(1),
		PRIMARY KEY(id)
	);
	 while(contador <255)do
	 		set contador=contador+1;
	INSERT INTO alfabeto(id,alfa)VALUES(default,CHR(contador));

	END while;
	SELECT * FROM alfabeto;
END//
delimiter ;

CALL alfabeto1();

DROP PROCEDURE if EXISTS numeros1;
delimiter //
CREATE PROCEDURE numeros1()
BEGIN
	DECLARE contador INTEGER DEFAULT 0;
	CREATE OR REPLACE TABLE numeros(
		id INTEGER AUTO_INCREMENT,
		num VARCHAR(10),
		PRIMARY KEY(id)
	);
	 while(contador <1000)do
	 		set contador=contador+1;
	INSERT INTO numeros(id,num)VALUES(default,contador);

	END while;
	SELECT * FROM numeros;
END//
delimiter ;

CALL numeros1();