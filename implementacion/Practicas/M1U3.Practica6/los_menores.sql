DROP DATABASE if EXISTS `ajcm8.m1u3.practica6`;
CREATE DATABASE `ajcm8.m1u3.practica6`;
USE `ajcm8.m1u3.practica6`;

-- ejercicio 10

-- Ejercicio 9

CREATE OR REPLACE VIEW vista1 AS
	SELECT nombre,apellidos,edad 
	FROM personas
	WHERE edad<18;
	
-- Ejercicio 10

CREATE OR REPLACE TABLE menores AS 	SELECT nombre,apellidos,edad 
	FROM personas
	WHERE edad<18;
	
CREATE OR REPLACE TABLE no_menores AS 	SELECT nombre,apellidos,edad 
	FROM personas
	WHERE edad>=18;