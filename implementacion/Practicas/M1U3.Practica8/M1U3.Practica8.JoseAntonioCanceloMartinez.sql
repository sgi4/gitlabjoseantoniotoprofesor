-- Modulo-1 Unidad-3 Practica 8

-- Ejercicio 1

DROP DATABASE if EXISTS `ajcm8.m1u3.practica8`;
CREATE DATABASE `ajcm8.m1u3.practica8`;
USE `ajcm8.m1u3.practica8`;

DROP PROCEDURE if EXISTS p1;
delimiter //
CREATE PROCEDURE p1(IN num1 INTEGER,IN num2 INTEGER)
BEGIN
-- a.-
	if(num1>num2)then
		SELECT 'numero1>numero2';
	ELSEIF(num2>num1)then
		SELECT 'numero2>numero1';
	else
		SELECT 'los numero son iguales';
	END if;
-- b.-	
   CREATE OR REPLACE TABLE mayor(
		id INTEGER AUTO_INCREMENT,
		num INTEGER,
		PRIMARY KEY(id)
	);
	INSERT INTO mayor(id,num)values
	(1,num1),
	(2,num2);
	SELECT MAX(num)FROM mayor;
-- c.-
	SELECT GREATEST(num1,num2);

END//
delimiter ;

CALL p1(3,4);
CALL p1(5,2);
CALL p1(1,1);

-- Ejercicio 2

DROP PROCEDURE if EXISTS p2;
delimiter //
CREATE PROCEDURE p2(IN num1 INTEGER,IN num2 INTEGER,IN num3 integer)
BEGIN
-- a.-
	if((num1>num2)and(num1>num3))then
		SELECT 'numero1 mayor';
	ELSEIF((num2>num1)and(num2>num3))then
		SELECT 'numero2 mayor';
	elseif((num3>num2)and(num3>num1))then
		SELECT 'numero3 mayor';
	ELSEIF((num1=num2)AND(num1=num3))then
		SELECT 'los numero son iguales';
	END if;
-- b.-	
   CREATE OR REPLACE TABLE mayor(
		id INTEGER AUTO_INCREMENT,
		num INTEGER,
		PRIMARY KEY(id)
	);
	INSERT INTO mayor(id,num)values
	(1,num1),
	(2,num2),
	(3,num3);
	SELECT MAX(num)FROM mayor;
-- c.-
	SELECT GREATEST(num1,num2,num3);

END//
delimiter ;

CALL p2(3,4,7);
CALL p2(5,7,2);
CALL p2(7,6,2);
CALL p2(1,1,1);

-- Ejercicio 3

DROP PROCEDURE if EXISTS p3;
delimiter //
CREATE PROCEDURE p3(IN num1 INTEGER,IN num2 INTEGER,IN num3 INTEGER,OUT minimo INTEGER,OUT mayor integer)
BEGIN

	SELECT GREATEST(num1,num2,num3)INTO mayor;
	SELECT LEAST(num1,num2,num3)INTO minimo;

END//
delimiter ;
SET @mayor=0;
SET @minimo=0;

CALL p3(3,4,7,@minimo,@mayor);SELECT @minimo,@mayor;
CALL p3(5,7,2,@minimo,@mayor);SELECT @minimo,@mayor;
CALL p3(7,6,2,@minimo,@mayor);SELECT @minimo,@mayor;
CALL p3(1,1,1,@minimo,@mayor);SELECT @minimo,@mayor;

-- Ejercicio 4

DROP PROCEDURE if EXISTS p4;
delimiter //
CREATE PROCEDURE p4(IN fecha1 DATE,IN fecha2 DATE)
BEGIN
	SELECT abs(TO_DAYS(fecha1)-TO_DAYS(fecha2));
END//
delimiter ;

CALL p4('2022/2/2','2022/2/1');

-- Ejercicio 5

DROP PROCEDURE if EXISTS p5;
delimiter //
CREATE PROCEDURE p5(IN fecha1 DATE,IN fecha2 DATE)
BEGIN
	SELECT abs(TIMESTAMPDIFF(MONTH,'2022/2/1','2022/1/1'));
END//
delimiter ;

CALL p5('2022/2/2','2022/2/1');

-- Ejercicio 6

DROP PROCEDURE if EXISTS p6;
delimiter //
CREATE PROCEDURE p6(IN fecha1 DATE,IN fecha2 DATE,OUT dias INTEGER,OUT meses INTEGER,OUT anio integer)
BEGIN
	SELECT abs(TIMESTAMPDIFF(DAY,'2022/2/1','2022/1/1'))INTO dias;
	SELECT abs(TIMESTAMPDIFF(MONTH,'2022/2/1','2022/1/1'))INTO meses;
	SELECT abs(TIMESTAMPDIFF(YEAR,'2022/2/1','2022/1/1'))INTO anio;
END//
delimiter ;

SET @dias=0;
SET @meses=0;
SET @anio=0;

CALL p6('2022/2/2','2022/2/1',@dias,@meses,@anio);

SELECT @dias,@meses,@anio;

-- Ejercicio 7 

DROP PROCEDURE if EXISTS p7;
delimiter //
CREATE PROCEDURE p7(IN frase VARCHAR(20))
BEGIN
	SELECT 'numero de caracteres de la frase: ',LENGTH(frase);
END//
delimiter ;

CALL p7('hola');