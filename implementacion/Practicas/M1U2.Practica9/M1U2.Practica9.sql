USE `ajcm8.pract9.ciclistas`;
_____________________________________________________________________________________
-- Ejercicio 1,1
SELECT DISTINCT c.edad FROM ciclista AS c;
_____________________________________________________________________________________
-- Ejercicio 1.2
SELECT DISTINCT c.edad FROM ciclista AS c WHERE c.nomequipo='Artiach';
_____________________________________________________________________________________
-- Ejercicio 1.3
SELECT DISTINCT e.edad FROM ciclista AS c WHERE (c.nomequipo='Artiach') OR (c.nomequipo='Amore Vita';
_____________________________________________________________________________________
-- Ejercicio 1.4
SELECT c.dorsal FROM ciclista AS c WHERE (c.edad<25) OR (e.edad>30);
_____________________________________________________________________________________
-- Ejercicio 1.5
SELECT c.dorsal FROM ciclista AS c WHERE (c.edad BETWEEN 28 AND 32) AND (c.nomequipo='Banesto');
_____________________________________________________________________________________
-- Ejercicio 1.6
SELECT c.nombre FROM ciclista AS c WHERE CHAR_LENGHT(c.nombre)>8;
_____________________________________________________________________________________
-- Ejercicio 1.7
SELECT c.nombre, c.dorsal, UPPER(c.nombre) AS `nombre Mayusculas` FROM ciclista AS c;
_____________________________________________________________________________________
-- Ejercicio 1.8
SELECT DISTINCT l.dorsal FROM lleva AS l WHERE l.codigo='MGE';
_____________________________________________________________________________________
-- Ejercicio 1.9
SELECT p.nompuerto FROM puerto AS p WHERE p.altura > 1500;
_____________________________________________________________________________________
-- Ejericicio 1.10
SELECT DISTINCT p.dorsal FROM puerto AS p WHERE (p.altura BETWEEN 1800 AND 3000) OR (p.pendiente > 8);
_____________________________________________________________________________________
-- Ejercicio 1.11
SELECT DISTINCT p.dorsal FROM puerto AS p WHERE (p.altura BETWEEN 1800 AND 3000) AND (p.pendiente >8);
_____________________________________________________________________________________
