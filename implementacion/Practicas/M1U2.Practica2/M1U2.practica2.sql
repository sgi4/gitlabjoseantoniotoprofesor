DROP DATABASE IF EXISTS `ajcm8.M1U2.Pract2`;
CREATE DATABASE IF NOT EXISTS `ajcm8.M1U2.Pract2`;
-- Seleccionamos la base de datos
USE `ajcm8.M1U2.Pract2`;
-- ------------------------------------------------------------------
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract2`.`clientes`;
CREATE TABLE `ajcm8.M1U2.Pract2`.`clientes`(
	-- campos
	cod INTEGER AUTO_INCREMENT COMMENT 'Clave Principal', #campo con un comentario
	nombre VARCHAR(200),
	-- indices
	PRIMARY KEY(cod)
);

INSERT INTO `ajcm8.M1U2.Pract2`.`clientes`(cod,nombre)
	Values
	(1,'Luis Perez Galdos'),
	(2,'Rosa Maria Gomez'),
	(3,'Pedro Gomez Suarez');
	
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract2`.`coches`;
CREATE TABLE `ajcm8.M1U2.Pract2`.`coches`(
	-- campos
	id INTEGER AUTO_INCREMENT,
	marca VARCHAR(100),
	fecha DATE,
	precio FLOAT,
	-- indices
	PRIMARY KEY(id)
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract2`.`coches`(id,marca,fecha,precio)
	Values
	(1,'Seat 600','1960/01/01',1000.99),
	(2,'ferrary k4000','2022/01/01',19999.99),
	(3,'Seat Supermiriaflori','1999/01/01',2999.99);
	
# Snippet CreateTable
DROP TABLE if EXISTS `ajcm8.M1U2.Pract2`.`comprados`;
CREATE TABLE `ajcm8.M1U2.Pract2`.`comprados`(
	-- campos
	idCoches INTEGER,
	codCliente INTEGER,
	-- indices
	PRIMARY KEY(idCoches,codCliente),
	UNIQUE KEY(idCoches),
	CONSTRAINT fkTblCompradosTblCoches
			FOREIGN KEY (idCoches) REFERENCES `ajcm8.M1U2.Pract2`.`coches`(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT fkTblCompradosTblClientes
			FOREIGN KEY (codCliente) REFERENCES `ajcm8.M1U2.Pract2`.`clientes`(cod) ON DELETE RESTRICT ON UPDATE RESTRICT 
		
);
-- ------------------------------------------------------------------
# Snippet InsertarDatos
INSERT INTO `ajcm8.M1U2.Pract2`.`comprados`(idCoches,codCliente)
	Values
	(2,2),
	(3,1),
	(1,3);