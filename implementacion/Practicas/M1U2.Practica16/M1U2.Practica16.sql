SET NAMES 'utf8';
USE `ajcm8.comercios`;

-- Ejercicio 1.1
SELECT cliente.dni
FROM cliente;

-- Ejercicio 1.2
SELECT *
FROM programa;

-- Ejercicio 1.3
SELECT programa.nombre
FROM programa;

-- Ejercicio 1.4
SELECT DISTINCT comercio.ciudad
FROM comercio;

-- Ejercicio 1.5
SELECT DISTINCT programa.nombre
FROM programa;

-- Ejercicio 1.6
SELECT fabricante.nombre
FROM fabricante
WHERE fabricante.pais="Estados Unidos";

-- Ejercicio 1.7
SELECT fabricante.nombre
FROM fabricante
WHERE fabricante.pais NOT IN ("España");

-- Ejercicio 1.8
SELECT comercio.nombre
FROM comercio
WHERE comercio.nombre NOT IN ("El Corte Inglés");

-- Ejercicio 1.9
SELECT programa.codigo
FROM programa
WHERE programa.nombre IN ("Windows","Access");

-- Ejercicio 1.10
SELECT cliente.nombre
FROM cliente
WHERE cliente.edad BETWEEN 10 AND 25 or cliente.edad > 50;

-- Ejercicio 1.11
SELECT cliente.nombre
FROM cliente
WHERE cliente.nombre LIKE "%o";

SELECT cliente.nombre
FROM cliente
WHERE RIGHT(LOWER(cliente.nombre),1)="o";

-- Ejercicio 1.12
SELECT cliente.nombre
FROM cliente
WHERE RIGHT(LOWER(cliente.nombre),1)="o" AND cliente.edad>30;

-- Ejercicio 1.13
SELECT programa.nombre
FROM programa
WHERE RIGHT(programa.`version`,"i") OR LEFT(LOWER(programa.nombre),1)="A" OR LEFT(LOWER(programa.nombre),1)="W";

-- Ejercicio 1.14
SELECT fabricante.nombre
FROM fabricante
ORDER BY fabricante.nombre DESC;

-- Ejercicio 1.15
--c1 
SELECT programa.codigo
FROM programa
WHERE programa.nombre="windows";
-- c2
SELECT distribuye.cif
FROM distribuye
INNER JOIN (
				SELECT programa.codigo
				FROM programa
				WHERE programa.nombre="windows"
				) AS c1
ON distribuye.codigo=c1.codigo;

SELECT comercio.nombre
FROM comercio
WHERE comercio.cif IN (SELECT distribuye.cif
								FROM distribuye
								INNER JOIN (
												SELECT programa.codigo
												FROM programa
								WHERE programa.nombre="windows"
												) AS c1
								ON distribuye.codigo=c1.codigo
								);
-- Ejercicio 1.16
--c1 
SELECT comercio.cif
FROM comercio
WHERE comercio.nombre="El Corte Ingles" AND comercio.ciudad="Madrid";

-- c2
SELECT distribuye.codigo,distribuye.cantidad
FROM distribuye 
WHERE distribuye.cif = (
								SELECT comercio.cif
								FROM comercio
								WHERE comercio.nombre="El Corte Ingles" AND comercio.ciudad="Madrid"
								);							

SELECT programa.codigo,programa.nombre,c1.cantidad
FROM programa
INNER JOIN (
				SELECT distribuye.codigo,distribuye.cantidad
				FROM distribuye 
				WHERE distribuye.cif = (
								SELECT comercio.cif
								FROM comercio
								WHERE comercio.nombre="El Corte Ingles" AND comercio.ciudad="Madrid"
								)
				)AS c1
ON programa.codigo=c1.codigo;

-- Ejercicio 17
-- c1
SELECT registra.dni
FROM registra
WHERE registra.medio="Internet";

SELECT cliente.dni,cliente.nombre
FROM cliente
WHERE cliente.dni IN (
							SELECT registra.dni
							FROM registra
							WHERE registra.medio="Internet"
							);